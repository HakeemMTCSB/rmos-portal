<?php 
class icampus_Plugin_Layout extends Zend_Controller_Plugin_Abstract 
{
	public function preDispatch(Zend_Controller_Request_Abstract $request){
		 
    	$auth = Zend_Auth::getInstance(); 
    	
    	if ($auth->hasIdentity()) {
    		if( isset($auth->getIdentity()->role) ){
        		$role = $auth->getIdentity()->role;
    		}else{
    			$role = "guest";
    		}
    	}else{
    		$role = "";
    	} 
        $layout = Zend_Layout::getMvcInstance(); 
    	
        switch ($role) { 
        	case 'administrator': 
            	$layout->setLayout('administrator'); 
               	break; 
    
            default:
            	$layout->setLayout('default'); 
                break; 
        } 
	} 
	
}
?>