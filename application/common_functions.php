<?php
/**
* collection of common functions
*
**/



/**
* get database connection
*
**/
function getDB() {
	$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');
	$params = array('host'=>$config->resources->db->params->host,
					'username' => $config->resources->db->params->username,
					'password'=>$config->resources->db->params->password,
					'dbname'=>$config->resources->db->params->dbname,
					'driver_options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8')
				);
	if(isset($config->resources->db->params->unix_socket)) {
		$params['unix_socket'] = $config->resources->db->params->unix_socket;
	}
	$db= Zend_Db::factory('Pdo_Mysql', $params);
	return($db);
}

/**
 * prints variable values and code location 
 *
 * @param mixed $variable
 * */
function debug($variable)
{
    $backtrace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1);
    echo "<pre>";
    echo "file:<strong> " . $backtrace[0]['file'] . "</strong>\n";
    echo "line : <strong>" . $backtrace[0]['line'] . "</strong>\n";

    print_r($variable);
    echo "</pre>";
}

/**
 * like debug but
 * exits as well
 *
 * @param mixed $variable
 * */
function dd($variable)
{
    $backtrace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1);
    echo "<pre>";
    echo "file:<strong> " . $backtrace[0]['file'] . "</strong>\n";
    echo "line : <strong>" . $backtrace[0]['line'] . "</strong>\n";

    print_r($variable);
    echo "</pre>";
    exit;
}

/**
* takes two dates and format them for display
* example:
* echo tofrom_date_string('2014-05-01', '2014-05-04');
* // 01 - 04 May 2014
* echo tofrom_date_string('2014-05-01', '2014-05-01');
* // 01 May 2014 
*
**/
function tofrom_date_string($date_from, $date_to)
{


    $date_from_timestamp = strtotime($date_from);
    $date_to_timestamp = strtotime($date_to);

    if (($date_from_timestamp == 0) || ($date_to_timestamp == 0)) {
        return "N/A";
    }

    if (date('Y-m', $date_to_timestamp) == date('Y-m', $date_from_timestamp)) {
        if ($date_from == $date_to) {
            $date_string = date('d M, Y', $date_from_timestamp);

        } else {
            $date_string = date('d', $date_from_timestamp) . ' - ' . date('d M, Y', $date_to_timestamp);
        }
    } else {
        if ($date_from == $date_to) {
            $date_string = date('d M, Y');

        } else {

            $date_string = date('d M', $date_from_timestamp) . ' - ' . date('d M, Y', $date_to_timestamp);
        }
    }
    return ($date_string);
}


function display_message($message, $notice_type = 'notice') {
    ?>
    <div class="message <?php echo $notice; ?>"><strong><?php echo $message;?></strong>.</div>
    <?php
}

/**
 * this changes a flat array into a multi-dimension array based on field value
 * $records an array of records returned from a query
 * $groupbyfield = array('country','state','fief')
 *
 * @param unknown $records
 * @param unknown $groupbyfield
 * @return unknown
 * */
function flat2structured($records, $groupbyfield)
{
    $varname = array();
    $groupbyes = $groupbyfield; //array('country','state','fief');

    $variablename = '$varname';
    foreach ($groupbyes as $groupby) {
        $variablename .= '[\'{' . strtolower($groupby) . '}\']';
    }

    $variablename .= '[] = $record;';
    foreach ($records as $record) {
        $tobereplace = $variablename;
        foreach ($record as $key => $value) {
            $tobereplace = str_replace("{" . strtolower($key) . "}", $value, $tobereplace);
        }
        eval($tobereplace);
    }
    return $varname;
}

function format_date($date, $format=null)
{
	return format_date_to_view($date, $format);
}

function format_date_to_view($date, $format=null) {
    if($format == null) {
        $format = 'd-m-Y';
    }
	
	/* for lazy people */
	if ( $format == 'full' ) {
		$format = 'd-m-Y H:i A';
	}

    if(date('Y-m-d', strtotime($date)) == '1970-01-01') {
        return ('');
    }
    return(date($format, strtotime($date)));
}

function clean_string($name,$replace='-')
{
	$untouched = $name;
	$name = strip_tags( trim($name) );
	preg_match_all('/[a-z0-9]+/',strtolower($name), $match);

	$value = implode($replace,$match[0]);

	//this means that the value is some unicode crap
	if ( $value == '' )
	{
		$value = substr(md5(strlen($value)),0,8);
		//$value = $untouched;
	}

	return $value;
}

//Clears the array of fields that's not in the schema.
//mysql/mariadb ONLY
function friendly_columns($table, $data) {

    $db = getDb();
    $sql = "Describe " . $table ;
    $result = $db->query($sql);

    $schema =  $result->fetchAll();
    foreach($schema as $row) {
        $fields[]  = $row['Field'];
    }


    foreach($data as $fieldname => $value) {
        if(!in_array($fieldname, $fields)) {
            unset($data[$fieldname]);
        }
    }

    return($data);


}


/*
 * Convert numerical number to words
 */
function convert_number_to_words($number) {

	$hyphen      = '-';
	$conjunction = ' and ';
	$separator   = ', ';
	$negative    = 'negative ';
	$decimal     = ' point ';
	$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
	);

	if (!is_numeric($number)) {
		return false;
	}

	if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
		// overflow
		trigger_error(
		'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
		E_USER_WARNING
		);
		return false;
	}

	if ($number < 0) {
		return $negative . convert_number_to_words(abs($number));
	}

	$string = $fraction = null;

	if (strpos($number, '.') !== false) {
		list($number, $fraction) = explode('.', $number);
	}

	switch (true) {
		case $number < 21:
			$string = $dictionary[$number];
			break;
		case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
		case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
		default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
	}

	if (null !== $fraction && is_numeric($fraction)) {
		$string .= $decimal;
		$words = array();
		foreach (str_split((string) $fraction) as $number) {
			$words[] = $dictionary[$number];
		}
		$string .= implode(' ', $words);
	}

	return ucwords($string);
}

/*
 * Generate pdf
 * @param array $content_option array of option to generate PDF
 * @return int The function returns the number of bytes that were written to the file, or false on failure.
 * @throws Exception 
 *  
 */
function generatePdf(array $content_option){
	
	$options = array(
		'content' => '',
		'file_name' => 'INCEIF',
		'file_extension' => 'pdf',
		'save_path' => DOCUMENT_PATH,
		'save' => false,
		'css' => '@page { margin: 10px 50px; 0px; 50px}
				  body { font-family: Helvetica, Arial, sans-serif; font-size:14px }',
			
		'header' => '<script type="text/php">
					if ( isset($pdf) ) {
				
					  $header = $pdf->open_object();
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
						
					  $img_w = 180; 
  					  $img_h = 64;
//                      $pdf->image("images/logo_text.jpg",  $w-($img_w+35), 20, $img_w, $img_h);
					  
					  // Draw a line along the bottom
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
			 		  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					  $y = $h - (2.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
			
			          $pdf->close_object();
			
					  $pdf->add_object($header, "all");
					}
					</script>',
			
		'footer' => '<script type="text/php">
					if ( isset($pdf) ) {
				
			          $footer = $pdf->open_object();
			
					  $font = Font_Metrics::get_font("Helvetica");
					  $size = 8;
					  $color = array(0,0,0);
					  $text_height = Font_Metrics::get_font_height($font, $size)+2;
					 
					  $w = $pdf->get_width();
					  $h = $pdf->get_height();
					 
				
					  // Draw a line along the bottom
					  $y = $h - (2.5 * $text_height)-10;
					  $pdf->line(10, $y, $w - 10, $y, $color, 1);
				
					  //1st row footer
					  $text = "Lorong Universiti A, 59100 Kuala Lumpur, Malaysia. (718736-K)";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (2 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
					 
			          //2nd row footer
					  $text = "t: +603 7651 4000		f : +603 7651 4094		w: www.inceif.org";
			          $width = Font_Metrics::get_text_width($text, $font, $size);	
				      $y = $h - (1 * $text_height)-10;
					  $x = ($w - $width) / 2.0;
					  
					  $pdf->page_text($x, $y, $text, $font, $size, $color);
				
					  $pdf->close_object();
			
					  $pdf->add_object($footer, "all");
					 
					}
					</script>' 		
	);
	
	foreach ($content_option as $key => $value) {
		if ( !array_key_exists($key, $options) ) {
	      throw new Exception("Option '$key' doesn't exist");
	    }
	
	    $options[$key] = $value;
	}
	
	
        require_once '../library/dompdf/dompdf_config.inc.php';
	//require_once 'dompdf_config.inc.php';
	$autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
	$autoloader->pushAutoloader('DOMPDF_autoload');

	$dompdf = new DOMPDF();
	$dompdf->set_paper("a4","portrait");

	$html = '<html>
            		<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><style>'.$options['css'].'body { font-family: dejavu sans; }</style></head>
            		<body>'.$options['header'].$options['content'].$options['footer'].'</body>
             </html>';

	$dompdf->load_html($html,'UTF-8');
	$dompdf->render();

	if($options['save'] == true){ //  save file to location

		$output = $dompdf->output();

		if (!is_dir($options['save_path']))
		{
			if ( mkdir_p($options['save_path']) === false )
			{
				throw new Exception('Cannot create attachment folder ('.$options['save_path'].')');
			}
		}

		$pdf_file = file_put_contents($options['save_path']."/".$options['file_name'], $output);
	  
	}else{// stream file

		$pdf_file = $dompdf->stream($options['file_name']);
	}

	return $pdf_file;
}



/*
 * credits to WP
 * 
 * credit to munzir also
 */
function mkdir_p( $target )
{
    // safe mode fails with a trailing slash under certain PHP versions.
    $target = rtrim($target, '/'); // Use rtrim() instead of untrailingslashit to avoid formatting.php dependency.
    if ( empty($target) )
        $target = '/';

    if ( file_exists( $target ) )
        return @is_dir( $target );

    // We need to find the permissions of the parent folder that exists and inherit that.
    $target_parent = dirname( $target );
    while ( '.' != $target_parent && ! is_dir( $target_parent ) ) {
        $target_parent = dirname( $target_parent );
    }

    // Get the permission bits.
    $dir_perms = false;
    if ( $stat = @stat( $target_parent ) ) 
    {
        $dir_perms = $stat['mode'] & 0007777;
    }
    else 
    {
        $dir_perms = 0777;
    }

    if ( @mkdir( $target, $dir_perms, true ) ) 
    {

        // If a umask is set that modifies $dir_perms, we'll have to re-set the $dir_perms correctly with chmod()
        if ( $dir_perms != ( $dir_perms & ~umask() ) ) 
        {
            $folder_parts = explode( '/', substr( $target, strlen( $target_parent ) + 1 ) );
            for ( $i = 1; $i <= count( $folder_parts ); $i++ ) 
            {
                @chmod( $target_parent . '/' . implode( '/', array_slice( $folder_parts, 0, $i ) ), $dir_perms );
            }
        }

        return true;
    }

    return false;
}

function getext($filename)
{
	$ext = pathinfo($filename, PATHINFO_EXTENSION);
	return $ext;
}

function getPublic() {
    chdir(APPLICATION_PATH);
    return realpath("../public");
}

function generateRandomString($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

function get_ip()
{
    /*inception level ternaries*/
    $ip = getenv('HTTP_CLIENT_IP')?:
    getenv('HTTP_X_FORWARDED_FOR')?:
    getenv('HTTP_X_FORWARDED')?:
    getenv('HTTP_FORWARDED_FOR')?:
    getenv('HTTP_FORWARDED')?:
    getenv('REMOTE_ADDR');

    return $ip;
}

//get the date part correctly from dojo calendar input
function splitAndFormatDojodate($date) {
	$positionoftime = strpos($date, '00:00:00');
	$date_parts = str_split($date, $positionoftime);
	$datetodatabase = date('Y-m-d', strtotime($date_parts[0]));

	return($datetodatabase);
}

function time_ago($time)
{
   $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
   $lengths = array("60","60","24","7","4.35","12","10");

   $now = time();

       $difference     = $now - $time;
       $tense         = "ago";

   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
       $difference /= $lengths[$j];
   }

   $difference = round($difference);

   if($difference != 1) {
       $periods[$j].= "s";
   }

   return "$difference $periods[$j] ".$tense;
}

/*
	generate datetime for html5 time tag
*/
function getDateTimeValue( $intDate = null ) {

	$strFormat = 'Y-m-dTH:i:sP';
	$strDate = $intDate ? date( $strFormat, $intDate ) : date( $strFormat ) ; 
	
	return $strDate;
}

function ifexists(&$what,$return='')
{
	return isset($what) ? $what : $return;
}

function clean($value='',$type='normal')
{
	$value = trim(strip_tags($value));
	
	if ( $type == 'alpha' )
	{
		$value = preg_replace('/[^\da-z]/i', '', $value);
	}

	return $value;
}

function clean_array($data=array(), $keys=array())
{
	if (is_array($data))
	{
		foreach( $data as $key => $val)
		{
			if ( !empty($keys) )
			{
				if ( is_string($val) && in_array($key, $keys) )
				{
					$data[$key] = clean($val);
				}
				else
				{
					$data[$key] = clean_array($val,$keys);
				}
			}
			else
			{

				if(is_string($val))
				{
					$data[$key] = clean($val);
				}
				elseif(is_array($val))
				{
					$data[$key] = clean_array($val,$keys);
				}
			}
		}

	}
	return $data;
}

function snippet($text,$length=250,$tail="...")
{
	$text = trim($text);
	$txtl = strlen($text);
	if($txtl > $length) {
		for($i=1;$text[$length-$i]!=" ";$i++) {
			if($i == $length) {
				return substr($text,0,$length) . $tail;
			}
		}
		$text = substr($text,0,$length-$i+1) . $tail;
	}
	return $text;
}

function format_filesize($size)
{
	$unit=array('b','kb','mb','gb','tb','pb');
	return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

function encrypt($text,$enc = true)
{
	$salt = '1nc31f';
	if ( $enc == true ) 
	{
    	return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
	}
	else
	{
  	 	return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
	}
}

/* return epoch time in miliseconds */
function datetomili($date)
{
	return strtotime($date) * 1000;
}

/* Javascript Packer */
require_once('JSPacker/Packer.php');
