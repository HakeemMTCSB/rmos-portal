<?php

class PortalController extends Zend_Controller_Action
{

    public function init()
    {
        $auth = $this->auth = Zend_Auth::getInstance();
        $this->role = $auth->getIdentity()->role;
        $this->regModel = new Thesis_Model_DbTable_Registration();

        $this->announceDB = new App_Model_General_DbTable_Announcements();
    }

    public function indexAction()
    {
        $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'portal', 'action' => 'home'), 'default', true));
    }

    public function homeAction()
    {

        $this->view->title = $this->view->translate('Home');

        if ($this->role == 'supervisor') {

            $this->userId = $this->auth->getIdentity()->iduser;

            $profile = $this->auth->getIdentity()->info;
            $this->view->profile = $profile;
            
            $proposal = $this->regModel->getProposalSuperviseeHome($this->userId,null, null,1);

            $staff = $this->regModel->getStaff($profile['IdStaff']);
            $this->view->staff = $staff;

            $form = new App_Form_Supervisee();

            if ($this->getRequest()->isPost()) {
                $formData = $this->getRequest()->getPost();

                $form->populate($formData);
                $proposal = $this->regModel->getProposalSuperviseeHome($this->userId,$formData, null, 1);

                if($formData['save_email2'] == 'Update'){

                    $data['email2'] = $formData['email2'];

                    $this->regModel->updateEmail2($data,'IdStaff = ' . $profile['IdStaff']);
                    $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'portal', 'action' => 'home'), 'default', true));
                }
            }
//			$articleship = $this->regModel->getArticleshipSupervisee($this->userId);
//			$exemption = $this->regModel->getExemptionSupervisee($this->userId);

                $results = array();

                foreach ($proposal as $pro) {
                    $results[strtotime($pro['created_date'])] = array(
                        'id' => $pro['p_id'],
                        'SubCode' => $pro['SubCode'],
                        'p_title' => $pro['p_title'],
                        'student_id' => $pro['registrationId'],
                        'student_name' => $pro['student_name'],
                        'research_type' => 'proposal',
                        'date' => $pro['created_date'],
                        'StatusName' => $pro['StatusName'],
                        'pd_status_description' => $pro['pd_status_description'],
                        'status_description' => $pro['status_description'],
                        'student_email' => $pro['student_email'],
                        'DefinitionCode' => $pro['DefinitionCode']
                    );
                }



            krsort($results);

                $this->view->results = $results;

                //notify
                $this->notifyModel = new Thesis_Model_DbTable_Notify();
                $notifies = $this->notifyModel->getNotifiesForSupervisor($this->userId);

                $this->view->notifies = $notifies;


            $this->view->form = $form;

            $this->render('home-supervisor');
        } else if ($this->role == 'examiner') {
            $this->userId = $this->auth->getIdentity()->id;

            $proposal = $this->regModel->getProposalExaminee($this->userId);
            $articleship = $this->regModel->getArticleshipExaminee($this->userId);
            $exemption = $this->regModel->getExemptionExaminee($this->userId);

            $maindata = array();

            foreach ($proposal as $prop) {
                $maindata[] = array(
                    'id'            => $prop['p_id'],
                    'student_id'    => $prop['student_id'],
                    'research_type' => 'proposal',
                    'status_code'   => $prop['status_code'],
                    'student_name'  => $prop['student_name'],
                    'created_date'  => $prop['created_date'],
                    'updated_date'  => $prop['updated_date']
                );
            }

            foreach ($articleship as $art) {
                $maindata[] = array(
                    'id'            => $art['a_id'],
                    'student_id'    => $art['student_id'],
                    'research_type' => 'articleship',
                    'status_code'   => $art['status_code'],
                    'student_name'  => $art['student_name'],
                    'created_date'  => $art['created_date'],
                    'updated_date'  => $art['updated_date']
                );
            }

            foreach ($exemption as $exm) {
                $maindata[] = array(
                    'id'            => $exm['e_id'],
                    'student_id'    => $exm['student_id'],
                    'research_type' => 'exemption',
                    'status_code'   => $exm['status_code'],
                    'student_name'  => $exm['student_name'],
                    'created_date'  => $exm['created_date'],
                    'updated_date'  => $exm['updated_date']
                );
            }

            $this->view->results = $maindata;

            $this->render('home-examiner');
        } else {
            $IdStudentRegistration = $this->auth->getIdentity()->registration_id;

            $supervisors = $this->view->supervisors =  $this->regModel->getSupervisorsStudentDistinct($IdStudentRegistration, 'proposal');

            $this->studentinfo = $this->view->studentinfo = $this->auth->getIdentity()->info;


            //announcement
            $idScheme = $this->announceDB->getIdSchemebasedOnReg($IdStudentRegistration);

            $this->view->importantannouncements = $this->announceDB->getStudentAnnouncements($this->studentinfo['IdProgram'], $idScheme, 'thesis', $this->studentinfo['IdStudentRegistration']);

            foreach ($this->view->importantannouncements as $i => $announcement) {
                $files = $this->announceDB->getFiles($announcement['id']);
                $this->view->importantannouncements[$i]['files'] = $files;
            }

            //get research
            $proposal = $this->regModel->getProposalByStudent($this->studentinfo['IdStudentRegistration']);
            $articleship = $this->regModel->getArticleshipByStudent($this->studentinfo['IdStudentRegistration']);
            $exemption = $this->regModel->getExemptionByStudent($this->studentinfo['IdStudentRegistration']);

            $this->view->proposal = $proposal;
            $this->view->articleship = $articleship;
            $this->view->exemption = $exemption;

            //render
            $this->render('home-student');
        }
    }

    public function photoAction()
    {
        $this->view->title = $this->view->translate('Photo');
    }

    public function euniAction()
    {
        $url = EUNI_URL;

        if ($this->role == 'student') {
            $student = $this->auth->getIdentity();

            $data = array('username' => $student->appl_username, 'password' => $student->appl_password);

            $token = urlencode(encrypt(json_encode($data)));
            $this->_redirect($url . '/authentication/validate/?token=' . $token, array('prependBase' => false));
        } else {
            $this->_redirect($url, array('prependBase' => false));
        }
    }
}