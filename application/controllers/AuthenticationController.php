<?php

class AuthenticationController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'authentication', 'action' => 'login'), 'default', true));
        exit;
    }

    public function loginAction()
    {
        //http://rmos-portal.mtcsb.my/authentication/login/check/3/username/??
        $check = $this->_getParam('check');
        $icno_encode = $this->_getParam('username');

//        $check = 3;
//        $icno_encode = 700611025221;

        //single sign-on

//        $icno_decode = base64_encode($icno_encode);

        $icno = base64_decode($icno_encode);

        if ($check == 3) {

            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
            $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

            $authAdapter->setTableName('student_profile')
                ->setIdentityColumn('appl_idnumber')
                ->setCredentialColumn('appl_idnumber');

            $authAdapter->setIdentity($icno);
            $authAdapter->setCredential($icno);

            $authAdapter->getDbSelect()
                ->join('tbl_studentregistration', 'tbl_studentregistration.sp_id=student_profile.id', array())
                ->where('tbl_studentregistration.profileStatus = ?', 92);

            // do the authentication
            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($authAdapter);
            if ($result->isValid()) {
                // success : store database row to auth's storage system
                $data = $authAdapter->getResultRowObject(null, 'password');

                if (!$data) {
                    echo "<script>alert('No data found for this student');</script>";

                    $storage = new Zend_Auth_Storage_Session();
                    $storage->clear();
                    exit;
                } else {
                    //mapping id with userid from table user
                    $data->role = "student";

                    //select account if have multi program
                    $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
                    $student = $studentRegDB->getDataFromProfile($data->id);

                    if (!$student) {
                        echo "<script>alert('No registration found for this student');</script>";

                        $storage = new Zend_Auth_Storage_Session();
                        $storage->clear();
                        exit;
                    } else {
                        $data->registrationId = $student[0]['registrationId'];
                        $data->registration_id = $student[0]['IdStudentRegistration'];
                        $data->IdStudentRegistration = $student[0]['IdStudentRegistration'];

                        //program info
                        $info = $studentRegDB->getStudentInfo($student[0]['IdStudentRegistration']);

                        $data->info = $info;

                        $thesisDb = new Thesis_Model_DbTable_General();
                        $research = $thesisDb->getResearchType($student[0]['IdStudentRegistration']);

                        if (empty($research)) {
                            echo "<script>alert('No researh found for this student');</script>";

                            $storage = new Zend_Auth_Storage_Session();
                            $storage->clear();
                            exit;
                        }

                        $data->research_type = $research;

                        $auth->getStorage()->write($data);

                        $username = $auth->getIdentity()->username;

                        $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'portal', 'action' => 'home'), 'default', true));
                    }

                }

            } else {

                echo "<script>alert('No data found for this student');</script>";
                exit;
            }

        }
        //single sign-on

        $form = new App_Form_Login();

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                // collect the data from the user
                Zend_Loader::loadClass('Zend_Filter_StripTags');
                $filter = new Zend_Filter_StripTags();
                $username = $filter->filter($this->_request->getPost('username'));
                $password = $filter->filter($this->_request->getPost('password'));

                $type = $this->_request->getPost('type', 0);

                //SUPERVISOR
                if ($type == 1) {
                    //process form
                    $dbAdapter = Zend_Db_Table::getDefaultAdapter();
                    $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

                    $authAdapter->setTableName('tbl_user')
                        ->setIdentityColumn('loginName')
                        ->setCredentialColumn('passwd');

                    // Set the input credential values to authenticate against
                    $authAdapter->setIdentity($username);
                    $authAdapter->setCredential(md5($password));

                    $authAdapter->getDbSelect()
                        ->join('tbl_staffmaster', 'tbl_staffmaster.IdStaff=tbl_user.IdStaff', array())
                        ->where('tbl_user.UserStatus = ?', 1);

                    // do the authentication
                    $auth = Zend_Auth::getInstance();
                    $result = $auth->authenticate($authAdapter);

                    if ($result->isValid()) {
                        // success : store database row to auth's storage system
                        $data = $authAdapter->getResultRowObject(null, 'passwd');

                        if (!$data) {
                            $flashMessenger = $this->_helper->getHelper('FlashMessenger');
                            $flashMessenger->addMessage(array('error' => 'Supervisor data not found'));
                            $this->_redirect($this->view->url(array('module' => 'default'), 'default', true));
                        } else {
                            //mapping id with userid from table user
                            $data->id = $data->iduser;
                            $data->role = "supervisor";


                            $larrCommonModel = new App_Model_Common();
                            $staffdetails = $larrCommonModel->fnGetStaff($data->IdStaff);

                            if (!$staffdetails) {
                                $storage = new Zend_Auth_Storage_Session();
                                $storage->clear();

                                $flashMessenger = $this->_helper->getHelper('FlashMessenger');
                                $flashMessenger->addMessage(array('error' => 'Staff Profile record not found'));
                                $this->_redirect($this->view->url(array('module' => 'default'), 'default', true));
                            } else {
                                $data->info = $staffdetails;

                                $auth->getStorage()->write($data);

                                $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'portal', 'action' => 'home'), 'default', true));
                            }

                        }

                    } else {
                        // failure: clear database row from session

                        $flashMessenger = $this->_helper->getHelper('FlashMessenger');
                        $flashMessenger->addMessage(array('error' => 'Login failed. Either username or password is incorrect'));
                        $this->_redirect($this->view->url(array('module' => 'default'), 'default', true));
                    }
                } else if ($type == 2) {
                    //process form
                    $db = Zend_Db_Table::getDefaultAdapter();

                    $fail = 0;

                    $select = $db->select()
                        ->from(array('a' => 'thesis_examiner_user'))
                        ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.staff_id', array('u.passwd'))
                        ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
                        ->where('a.email=?', $username);

                    $check = $db->fetchRow($select);

                    if (empty($check)) {
                        $fail = 1;
                    }

                    // do the authentication
                    if ($fail == 0) {
                        $auth = Zend_Auth::getInstance();
                        $data = (object)$check;
                        $data->role = "examiner";

                        if ($data->examiner_type == 0) {
                            $data->password = $data->passwd;
                        }

                        if (md5($password) != $data->password) {
                            $storage = new Zend_Auth_Storage_Session();
                            $storage->clear();

                            $flashMessenger = $this->_helper->getHelper('FlashMessenger');
                            $flashMessenger->addMessage(array('error' => 'Invalid Password'));
                            $this->_redirect($this->view->url(array('module' => 'default'), 'default', true));
                        } else {
                            $data->info = $check;
                            $auth->getStorage()->write($data);

                            $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'portal', 'action' => 'home'), 'default', true));
                        }


                    }

                    if ($fail) {
                        $flashMessenger = $this->_helper->getHelper('FlashMessenger');
                        $flashMessenger->addMessage(array('error' => 'Login failed. Either username or password is incorrect'));
                        $this->_redirect($this->view->url(array('module' => 'default'), 'default', true));
                    }
                } else {
                    //process form
                    $dbAdapter = Zend_Db_Table::getDefaultAdapter();
                    $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

                    /*$authAdapter->setTableName('student_profile')
                                ->setIdentityColumn('appl_email')
                                ->setCredentialColumn('appl_password');

                    // Set the input credential values to authenticate against
                    $authAdapter->setIdentity($username);
                    //$authAdapter->setCredential(md5($password));
                    $authAdapter->setCredential($password);*/

                    $authAdapter->setTableName('student_profile')
                        ->setIdentityColumn('appl_username')
                        ->setCredentialColumn('appl_password')
                        ->setCredentialTreatment('MD5(?)'); // changed

                    $authAdapter->setIdentity($username);
                    $authAdapter->setCredential($password);

                    $authAdapter->getDbSelect()
                        ->join('tbl_studentregistration', 'tbl_studentregistration.sp_id=student_profile.id', array())
                        ->where('tbl_studentregistration.profileStatus = ?', 92);

                    // do the authentication
                    $auth = Zend_Auth::getInstance();
                    $result = $auth->authenticate($authAdapter);
                    if ($result->isValid()) {
                        // success : store database row to auth's storage system
                        $data = $authAdapter->getResultRowObject(null, 'password');

                        if (!$data) {
                            $flashMessenger = $this->_helper->getHelper('FlashMessenger');
                            $flashMessenger->addMessage(array('error' => 'Student data not found'));
                            $this->_redirect($this->view->url(array('module' => 'default'), 'default', true));
                        } else {
                            //mapping id with userid from table user
                            $data->role = "student";

                            //select account if have multi program
                            $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
                            $student = $studentRegDB->getDataFromProfile($data->id);

                            if (!$student) {
                                $storage = new Zend_Auth_Storage_Session();
                                $storage->clear();

                                //$this->view->alertError = 'Student registration record not found';

                                $flashMessenger = $this->_helper->getHelper('FlashMessenger');
                                $flashMessenger->addMessage(array('error' => 'Student registration record not found'));
                                $this->_redirect($this->view->url(array('module' => 'default'), 'default', true));
                            } else {
                                $data->registrationId = $student[0]['registrationId'];
                                $data->registration_id = $student[0]['IdStudentRegistration'];
                                $data->IdStudentRegistration = $student[0]['IdStudentRegistration'];

                                //program info
                                $info = $studentRegDB->getStudentInfo($student[0]['IdStudentRegistration']);

                                $data->info = $info;

                                $thesisDb = new Thesis_Model_DbTable_General();
                                $research = $thesisDb->getResearchType($student[0]['IdStudentRegistration']);

                                if (empty($research)) {
                                    $storage = new Zend_Auth_Storage_Session();
                                    $storage->clear();

                                    //$this->view->alertError = 'Student registration record not found';

                                    $flashMessenger = $this->_helper->getHelper('FlashMessenger');
                                    $flashMessenger->addMessage(array('error' => 'No research found.'));
                                    $this->_redirect($this->view->url(array('module' => 'default'), 'default', true));
                                }

                                $data->research_type = $research;

                                $auth->getStorage()->write($data);

                                $username = $auth->getIdentity()->username;

                                $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'portal', 'action' => 'home'), 'default', true));
                            }

                        }

                    } else {
                        // failure: clear database row from session

                        $flashMessenger = $this->_helper->getHelper('FlashMessenger');
                        $flashMessenger->addMessage(array('error' => 'Login failed. Either username or password is incorrect'));
                        $this->_redirect($this->view->url(array('module' => 'default'), 'default', true));
                    }
                }//TYPE
            } else {
                $form->populate($formData);
            }

        }

        $this->view->form = $form;
    }

    public function validateAction()
    {
        $type = $this->_getParam('type');
        $token = $this->_getParam('token');
        $fail = false;

        $auth = Zend_Auth::getInstance();

        if ($auth->hasIdentity()) {
            $this->_redirect($this->view->url(array('module' => 'portal', 'controller' => 'home', 'action' => 'index'), 'default', true));
        }

        if (empty($token)) {
            //throw new Exception('Invalid Token');
            $fail = true;
        }

        $tokendata = json_decode(encrypt($token, false), true);

        if (empty($tokendata)) {
            //throw new Exception('Invalid Token Data');
            $fail = true;
        }

        $username = $tokendata['username'];
        $password = $tokendata['password'];


        //SUPERVISOR
        if ($type == 1) {
            //process form
            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
            $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

            $authAdapter->setTableName('tbl_user')
                ->setIdentityColumn('loginName')
                ->setCredentialColumn('passwd');

            // Set the input credential values to authenticate against
            $authAdapter->setIdentity($username);
            $authAdapter->setCredential($password);

            // do the authentication
            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($authAdapter);


            if ($result->isValid()) {
                $data = $authAdapter->getResultRowObject(null, 'password');

                if (!$data) {
                    $fail = true;
                } else {
                    //mapping id with userid from table user
                    $data->id = $data->iduser;
                    $data->role = "supervisor";


                    $larrCommonModel = new App_Model_Common();
                    $staffdetails = $larrCommonModel->fnGetStaff($data->IdStaff);

                    if (!$staffdetails) {
                        $fail = true;
                    } else {
                        $data->info = $staffdetails;

                        $auth->getStorage()->write($data);

                        $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'portal', 'action' => 'home'), 'default', true));
                    }

                }
            }
        } elseif ($type == 2) {
            //process form
            $db = Zend_Db_Table::getDefaultAdapter();

            $select = $db->select()
                ->from(array('a' => 'thesis_examiner_user'))
                ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.staff_id', array())
                ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
                ->where('a.email=?', $username);

            $check = $db->fetchRow($select);

            if (empty($check)) {
                $fail = true;
            } else {
                // do the authentication

                $auth = Zend_Auth::getInstance();
                $data = (object)$check;
                $data->role = "examiner";

                if ($password != $data->password) {
                    $fail = true;
                } else {
                    $data->info = $check;
                    $auth->getStorage()->write($data);

                    $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'portal', 'action' => 'home'), 'default', true));
                }
            }
        } else {

            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
            $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
            $authAdapter->setTableName('student_profile')
                ->setIdentityColumn('appl_username')
                ->setCredentialColumn('appl_password');

            $authAdapter->setIdentity($username);
            $authAdapter->setCredential($password);

            $authAdapter->getDbSelect()
                ->join('tbl_studentregistration', 'tbl_studentregistration.sp_id=student_profile.id', array())
                ->where('tbl_studentregistration.profileStatus = ?', 92);


            // do the authentication
            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($authAdapter);


            if ($result->isValid()) {
                $data = $authAdapter->getResultRowObject(null, 'password');

                if (!$data) {
                    $fail = true;
                } else {
                    //mapping id with userid from table user
                    $id = $data->id;
                    $data->role = "student";

                    //select account if have multi program
                    $studentRegDB = new App_Model_Record_DbTable_StudentRegistration();
                    $student = $studentRegDB->getDataFromProfile($data->id);

                    //
                    $auth = Zend_Auth::getInstance();
                    $authns = new Zend_Session_Namespace($auth->getStorage()->getNamespace());
                    $authns->setExpirationSeconds(30 * 60); // 60 mins

                    if (!$student) {
                        $fail = true;
                    } else {
                        $data->registrationId = $student[0]['registrationId'];

                        $data->registrationId = $student[0]['registrationId'];
                        $data->registration_id = $student[0]['IdStudentRegistration'];

                        //program info
                        $info = $studentRegDB->getStudentInfo($student[0]['IdStudentRegistration']);

                        $thesisDb = new Thesis_Model_DbTable_General();
                        $research = $thesisDb->getResearchType($student[0]['IdStudentRegistration']);

                        if (empty($research)) {
                            $storage = new Zend_Auth_Storage_Session();
                            $storage->clear();

                            //$this->view->alertError = 'Student registration record not found';

                            $flashMessenger = $this->_helper->getHelper('FlashMessenger');
                            $flashMessenger->addMessage(array('error' => 'No research found.'));
                            $this->_redirect($this->view->url(array('module' => 'default'), 'default', true));
                        }

                        $data->research_type = $research;

                        $data->info = $info;

                        $auth->getStorage()->write($data);

                        $username = $auth->getIdentity()->username;

                        $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'portal', 'action' => 'home'), 'default', true));
                    }

                }
            }

        }

        if ($fail) {
            $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'authentication', 'action' => 'login'), 'default', true));
        }
    }

    public function logoutAction()
    {
        $storage = new Zend_Auth_Storage_Session();
        $storage->clear();

        $this->_redirect($this->view->url(array('module' => 'default'), 'default', true));
//        http://oumvle.oum.edu.my/index/home?lang=en
//        $url = "https://www.oum.edu.my/";

//        $this->_redirect($url);
    }

    public function changepasswordAction()
    {
        // creating form object
        $auth = Zend_Auth::getInstance();
        $user = $auth->getIdentity();
//        print_r($user);exit;
        $lobjChangepasswordform = new App_Form_Changepassword();

        // making form available to view
        $this->view->form = $lobjChangepasswordform;

        // checking for clear button is pressed or not
        if ($this->getRequest()->getParam('Clear')) {
            $this->_redirect($this->baseUrl . '/authentication/changepassword');
        }

        // checking wheather form is pressed or not
        if ($this->getRequest()->ispost()) {
            // creating db object
            $lobjdb = Zend_Db_Table::getDefaultAdapter();

            // getting userid form present session
            $auth = Zend_Auth::getInstance();
            $lintiduser = $auth->getIdentity()->iduser;

            // getting current user password from db
            $lstrsql = $lobjdb->select()
                ->from('tbl_user', 'passwd')
                ->where("iduser = $lintiduser ");

            $stroldpasswordfromdb = App_Model_DefModel::selectrow($lstrsql);
            $lstrsqlName = $lobjdb->select()
                ->from('tbl_user', 'loginName')
                ->where("iduser = $lintiduser ");

            $loginName = App_Model_DefModel::selectrow($lstrsqlName);
            // getting entered old password data from form & making md5 string of it
            $strpasswordFromForm = $this->getRequest()->getParam('oldpassword');
            $strhashmd5 = md5($strpasswordFromForm);

            // if oldpassword from db and entered oldpassword in form is not equal then display message


            // getting newpassword & retype password data from form
            $stroldPasswordFromForm = $this->getRequest()->getParam('oldpassword');
            $strnewPasswordFromForm = $this->getRequest()->getParam('newpassword');
            $strretypepasswordFromForm = $this->getRequest()->getParam('retypepassword');


            /*			if($stroldPasswordFromForm == $strnewPasswordFromForm) {
                            echo '<script language="javascript">alert("Old password & New password are same")</script>';
                        } */
            if ($stroldpasswordfromdb["passwd"] != $strhashmd5) {
                echo '<script language="javascript">alert("Old Password is Invalid")</script>';
            } // checking wheather newpassword & retype is equal or not
            elseif ($strnewPasswordFromForm != $strretypepasswordFromForm) {

                echo '<script language="javascript">alert("New password & retype password is not matching")</script>';
            } // update new password in db if validation is correct
            elseif ($stroldpasswordfromdb["passwd"] == $strhashmd5 && $strnewPasswordFromForm == $strretypepasswordFromForm) {
                $strupdatePassword = md5($strretypepasswordFromForm);
                $lobjdata = array('passwd' => $strupdatePassword);
                $lintwhereCondition = "iduser = $lintiduser";
                $lobjresult = App_Model_DefModel::update('tbl_user', $lobjdata, $lintwhereCondition);

                $this->_helper->flashMessenger->addMessage(array('success' => 'Password successfully updated'));

                $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'authentication', 'action' => 'changepassword'), 'default', true));

                //$this->_redirect( $this->baseUrl . '/generalsetup/changepassword/index');

            }
        }
    }

    public function changepasswordstudentAction()
    {
        // creating form object
        $auth = Zend_Auth::getInstance();
        $user = $auth->getIdentity()->id;
//        print_r($user);exit;
        $lobjChangepasswordform = new App_Form_Changepassword();

        // making form available to view
        $this->view->form = $lobjChangepasswordform;

        // checking for clear button is pressed or not
        if ($this->getRequest()->getParam('Clear')) {
            $this->_redirect($this->baseUrl . '/authentication/changepasswordstudent');
        }

        // checking wheather form is pressed or not
        if ($this->getRequest()->ispost()) {
            // creating db object
            $lobjdb = Zend_Db_Table::getDefaultAdapter();

            // getting userid form present session
            $auth = Zend_Auth::getInstance();
            $lintiduser = $auth->getIdentity()->id;

            // getting current user password from db
            $lstrsql = $lobjdb->select()
                ->from('student_profile', 'appl_password')
                ->where("id = $lintiduser ");

            $stroldpasswordfromdb = App_Model_DefModel::selectrow($lstrsql);
//            $lstrsqlName = $lobjdb->select()
//                ->from('tbl_user','loginName')
//                ->where("iduser = $lintiduser ");
//
//            $loginName = App_Model_DefModel::selectrow($lstrsqlName);
            // getting entered old password data from form & making md5 string of it
            $strpasswordFromForm = $this->getRequest()->getParam('oldpassword');
            $strhashmd5 = md5($strpasswordFromForm);

            // if oldpassword from db and entered oldpassword in form is not equal then display message


            // getting newpassword & retype password data from form
            $stroldPasswordFromForm = $this->getRequest()->getParam('oldpassword');
            $strnewPasswordFromForm = $this->getRequest()->getParam('newpassword');
            $strretypepasswordFromForm = $this->getRequest()->getParam('retypepassword');


            /*			if($stroldPasswordFromForm == $strnewPasswordFromForm) {
                            echo '<script language="javascript">alert("Old password & New password are same")</script>';
                        } */
            if ($stroldpasswordfromdb["appl_password"] != $strhashmd5) {
                echo '<script language="javascript">alert("Old Password is Invalid")</script>';
            } // checking wheather newpassword & retype is equal or not
            elseif ($strnewPasswordFromForm != $strretypepasswordFromForm) {

                echo '<script language="javascript">alert("New password & retype password is not matching")</script>';
            } // update new password in db if validation is correct
            elseif ($stroldpasswordfromdb["appl_password"] == $strhashmd5 && $strnewPasswordFromForm == $strretypepasswordFromForm) {
                $strupdatePassword = md5($strretypepasswordFromForm);
                $lobjdata = array('appl_password' => $strupdatePassword);
                $lintwhereCondition = "id = $lintiduser";
                $lobjresult = App_Model_DefModel::update('student_profile', $lobjdata, $lintwhereCondition);

                $this->_helper->flashMessenger->addMessage(array('success' => 'Password successfully updated'));

                $this->_redirect($this->view->url(array('module' => 'default', 'controller' => 'authentication', 'action' => 'changepasswordstudent'), 'default', true));

                //$this->_redirect( $this->baseUrl . '/generalsetup/changepassword/index');

            }
        }
    }

    public function forgotpasswordAction()
    {
        $this->_helper->layout()->setLayout('forgotpassword');
        $form = new App_Form_ForgotPassword();
        $this->view->errorMsg = '';
        $this->view->sent = 0;
        $db = Zend_Db_Table::getDefaultAdapter();

        if ($this->_request->isPost()) {
            $larrformData = $this->_request->getPost();
            if ($larrformData['type'] == 2) {
                $select = $db->select()
                    ->from(array('a' => 'tbl_user'), array('email', 'iduser', 'loginName', 'fName'))
                    ->join(array('b' => 'tbl_staffmaster'), 'b.IdStaff = a.IdStaff', array('FrontSalutation'))
                    ->joinLeft(array('c' => 'tbl_definationms'), 'c.idDefinition = b.FrontSalutation', array('DefinitionDesc'))
                    ->where("a.email = ?", $larrformData['username'])
                    ->where("a.UserStatus = ?", 1);//active

                $larrResult = $db->fetchRow($select);

                if (empty($larrResult)) {
                    $this->view->errorMsg = $this->view->translate('Invalid email address. Please try again');
                } else {
                    $key = generateRandomString(16);

//                    $keylink = $this->view->serverUrl() . "/authentication/reset/type/" . $larrformData['type'] . "/id/" . $larrResult['iduser'] . "/key/" . $key;
                    $keylink = APPLICATION_URL . "/authentication/reset/type/" . $larrformData['type'] . "/id/" . $larrResult['iduser'] . "/key/" . $key;
                    $db->update('tbl_user', array('userkey' => $key), $db->quoteInto('iduser=?', $larrResult['iduser']));

                    //email new
                    $emailDb = new App_Model_Email();
                    $commDb = new App_Model_Template();

                    $gettemplate = $commDb->getTemplatesByCategory('thesis', 'forgot-password', 0, 1);
                    $template = $gettemplate[0];
                    $larrResult['keylink'] = $keylink;
                    $larrResult['fName'] = $larrResult['DefinitionDesc'] . ' ' . $larrResult['fName'];

                    $dataEmail = array(
                        'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $larrResult['email'],
                        'subject' => $template['tpl_name'],
                        'content' => Thesis_Model_DbTable_Registration::parseContent($larrResult, $template['tpl_content']),
                        'date_que' => date('Y-m-d H:i:s')
                    );

                    $emailDb->add($dataEmail);
                    //email new

                    $this->view->Msg = $this->view->translate('An email has been sent to you. To reset your password, please open the verification email and click on the link provided. ');

                    $this->view->sent = 1;
                }
            } elseif ($larrformData['type'] == 1) {
                $select = $db->select()
                    ->from(array('a' => 'student_profile'), array('appl_email', 'id', 'appl_username', 'appl_fname'))
                    ->where("a.appl_username = ?", $larrformData['username']);

                $larrResult = $db->fetchRow($select);

                if (empty($larrResult)) {
                    $this->view->errorMsg = $this->view->translate('Invalid student ID. Please try again');
                } else {
                    $key = generateRandomString(16);

//                    $keylink = $this->view->serverUrl() . "/authentication/reset/type/" . $larrformData['type'] . "/id/" . $larrResult['id'] . "/key/" . $key;
                    $keylink = APPLICATION_URL . "/authentication/reset/type/" . $larrformData['type'] . "/id/" . $larrResult['id'] . "/key/" . $key;
                    $db->update('student_profile', array('userKey' => $key), $db->quoteInto('id=?', $larrResult['id']));

                    //email new
                    $emailDb = new App_Model_Email();
                    $commDb = new App_Model_Template();

                    $gettemplate = $commDb->getTemplatesByCategory('thesis', 'forgot-password', 0, 1);
                    $template = $gettemplate[0];
                    $larrResult['keylink'] = $keylink;
                    $larrResult['fName'] = $larrResult['appl_fname'];

                    $dataEmail = array(
                        'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $larrResult['appl_email'],
                        'subject' => $template['tpl_name'],
                        'content' => Thesis_Model_DbTable_Registration::parseContent($larrResult, $template['tpl_content']),
                        'date_que' => date('Y-m-d H:i:s')
                    );

                    $emailDb->add($dataEmail);
                    //email new

                    $this->view->Msg = $this->view->translate('An email has been sent to you. To reset your password, please open the verification email and click on the link provided. ');

                    $this->view->sent = 1;
                }
            }
        }

        $this->view->form = $form;

    }

    public function resetAction()
    {
        $this->_helper->layout()->setLayout('forgotpassword');
        $form = new App_Form_ForgotPassword();
        $type = $this->_getParam('type');
        $id = $this->_getParam('id');
        $key = $this->_getParam('key');

        $this->view->errorMsg = '';
        $this->view->Msg = '';
        $this->view->sent = 0;
        $db = Zend_Db_Table::getDefaultAdapter();

        if ($type == 2) {
            // get userinfo
            $select = $db->select()
                ->from('tbl_user', array('email', 'iduser', 'loginName', 'userkey'))
                ->where("iduser = ?", $id);

            $userinfo = $db->fetchRow($select);

            if (empty($userinfo)) {
                $this->view->errorMsg = $this->view->translate('Invalid User ID');
                return false;
            }

            if ($userinfo['userkey'] != $key) {
                $this->view->errorMsg = $this->view->translate('Key doesn\'t match. Please try again.');
                return false;
            }

            //set new pass
            if ($this->_request->isPost()) {
                $larrformData = $this->_request->getPost();

                $newpass = md5($larrformData['password']);

                $db->update('tbl_user', array('passwd' => $newpass), $db->quoteInto('iduser=?', $userinfo['iduser']));

                $this->view->Msg = $this->view->translate('Your new password has been set.');
                $this->view->sent = 1;
            }

        }elseif ($type == 1){
            $select = $db->select()
                ->from(array('a' => 'student_profile'), array('appl_email', 'id', 'appl_username', 'appl_fname', 'userKey'))
                ->where("a.id = ?", $id);

            $userinfo = $db->fetchRow($select);
//print_r($key);
            if (empty($userinfo)) {
                $this->view->errorMsg = $this->view->translate('Invalid Student ID');
                return false;
            }

            if ($userinfo['userKey'] != $key) {
                $this->view->errorMsg = $this->view->translate('Key doesn\'t match. Please try again.');
                return false;
            }

            //set new pass
            if ($this->_request->isPost()) {
                $larrformData = $this->_request->getPost();

                $newpass = md5($larrformData['password']);

                $db->update('student_profile', array('appl_password' => $newpass), $db->quoteInto('id=?', $userinfo['id']));

                $this->view->Msg = $this->view->translate('Your new password has been set.');
                $this->view->sent = 1;
            }
        }

        $this->view->form = $form;

    }

}

?>