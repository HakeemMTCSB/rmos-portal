<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
    	$auth = Zend_Auth::getInstance();
    	
    	if($auth->hasIdentity()){
    		$this->_redirect($this->view->url(array('module'=>'default','controller'=>'portal', 'action'=>'home'),'default',true));
    	}
    }
 
}

