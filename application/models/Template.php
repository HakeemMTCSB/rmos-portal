<?php

class App_Model_Template extends Zend_Db_Table
{
    protected $_name = 'comm_template';
    protected $_content = 'comm_template_content';
    protected $_tags = 'comm_template_tags';
    protected $_condition = 'comm_template_condition';

    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
    }


    public function getPaginateData($module = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('ebt' => $this->_name))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = ebt.created_by', array())
            ->joinLeft(array('ts' => 'tbl_staffmaster'), 'ts.IdStaff = u.IdStaff', array('created_by_name' => 'Fullname'))
            ->joinLeft(array('d' => 'tbl_definationms'), 'd.idDefinition=ebt.tpl_type', array('tpl_type_name' => 'd.DefinitionDesc'));

        if ($module != '') {
            $select->where('tpl_module = ?', $module);
        }

        $select->order('ebt.tpl_name');

        return $select;
    }

    public function updateTemplate($data, $where)
    {
        $db = $this->db;

        $db->update($this->_name, $data, $where);
    }

    public function addTemplate($data)
    {
        $db = $this->db;

        //insert comm_template
        $db->insert($this->_name, $data);

        $tpl_id = $db->lastInsertId();

        return $tpl_id;
    }

    public function addTemplateContent($data)
    {
        $db = $this->db;

        $db->insert($this->_content, $data);
    }

    public function updateTemplateContent($data, $where)
    {
        $db = $this->db;

        $db->update($this->_content, $data, $where);
    }

    public function getTemplates($tpl_module = '', $type = '')
    {
        $db = $this->db;

        $select = $db->select()->from($this->_name);

        if ($tpl_module != '') {
            $select->where('tpl_module = ?', $tpl_module);
        }

        if ($type != '') {
            $select->where('tpl_type = ?', $type);
        }

        $results = $db->fetchAll($select);

        return $results;
    }

    public function getTemplatesByCategory($tpl_module = '', $category = '', $limit = 0, $content = 0, $locale = 'en_US')
    {
        $db = $this->db;

        if ($content) {
            $select = $db->select()->from(array('a' => $this->_content));
            $select->joinLeft(array('b' => $this->_name), 'a.tpl_id=b.tpl_id', array('b.tpl_type', 'b.tpl_name'));
            $select->where('a.locale = ?', $locale);
        } else {
            $select = $db->select()->from(array('b' => $this->_name));
        }


        if ($tpl_module != '') {
            $select->where('b.tpl_module = ?', $tpl_module);
        }

        if ($category != '') {
            $select->where('FIND_IN_SET(?, b.tpl_categories)', $category);
        }

        if ($limit == 1) {
            $results = $db->fetchRow($select);
        } else {
            $results = $db->fetchAll($select);
        }

        return $results;
    }

    public function getTemplate($id)
    {
        $db = $this->db;

        $select = $db->select()->from($this->_name)->where('tpl_id = ?', $id);

        $results = $db->fetchRow($select);

        return $results;
    }

    public function getTemplateContent($id, $locale = '')
    {
        $db = $this->db;

        $select = $db->select()->from(array('a' => $this->_content))->where('a.tpl_id = ?', $id);
        $select->joinLeft(array('b' => $this->_name), 'a.tpl_id=b.tpl_id', array('b.tpl_type'));

        if ($locale != '') {
            $select->where('a.locale = ?', $locale);

            $results = $db->fetchRow($select);
        } else {
            $results = $db->fetchAll($select);
        }

        return $results;
    }

    public function getTemplateTags($module = '', $id = 0, $join = 0)
    {
        $db = $this->db;

        $select = $this->getTemplateTagsSelect($module, $id, $join);

        $results = $db->fetchAll($select);

        return $results;
    }

    public function getTemplateTag($id)
    {
        $db = $this->db;

        $select = $db->select()->from($this->_tags)->where('tag_id = ?', $id);

        $results = $db->fetchRow($select);

        return $results;
    }

    public function getTemplateTagsSelect($module = '', $id = 0, $join = 0, $order = null)
    {
        $db = $this->db;

        $select = $db->select()->from(array('a' => $this->_tags));

        if ($join) {
            //$select->joinLeft(array('t' => $this->_name), 't.tpl_id=a.tpl_id', array('COALESCE(t.tpl_name,"All") as tpl_name'));
        }
		
		$id = $db->quoteInto('?', $id);

        if ($id != '') {
            $select->where('a.tpl_id IN (0,' . $id . ')');
        } else {
            $select->where('a.tpl_id = 0');
        }

        if ($module != '') {
            $select->where('a.module = ?', $module);
        }

        if ($order != '') {
            $select->order($order);
        }


        return $select;
    }

    public function addTemplateTag($data)
    {
        $db = $this->db;

        $db->insert($this->_tags, $data);
    }

    public function updateTemplateTag($data, $id)
    {
        $db = $this->db;

        $db->update($this->_tags, $data, 'tag_id = ' . $id);
    }

    public function getTemplateContentBySafename($p_name, $locale = 'en_US')
    {
        $db = $this->db;

        $select = $db->select()->from(array('a' => $this->_content))->where('b.tpl_pname = ?', $p_name);
        $select->joinLeft(array('b' => $this->_name), 'a.tpl_id=b.tpl_id', array('b.tpl_type', 'b.tpl_name'));

        if ($locale != '') {
            $select->where('a.locale = ?', $locale);

            $results = $db->fetchRow($select);
        } else {
            $results = $db->fetchAll($select);
        }

        return $results;
    }

    public function addTemplateCondition($data)
    {
        $db = $this->db;

        $db->insert($this->_condition, $data);
    }

    public function getTemplateConditionSelect($module = '', $id = 0, $join = 0, $order = null)
    {
        $db = $this->db;

        $select = $db->select()->from(array('a' => $this->_condition));
        $select->joinLeft(array('t' => 'tbl_activity'), 't.idActivity = a.activity_id', array('ActivityName'))
            ->joinLeft(array('s' => 'tbl_semestermaster'), 's.IdSemesterMaster = a.semester_id', array('SemesterName' => 'SemesterMainCode'))
            ->join(array('cm' => $this->_name), 'cm.tpl_id = a.tpl_id', array('tpl_name'));

        if ($id) {
            $select->where('a.tpl_id =?', $id);
        }

        if ($module != '') {
            $select->where('a.module = ?', $module);
        }

        if ($order != '') {
            $select->order($order);
        }

        return $select;
    }

    public function getTemplateCondition($id)
    {
        $db = $this->db;

        $select = $db->select()->from($this->_condition)->where('cond_id = ?', $id);

        $results = $db->fetchRow($select);

        return $results;
    }

    public function updateTemplateCondition($data, $id)
    {
        $db = $this->db;

        $db->update($this->_condition, $data, 'cond_id = ' . $id);
    }

    public function getTemplateTagFinance($tplId, $moduleComm)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
            ->from(array("a" => "comm_template_tags"))
            ->where('a.tpl_id = ' . $tplId . ' or a.tpl_id = 0')
            ->where('a.module = ?', $moduleComm)
            ->order("a.tpl_id");

        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larrResult;
    }

}