<?php 
class App_Model_General_DbTable_Definationms extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_definationms';
	protected $_primary = "idDefinition";
	
	public function getData($id=0){
		$id = (int)$id;
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('d'=>$this->_name) ) 
	                ->where($this->_primary.' = ' .$id);			                     
        
        $row = $db->fetchRow($select);
		return $row;
		
	}
	
	public function getDataByType($idType=0){
				
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
	                ->from(array('d'=>$this->_name) ) 
	                ->where("d.idDefType = '".$idType."'");			                     
        
        $row = $db->fetchAll($select);
		return $row;
		
	}

    public function getDataByTypeCode($code=0){

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
            ->from(array('b'=>'tbl_definationms') )
            ->join(array('a' => 'tbl_definationtypems'), " a.idDefType = b.idDefType "  ,array('key' => 'b.idDefinition','value' => 'b.DefinitionCode'))
            ->where("a.defTypeDesc = ?",$code);

        $row = $db->fetchAll($select);

        return $row;

    }
	
	/*
	* getIdByDefType('DefCode','DefTypeName');
	*/
	public function getIdByDefType($defTypeDesc, $DefinitionCode){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $db->select()
						->from(array('a' => 'tbl_definationtypems'),array(''))
						->join(array('b' => 'tbl_definationms'), " a.idDefType = b.idDefType "  ,array('key' => 'b.idDefinition','value' => 'b.DefinitionCode'))
						->where("a.defTypeDesc= ?",$defTypeDesc)
						->where("b.DefinitionCode= ?",$DefinitionCode)
						->order('b.defOrder ASC');
		$larrResult = $db->fetchRow($lstrSelect);
		
		return $larrResult;
	}
	
	
}
?>