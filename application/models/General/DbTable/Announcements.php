<?php
class App_Model_General_DbTable_Announcements extends Zend_Db_Table 
{
    protected $_name = 'announcement';
	protected $_file = 'announcement_files';
	protected $_read = 'announcement_read';
	protected $_primary = "id";

	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
		$this->locale = Zend_Registry::get('Zend_Locale');
	}
	
	public function markRead($user_id, $id)
	{
		$db = $this->db;
		
		$select = $db->select()->from(array('a' => $this->_read), array('a.read_id'))
								->where('a.announcement_id = ? ',$id)
								->where('a.announcement_type = ?','studentportal')
								->where('a.user_id = ?',$user_id);

		$check = $db->fetchRow($select);

		if ( empty($check) )
		{
			$data = array(
							'announcement_id'	=> $id,
							'user_id'			=> $user_id,
							'announcement_type'	=> 'studentportal',
							'created_date'		=> new Zend_Db_Expr('NOW()')
						);
			
			$db->insert('announcement_read', $data);

			return true;
		}
		else
		{
			return false;
		}
	}

	public function getStudentAnnouncements($program_id=0, $semester_id=0,$type='studentportal',$student_id=0, $paginate=0,$hideread=1,$limit=10)
	{
		$db = $this->db;
		
		$select = $db->select()
	                ->from(array('a'=>$this->_name))
					->joinLeft(array('in'=>'tbl_intake'),'in.IdIntake=a.intake_id', array('in.IntakeDesc as intake_name'))
					->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=a.program_id', array('p.ProgramName as program_name'))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.created_by', array())
					->joinLeft(array('staff'=>'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff','FullName as created_name')
					->order('a.sticky DESC')
					->order('a.id DESC')
					->group('a.id')
					->where('FIND_IN_SET (?, a.type)', $type);

		if ($program_id!=0)
		{
			$data = array(0,$program_id);
			$select->where('a.program_id IN(?) OR a.program_id IS NULL', $data);
                        //$select->where('FIND_IN_SET('.$program_id.',a.program_id) OR a.program_id = 0');
		}
		
		//if ($semester_id!=0)
		//{
			//$data = array(0,$semester_id);
                        $date = date('Y-m-d');
			$select->where("a.semester_id IN(SELECT IdSemesterMaster FROM tbl_semestermaster WHERE SemesterMainStartDate <= '".$date."' AND SemesterMainEndDate >= '".$date."' AND IdScheme=".$semester_id.") OR a.semester_id IS NULL OR a.semester_id = 0");
		//}
		
		
		if ( $student_id !=0 )
		{
			if ( $hideread == 1 )
			{
				$select	->where('a.id NOT IN ( SELECT announcement_id FROM `announcement_read` WHERE user_id = ? AND announcement_type=\'thesis\')', $student_id)
						->where('a.sticky = 1');
			}
			$select->joinLeft(array('read'=>$this->_read), $db->quoteInto('read.announcement_id=a.id AND read.user_id=?',$student_id),array('read.read_id'));
		}

                
		$select->where('a.active = 1');

		if ( $paginate == 0 )
		{
			$select->limit($limit);
			$row = $db->fetchAll($select);
		}
		else
		{
			$row = $select;
		}
		
		
		return $row;
	}
	
	public function updateViews($id)
	{
		$db = $this->db;

		$db->update($this->_name, array('views' => new Zend_Db_Expr('views+1')), 'id='.(int)$id);
	}

	public function getData($id=0,$paginate=0)
	{
		
		$db = $this->db;
		
		$select = $db->select()
	                ->from(array('a'=>$this->_name))
					->joinLeft(array('in'=>'tbl_intake'),'in.IdIntake=a.intake_id', array('in.IntakeDesc as intake_name'))
					->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=a.program_id', array('p.ProgramName as program_name'))
					->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.created_by', array())
					->joinLeft(array('staff'=>'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff','FullName as created_name')
					->order('a.id DESC');

		$id = (int)$id;
		if ($id!=0)
		{
			$select->where('a.'.$this->_primary.' = ?',$id);
		}

		if ( $paginate == 0 )
		{
			$row = $id > 0 ? $db->fetchRow($select) : $db->fetchAll($select);
		}
		else
		{
			$row = $select;
		}
		
		return $row;
	}
	
	public function add($data)
	{
		$db = $this->db;
		
		$db->insert($this->_name, $data );
		
		$_id = $db->lastInsertId();

		return $_id;
	}

	public function updateData($data, $id)
	{
		$db = $this->db;
		$db->update($this->_name, $data, 'id='.(int)$id);
	}
	
	public function showTypes($types='')
	{
		$types = explode(',', $types);
		$new = array();

		$realtype = array(
							'sms'			=> 'SMS',
							'onapp'			=>	'Online Application',
							'studentportal'	=> 'Student Portal'
						);

		foreach ( $types as $type )
		{
			
			$new[] = $realtype[$type];
		}

		if ( count($new) == 3 )
		{
			return 'All';
		}

		return implode(', ', $new);
	}

	/*
	*	Files
	*/
	public function addFile($data)
	{
		$db = $this->db;
		
		$db->insert($this->_file, $data );
		
		$_id = $db->lastInsertId();

		return $_id;
	}
	
	public function getFile($id)
	{
		$db = $this->db;
		$select = $db->select()
								->from(array("a"=>$this->_file))
								->where('a.id = ?',$id);

		$result = $db->fetchRow($select);	
		return $result;
	}

	public function getFiles($pid)
	{
		$db = $this->db;
		$select = $db->select()
								->from(array("a"=>$this->_file))
								->where('a.announcement_id = ?',$pid);

		$result = $db->fetchAll($select);	
		return $result;
	}

	public function deleteFile($id)
	{
		$db = $this->db;

		$db->delete($this->_file, 'id='.(int)$id);
	}
        
        public function getIdSchemebasedOnReg($id){
            $db = $this->db;
            $select = $db->select()
                ->from(array('a'=>'tbl_studentregistration'), array('value'=>'b.IdScheme'))
                ->joinLeft(array('b'=>'tbl_program'), 'a.IdProgram=b.IdProgram')
                ->where('a.IdStudentRegistration = ?', $id);
            
            $result = $db->fetchOne($select);
            return $result;
        }
}
?>