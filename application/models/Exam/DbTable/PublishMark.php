<?php
class App_Model_Exam_DbTable_PublishMark extends Zend_Db_Table { 
	
	protected $_name = 'tbl_publish_mark';
	protected $_primary = 'pm_id';
	
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	public function getData($idProgram,$idSemester,$idSubject,$idGroup,$idComponent,$type) {
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select 	= $db->select()
								->from($this->_name)
								->where('pm_idProgram = ?',$idProgram)
								->where('pm_idSemester = ?',$idSemester)
								->where('pm_idSubject = ?',$idSubject)
								->where('pm_idGroup = ?',$idGroup)
								->where('pm_idComponent = ?',$idComponent)
								->where('pm_type = ?',$type);
		return $result = $db->fetchRow($select);
	}
	
	public function getPublishResult($idProgram,$idSemester) {
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select 	= $db->select()
								->from($this->_name)
								->where('pm_idProgram = ?',$idProgram)
								->where('pm_idSemester = ?',$idSemester)								
								->where('pm_type = 2');
		return $result = $db->fetchRow($select);
	}
	
	
	public function getPublishResultData($idProgram,$idSemester) {
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select 	= $db->select()
								->from(array('pm'=>$this->_name))
								->joinLeft(array('p'=>'tbl_program'),'p.IdProgram=pm.pm_idProgram',array('ProgramCode','ProgamName'=>'ArabicName'))
								->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=pm.pm_idSemester',array('SemesterName'=>'SemesterMainName'))
								->where('pm_idProgram = ?',$idProgram)
								->where('pm_idSemester = ?',$idSemester)								
								->where('pm_type = 2');
		return $result = $db->fetchAll($select);
	}
	
	
	public function publishAvailableDate($registrationId,$idProgram,$landscape=null){
		
		/* Case : Block Landascape
		 * Belum cater case jika dalam satu semester ada 2 block
		 * For now every block yg dalam semseter tu akan dipaparkan
		 */		
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$sql_sem = $db->select()
                   		  ->from(array('s' => "tbl_studentsemesterstatus"),array('IdSemesterMain')) 
                          ->where('s.IdStudentRegistration = ?',$registrationId); 		
		
		$select 	= $db->select()
						->from($this->_name)
						->where('pm_idProgram = ?',$idProgram)
						->where('pm_idSemester IN (?)',$sql_sem)
						->where('pm_date <= CURDATE()')								
						->where('pm_type = 2');
						
		return $result = $db->fetchAll($select);
	}
}
?>