<?php 

class App_Model_Exam_DbTable_ExamSlipRelease extends Zend_Db_Table_Abstract {
	
	protected $_name = 'exam_slip_release';
	protected $_primary = "esr_id";
	
	public function getData($id=null){
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
			->from(array('esr'=>$this->_name));
		
		if($id){
			$select->where('ega.ega_student_nim =?',$student_nim);
			$row = $db->fetchRow($select);
		}else{
			$row = $db->fetchAll($select);
		}
		
		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getReleaseData($semesterId,$assessmentTypeId=null){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		$select = $db ->select()
					->from(array('esr'=>$this->_name))
					->join(array('ass'=>'tbl_examination_assessment_type'), 'ass.IdExaminationAssessmentType = esr.esr_assessment_type_id')
					->where('esr_semester_id = ?', $semesterId);
		
		if($assessmentTypeId){
			$select->where('esr_assessment_type_id = ?', $assessmentTypeId);
			
			$row = $db->fetchRow($select);
		}else{
			$row = $db->fetchAll($select);
		}
	
		
	
		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getStudentExamSlipReleseData($studentId,$semesterId){
	
		$db = Zend_Db_Table::getDefaultAdapter();
	
		//student assessment type from exam group
		$sql = $db ->select()
		->from(array('egs'=>'exam_group_student'), array())
		->join(array('eg'=>'exam_group'),'eg.eg_id = egs.egst_group_id', 'DISTINCT(eg.eg_assessment_type)')
		->where('egs.egst_semester_id=?',$semesterId)
		->where('egs.egst_student_id = ?',$studentId);
		
		
		$select = $db ->select()
		->from(array('esr'=>$this->_name))
		->join(array('ass'=>'tbl_examination_assessment_type'), 'ass.IdExaminationAssessmentType = esr.esr_assessment_type_id')
		->where('esr_semester_id = ?', $semesterId)
		->where('esr_assessment_type_id in (?)',$sql);
	
		$row = $db->fetchAll($select);
		
		if($row){
			return $row;
		}else{
			return null;
		}
	}
	
	public function getAssessmentStatus($semesterId,$assessmentTypeId){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db ->select()
					  ->from(array('esr'=>$this->_name),array('esr_status'))
						->where('esr_semester_id = ?', $semesterId)
						->where('esr_assessment_type_id = ?', $assessmentTypeId);
		
		$row = $db->fetchRow($select);
		
		if($row){
			return $row['esr_status'];
		}else{
			return null;
		}
	}
	
}