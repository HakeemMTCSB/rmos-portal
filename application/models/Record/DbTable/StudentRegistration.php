<?php


class App_Model_Record_DbTable_StudentRegistration extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_studentregistration';
	protected $_primary = "IdStudentRegistration";
	
	
	public function getData($appl_id=0) {
		
		if($appl_id!=0){
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
					->from(array('sr'=>$this->_name))					
					->where('sr.IdApplication = ?',$appl_id)
					->where('sr.profileStatus = 92'); //Active
					
			$stmt = $db->query($select);						
			$row = $stmt->fetchRow();
			
		}else{
			$row = $this->fetchAll();
			$row=$row->toArray();
		}
		
		if(!$row){
			throw new Exception("There is No Information Found");
		}
		return $row;
	}
	
	public function getDataFromProfile($sp_id) {
	
		$db = Zend_Db_Table::getDefaultAdapter();
		
		$select = $db->select()
				->from(array('sr'=>$this->_name))
				->where('sr.sp_id = ?',$sp_id)
				->where('sr.profileStatus = 92'); //Active
				
		$stmt = $db->query($select);
		$row = $stmt->fetchAll();
	
		return $row;
	}
	
	
	public function getStudentInfo($id=0,$by='sr.IdStudentRegistration') {
			
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
				->from(array('sr'=>$this->_name))
				->join(array('ap'=>'student_profile'),'ap.id=sr.sp_id')
				->join(array('p'=>'tbl_program'),'p.IdProgram=sr.IdProgram',array('ArabicName','ProgramName','ProgramCode','IdProgram','IdScheme'))
				->join(array('s'=>'tbl_scheme'),'p.IdScheme=s.IdScheme', array('SchemeCode','EnglishDescription as SchemeName'))
				->join(array('i'=>'tbl_intake'),'i.IdIntake=sr.IdIntake',array('intake'=>'IntakeDefaultLanguage','IntakeId'))
				->joinLeft(array('d'=>'tbl_definationms'),'d.idDefinition=sr.profileStatus',array('StudentStatus'=>'DefinitionDesc'))
				->joinLeft(array('pm'=>'tbl_programmajoring'),'pm.IDProgramMajoring=sr.IDProgramMajoring',array('majoring'=>'BahasaDescription'))
				->joinLeft(array('b'=>'tbl_branchofficevenue'),'b.IdBranch=sr.IdBranch',array('BranchName','invoice'))
				->joinLeft(array('c'=>'tbl_collegemaster'),'c.IdCollege=p.IdCollege',array('CollegeName'=>'ArabicName','c.IdCollege'))
				->joinLeft(array('sm'=>'tbl_staffmaster'),'sm.IdStaff=sr.AcademicAdvisor',array('AcademicAdvisor'=>'FullName',"FrontSalutation","BackSalutation"))
				->join(array("aps" => "tbl_program_scheme"),'aps.IdProgramScheme=sr.IdProgramScheme', array('*'))
				->joinLeft(array("ds" => "tbl_definationms"),'ds.idDefinition=aps.mode_of_study', array('StudyMode'=>'ds.DefinitionDesc'))
                ->joinLeft(array("dss" => "tbl_definationms"),'dss.idDefinition=aps.mode_of_program', array('ProgramMode'=>'dss.DefinitionDesc'))
                ->joinLeft(array('city'=>'tbl_city'),'city.idCity=ap.appl_city',array('CityName'))
		        ->joinLeft(array('state'=>'tbl_state'),'state.IdState=ap.appl_state',array('StateName'))
		        ->joinLeft(array('ctrs'=>'tbl_countries'),'ctrs.idCountry=ap.appl_country',array('CountryName'))
                      
				->where($by.' = ?',$id)
				->where('sr.profileStatus = 92'); //Active
		

        $row = $db->fetchRow($select);				
		
		return $row;
	}
	
	
     /*
	 * This function to get course registered by semester.
	 */
	public function getCourseRegisteredBySemester($registrationId,$idSemesterMain,$idBlock=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                        ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                        ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
                        ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))                     
                        ->where('sr.IdStudentRegistration = ?', $registrationId)
                        //->where("srs.subjectlandscapetype != 2")
			->where('srs.IdSemesterMain = ?',$idSemesterMain);   
                                           
        if(isset($idBlock) && $idBlock != ''){ //Block 
        	$sql->where("srs.IdBlock = ?",$idBlock);
        	$sql->order("srs.BlockLevel");
        }  

     
        //echo $sql;     
        $result = $db->fetchAll($sql);
        return $result;
	}
	
	/*
	 * Get semester regular for particular semester
	 */
	function getSemesterRegular($idsemmain,$idstudreg){
		$db = Zend_Db_Table::getDefaultAdapter();
		$sql = "SELECT * FROM `tbl_semestermaster` sm
		inner join tbl_studentsemesterstatus as sss on sss.IdSemesterMain=sm.IdSemesterMaster
		inner join(
		select idacadyear,semestercounttype from tbl_semestermaster where idsemestermaster=".$idsemmain." )semmaster
			on sm.idacadyear=semmaster.idacadyear and sm.semestercounttype=semmaster.semestercounttype
			WHERE
			sss.idstudentregistration = $idstudreg and
			semesterfunctiontype = 0
			";
			$row = $db->fetchRow($sql);
			return $row;
	}
	
	
	/*
	 * This function to get course registered by semester.
	 */
	public function getCourseRegisteredByBlock($registrationId,$idBlock=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		 $sql = $db->select()
                        ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                        ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')   
                        ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))                  
                        ->where('sr.IdStudentRegistration  = ?', $registrationId)
                        ->where("srs.IdBlock = ?",$idBlock)
                        ->order("srs.BlockLevel");
                      
        $result = $db->fetchAll($sql);
        return $result;
	}
	
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	
	public function getCourseRegisteredBySemesterBlock($registrationId,$idSemester,$idBlock=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		 $sql = $db->select()
                        ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                        ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')   
                        ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))                  
                        ->where('sr.IdStudentRegistration  = ?', $registrationId)
                        ->where("srs.IdSemesterMain = ?",$idSemester)
                        ->where("srs.IdBlock = ?",$idBlock)
                        ->where("srs.subjectlandscapetype != 2")
                        ->order("srs.BlockLevel");
                                              
        $result = $db->fetchAll($sql);
        return $result;
	}
    
    public function getCourseRegisteredBySemesterId($registrationId,$idSemesterMain,$idBlock=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		/*$sql = $db->select('SUM CreditHours AS Total')
                        ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                        ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
                        ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))                     
                        ->where('sr.IdStudentRegistration = ?', $registrationId)
                        ->where('srs.IdSemesterMain IN ('.$idSemesterMain.')')   
                        ->where('srs.subjectlandscapetype != 2');*/   
		$sql = $db->select()
						->distinct()
                        ->from(array('sr' => 'tbl_studentregistration'), array())  
                        ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration',array())
                        ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('CreditHours','SubCode'))                     
                        ->where('sr.IdStudentRegistration = ?', $registrationId)
                        ->where('srs.IdSemesterMain IN ('.$idSemesterMain.')');  
                        //->where('srs.subjectlandscapetype != 2');
                                                                   
        if(isset($idBlock) && $idBlock != ''){ //Block 
        	$sql->where("srs.IdBlock = ?",$idBlock);
        	$sql->order("srs.BlockLevel");
        }  

     //echo $sql;
             
        $result = $db->fetchAll($sql);
       return $result;
	}
	
    public function getSubjectSchedule($id,$idGroup)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $sql = $db->select()
                ->from(array('a' => 'tbl_course_tagging_group'))
                ->joinRight(array('b' => 'course_group_schedule'),'b.idGroup = a.IdCourseTaggingGroup')
                ->where('a.idSubject = ?',(int)$id)
                ->where('a.IdCourseTaggingGroup = ?',(int)$idGroup);
        //echo $sql;
        $row = $db->fetchAll($sql);
        
        return $row;
    }
    
    public function getInfo($idGroup){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('cg'=>'tbl_course_tagging_group'))
					  ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('subject_code'=>'SubCode','subject_name'=>'subjectMainDefaultLanguage','faculty_id'=>'IdFaculty'))
					  ->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
					  ->where('IdCourseTaggingGroup = ?',$idGroup);					  
		 $row = $db->fetchRow($select);	
		 return $row;
	}
    
    public function getTotalStudentRegistered($idSubject,$idSemester,$idGroup)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql =   $db->select()
                    ->from('tbl_studentregsubjects',array('Total' => ('COUNT(*)')))
                    //->columns()
                    ->where('IdSemesterMain = ?',(int)$idSemester)
                    ->where('IdCourseTaggingGroup = ?',(int)$idGroup)
                    ->where('IdSubject = ?',(int)$idSubject);
                    
        $row =  $db->fetchRow($sql);
        //echo $sql;
        return $row;
    }
}