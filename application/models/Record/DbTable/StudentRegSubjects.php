<?php


class App_Model_Record_DbTable_StudentRegSubjects extends Zend_Db_Table_Abstract {
	/**
	 * The default table name 
	 */
	protected $_name = 'tbl_studentregsubjects';
	protected $_primary = "IdStudentRegSubjects";
	
	
	public function getActiveRegisteredCourse($idSemesterMain,$IdStudentRegistration){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		 $sql = $db->select()
                        ->from(array('sr' => 'tbl_studentregistration'), array('registrationId'))  
                        ->joinLeft(array('srs'=>'tbl_studentregsubjects'),'srs.IdStudentRegistration = sr.IdStudentRegistration')
                        ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))   
                        ->joinLeft(array('cgs'=>'course_group_schedule'),'idGroup=srs.IdCourseTaggingGroup')                  
                        ->where('sr.IdStudentRegistration = ?', $IdStudentRegistration)
                        ->where('srs.IdSemesterMain = ?',$idSemesterMain)
                        ->where('srs.Active=1')
                        ->where('srs.subjectlandscapetype != 2');
                        
                        
                        
       $sql .= 					"ORDER BY CASE cgs.sc_day 
                                 WHEN 'Monday' THEN 1
                                 WHEN 'Tuesday' THEN 2
                                 WHEN 'Wednesday' THEN 3
                                 WHEN 'Thursday' THEN 4
                                 WHEN 'Friday' THEN 5
                                 WHEN 'Saturday' THEN 6
                                 WHEN 'Sunday' THEN 7
                                 ELSE 8
                                 END ";
                          
         
        $result = $db->fetchAll($sql);
        return $result;
	}
	
	public function getSemesterSubjectRegistered($idSemesterMain,$IdStudentRegistration,$active=1, $subjectType=array(1,3)){
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$sql = $db->select()
		->from(array('srs'=>'tbl_studentregsubjects'))
		->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject',array('SubjectName','subjectMainDefaultLanguage','BahasaIndonesia','CreditHours','SubCode'))
		->where('srs.IdStudentRegistration = ?', $IdStudentRegistration)
		->where('srs.IdSemesterMain = ?',$idSemesterMain)
		->where('srs.Active=?',$active)
		->where('srs.subjectlandscapetype in (?)', $subjectType);
			
		$result = $db->fetchAll($sql);
		return $result;
	
	}
	
	
	public function getTotalCreditHoursActiveRegisteredCourse($idSemesterMain,$IdStudentRegistration){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                        ->from(array('srs' => 'tbl_studentregsubjects'),array())   
                        ->join(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=srs.IdSubject', array('total'=>new Zend_Db_Expr('SUM(CreditHours)')))                       
                        ->where('srs.IdStudentRegistration = ?', $IdStudentRegistration)
                        ->where('srs.IdSemesterMain = ?',$idSemesterMain)
                        ->where('srs.Active=1')
                        ->where('srs.subjectlandscapetype != 2');
                                                         
        $result = $db->fetchRow($sql);
        return $result["total"];
	}
	
	public function getSubject($idSemesterMain,$IdStudentRegistration,$idSubject){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                        ->from(array('srs' => 'tbl_studentregsubjects'),array())                                            
                        ->where('srs.IdStudentRegistration = ?', $IdStudentRegistration)
                        ->where('srs.IdSemesterMain = ?',$idSemesterMain)
                        ->where('srs.IdSubject = ?',$idSubject);
         
        $result = $db->fetchRow($sql);
        
	}
	
	
	public function isRegister($IdStudentRegistration,$idSubject,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 
		$sql = $db->select()
                        ->from(array('srs' => 'tbl_studentregsubjects'))                                            
                        ->where('srs.IdStudentRegistration = ?', $IdStudentRegistration)                        
                        ->where('srs.IdSubject = ?',$idSubject)
                        ->where('srs.IdSemesterMain = ?',$idSemester);
         
        return $result = $db->fetchRow($sql);
        
	}
	
	public function addData($data){		
		$this->insert($data);
        
        $this->addStudentMapping($data);
	}
	
	
	//to list subject yg pernah diregister and exam status completed and subject tu offer pada semester pembaikan (atau semester yg dipilih)
	public function getSubjectRegisterPreviousSemester($IdStudentRegistration,$landscape_id,$semester_id,$landscape_type,$subject_code){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 	
		//semester based
		if($landscape_type==43){
			
			 $sql = $db->select()
                        ->from(array('srs' => 'tbl_studentregsubjects'))    
                        ->join(array("s"=>"tbl_subjectmaster"),'s.IdSubject=srs.IdSubject',array('SubjectName','BahasaIndonesia','SubCode','CreditHours'))
                        ->join(array('ls'=>'tbl_landscapesubject'),'ls.IdSubject=srs.IdSubject',array())
                        ->joinLeft(array("d"=>"tbl_definationms"),'d.idDefinition=ls.SubjectType',array('SubjectType'=>'DefinitionDesc')) 
                        ->join(array("so"=>'tbl_subjectsoffered'),'so.IdSubject=srs.IdSubject',array())
                        ->where('srs.IdStudentRegistration = ?', $IdStudentRegistration)    
                        ->where("srs.exam_status = 'C' ") //status exam mesti C=completed
                        ->where('so.IdSemester = ?',$semester_id) //offer pada semester ini
                        ->where('ls.IdLandscape = ?',$landscape_id)    
                        ->order('s.SubCode') 
                        ->group('srs.IdSubject');
                        
			 if(isset($subject_code) && $subject_code!=''){
			  	$sql->where("s.SubCode LIKE '%".$subject_code."%'");
			  }
		}
		

       	//landscape based	
		if($landscape_type==44){
			
			 //untuk semester pembaikan display yang bujang dan anak sahaja.bapak x perlu
			 
			 $sql = $db->select()
                        ->from(array('srs' => 'tbl_studentregsubjects'))    
                        ->join(array("s"=>"tbl_subjectmaster"),'s.IdSubject=srs.IdSubject',array('SubjectName','BahasaIndonesia','SubCode','CreditHours'))
                        ->join(array("so"=>'tbl_subjectsoffered'),'so.IdSubject=srs.IdSubject',array())
                        ->join(array('ls'=>'tbl_landscapeblocksubject'),'ls.subjectid=srs.IdSubject',array())
                        ->join(array('lb'=>'tbl_landscapeblock'),'lb.idblock=ls.blockid',array('block_level'=>'block'))
                        ->join(array("d"=>"tbl_definationms"),'d.idDefinition=ls.coursetypeid',array('SubjectType'=>'DefinitionDesc')) 
                        ->where('srs.IdStudentRegistration = ?', $IdStudentRegistration)    
                        ->where("srs.exam_status = 'C' ")  //status exam mesti C=completed
                        ->where('(ls.type = 3 OR ls.type = 1)') //bujang atau anak  sahaja                  
                        ->where('so.IdSemester = ?',$semester_id) //offer pada semester ini
                        ->where('ls.IdLandscape = ?',$landscape_id)   
                        ->order('s.SubCode')                    
                        ->group('srs.IdSubject');   

		 	  if(isset($subject_code) && $subject_code!=''){
			  	$sql->where("s.SubCode LIKE '%".$subject_code."%'");
			  }
        }
       				
        $result =  $db->fetchAll($sql);
        

		foreach($result as $key=>$row){
		  				
			    //setkan child=='' sebab guna jquery loop yg sama kalo x nanti display ada bugs
		 	     $result[$key]['child'] = '';  
			    
				//get subject status already taken/registered or not
	         	$subjectRegDB = new App_Model_Record_DbTable_StudentRegSubjects();
	         	$subject_registered = $subjectRegDB->isRegister($IdStudentRegistration,$row["IdSubject"],$semester_id);	
	         	
	         	if(is_array($subject_registered)){
	         		$result[$key]['register_status']="Registered";
	         		$result[$key]['register']=1;
	         	} else{
	         		$result[$key]['register_status']="Not Registered";
	         		$result[$key]['register']=2;
	         	}  
		  }
		  
        return $result;
        
	}
	
	function drop($sregid){
		$db = Zend_Db_Table::getDefaultAdapter();
		
        $sql = $db->select()
               ->from($this->_name)
               ->where('IdStudentRegSubjects = ?', (int)$sregid);
        $data = $db->fetchRow($sql);
        
        $db->delete($this->_name,"IdStudentRegSubjects = $sregid");
	
        $this->dropStudentMapping($data);
    }
	
	public function getSubjectOfferedReg($IdStudentRegistration,$landscape_id,$semester_id,$landscape_type,$subject_code){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 	
		//semester based
		if($landscape_type==43){
			
			 $sql = $db->select()
                        ->from(array("s"=>"tbl_subjectmaster"),array('SubjectName','BahasaIndonesia','SubCode','CreditHours','IdSubject'))
                        ->join(array('ls'=>'tbl_landscapesubject'),'ls.IdSubject=s.IdSubject',array('IdLandscapeSub'))
                        ->joinLeft(array("d"=>"tbl_definationms"),'d.idDefinition=ls.SubjectType',array('SubjectType'=>'DefinitionDesc')) 
                        ->join(array("so"=>'tbl_subjectsoffered'),'so.IdSubject=s.IdSubject',array())
                        ->where('so.IdSemester = ?',$semester_id) //offer pada semester ini
                        ->where('ls.IdLandscape = ?',$landscape_id)    
                        ->order('s.SubCode') 
                        ->group('s.IdSubject');
                        
			 if(isset($subject_code) && $subject_code!=''){
			  	$sql->where("s.SubCode LIKE '%".$subject_code."%'");
			  }
		}
		
		
		if($landscape_type==44){
			
			 $sql = $db->select()
                        ->from(array("s"=>"tbl_subjectmaster"),array('SubjectName','BahasaIndonesia','SubCode','CreditHours','IdSubject'))
                        ->join(array('ls'=>'tbl_landscapeblocksubject'),'ls.subjectid=s.IdSubject',array('blockid'))
                        ->joinLeft(array("d"=>"tbl_definationms"),'d.idDefinition=ls.coursetypeid',array('SubjectType'=>'DefinitionDesc')) 
                        ->join(array("so"=>'tbl_subjectsoffered'),'so.IdSubject=s.IdSubject',array())
                        ->where('so.IdSemester = ?',$semester_id) //offer pada semester ini
                        ->where('ls.IdLandscape = ?',$landscape_id)    
                        ->order('s.SubCode') 
                        ->group('s.IdSubject');
                        
			 if(isset($subject_code) && $subject_code!=''){
			  	$sql->where("s.SubCode LIKE '%".$subject_code."%'");
			  }
		}
		
	    $result =  $db->fetchAll($sql);
        
	   
        if(count($result)>0){
		foreach($result as $key=>$row){
		  				
			    //setkan child=='' sebab guna jquery loop yg sama kalo x nanti display ada bugs
		 	     $result[$key]['child'] = '';  
			    
				//get subject status already taken/registered or not
	         	$subjectRegDB = new App_Model_Record_DbTable_StudentRegSubjects();
	         	$subject_registered = $subjectRegDB->isRegister($IdStudentRegistration,$row["IdSubject"],$semester_id);	
	         	
	         	if(is_array($subject_registered)){
	         		$result[$key]['register_status']="Registered";
	         		$result[$key]['register']=1;
	         	} else{
	         		$result[$key]['register_status']="Not Registered";
	         		$result[$key]['register']=2;
	         	}  
		  }
        }
		  
        return $result;
	}
	

	
	public function getRegSubjectBySemId($IdStudentRegistration,$IdSemester,$landscape){
		
		 $db = Zend_Db_Table::getDefaultAdapter();
		 $sql = $db->select()
                        ->from(array('srs' => 'tbl_studentregsubjects'))    
                        ->join(array("s"=>"tbl_subjectmaster"),'s.IdSubject=srs.IdSubject',array('SubjectName','BahasaIndonesia','SubCode','CreditHours'))
                        ->where('srs.IdSemesterMain = ?',$IdSemester)
                        ->where('srs.IdStudentRegistration = ?',$IdStudentRegistration)
                        ->where('srs.subjectlandscapetype !=2');
                        
                        
         if($landscape["LandscapeType"]==43){         	
         	 $sql->join(array('ls'=>'tbl_landscapesubject'),'ls.IdSubject=s.IdSubject',array());
         	 $sql->where('ls.IdLandscape= ?',$landscape['IdLandscape']);        	
         } 
         
         if($landscape["LandscapeType"]==44){         	
         	 $sql->join(array('ls'=>'tbl_landscapeblocksubject'),'ls.subjectid=s.IdSubject',array());         
         	 $sql->where('ls.IdLandscape= ?',$landscape['IdLandscape']); 	
         } 
         
         $sql->join(array('pr'=>'tbl_programrequirement'),'pr.IdProgramReq=ls.IdProgramReq',array());
         $sql->join(array("d"=>"tbl_definationms"),'d.idDefinition=pr.SubjectType',array('SubjectType'=>'DefinitionDesc'));
       
         //echo $sql;
		 $result =  $db->fetchAll($sql);
		 return $result;
	}

	public function isCompleted($IdStudentRegistration,$idSubject,$grade=null,$type=null,$total_credit=null){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		if(($grade==null)){ 
			$sql = $db->select()
	                        ->from(array('srs' => 'tbl_studentregsubjects'))                                            
	                        ->where('srs.IdStudentRegistration = ?', $IdStudentRegistration)                        
	                        ->where('srs.IdSubject = ?',$idSubject)
	                        ->where('srs.exam_status = ?','C')
	                        ->where('srs.grade_status = ?','Pass');
	          $result = $db->fetchRow($sql);
			 if(!empty($result)){
	         	return true;
	         }else{
	         	return false;
	         }
	         
		}else{
			
			if($type==2){ //Total Credit Hour
				
				if($total_credit >= $grade){
					return true;
				}else{
					return false;
				}
				
			}else{ //Pass With Grade
				
				//ni sepatutnya check kat setup tapi urgent hardcode sini dulu
				$gpoint["C"]=2.00;
				$gpoint["C+"]=2.50;
				$gpoint["B-"]=2.75;
				$gpoint["B"]=3.00;
				$gpoint["B+"]=3.50;
				$gpoint["A"]=4.00;
				$gpoint["A-"]=3.75;
				$gpoint["D"]=1.00;
				$gpoint["E"]=0.00;
				if(isset($gpoint[$grade])){
				$sql = $db->select()
			                        ->from(array('srs' => 'tbl_studentregsubjects'))                                            
			                        ->where('srs.IdStudentRegistration = ?', $IdStudentRegistration)                        
			                        ->where('srs.IdSubject = ?',$idSubject)
			                        ->where('srs.exam_status = ?','C')
			                        ->where('srs.grade_point >= ?',$gpoint[$grade]);
			         $result = $db->fetchRow($sql);
			         
			         if(!empty($result)){
			         	return true;
			         }else{
			         	return false;
			         }
				}else{
					return false;
				}			
			}
		}
	}

    public function addStudentMapping($data)
    {
        $Tagging = new Zend_Db_Table('tbl_course_group_student_mapping');
        $add = array(
            'IdCourseTaggingGroup' => $data['IdCourseTaggingGroup'],
            'IdStudent' => $data['IdStudentRegistration']
        );
        
        $Tagging->insert($add);
    }
    
    public function dropStudentMapping($data)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $delete = array(
            'IdCourseTaggingGroup = ?' => $data['IdCourseTaggingGroup'],
            'IdStudent = ?' => $data['IdStudentRegistration']
        );
        
        //$Tagging = new Zend_Db_Table('tbl_course_group_student_mapping');
       $db->delete('tbl_course_group_student_mapping',$delete);
    }

}


?>