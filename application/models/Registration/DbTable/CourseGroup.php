<?php 

class App_Model_Registration_DbTable_CourseGroup extends Zend_Db_Table_Abstract {
	
	protected $_name = 'tbl_course_tagging_group';
	protected $_primary = "IdCourseTaggingGroup";
	
	
	public function addData($data){		
	   $id = $this->insert($data);
	   return $id;
	}
	
	public function updateData($data,$id){
		 $this->update($data, $this->_primary .' = '. (int)$id);
	}
	
	public function deleteData($id){		
	  $this->delete($this->_primary .' =' . (int)$id);
	}
	
	
	public function getGroupList($idSubject,$idSemester,$idprogram){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))
					  ->joinLeft(array('s'=>'tbl_semestermaster'),'s.IdSemesterMaster=cg.IdSemester',array('semester_name'=>'SemesterMainName'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
					  ->joinRight(array('cgp'=>'course_group_program'),'cgp.group_id=cg.IdCourseTaggingGroup')
					 // ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('subject_code'=>'SubCode','subject_name'=>'subjectMainDefaultLanguage'))
					  ->where('IdSubject = ?',$idSubject)
					  ->where('IdSemester = ?',$idSemester)					  
					  ->where('cgp.program_id = ?',$idprogram);					  
		 $row = $db->fetchAll($select);	
		 return $row;
	}
	
	public function getTotalGroupByCourse($idCourse,$idSemester){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from($this->_name)
					  ->where("IdSubject = ?",$idCourse)
					  ->where('IdSemester = ?',$idSemester);					  
		 $row = $db->fetchAll($select);	
		 
		 if($row)
		 	return count($row);
		 else
		 return 0;
	}
	
	public function getInfo($idGroup){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		$select = $db ->select()
					  ->from(array('cg'=>$this->_name))
					  ->joinLeft(array('sm'=>'tbl_subjectmaster'),'sm.IdSubject=cg.IdSubject',array('subject_code'=>'SubCode','subject_name'=>'subjectMainDefaultLanguage'))
					  ->joinLeft(array('stm'=>'tbl_staffmaster'),'stm.IdStaff=cg.IdLecturer',array('FrontSalutation','FullName','BackSalutation'))
					  ->where('IdCourseTaggingGroup = ?',$idGroup);					  
		 $row = $db->fetchRow($select);	
		 return $row;
	}
	
	public function getStudentCourseGroup($IdStudentRegistration,$idSemester,$idSubject){
		
		$db = Zend_Db_Table::getDefaultAdapter();		
		
		 $select = $db ->select()		
					  ->from(array('cgsm'=>'tbl_course_group_student_mapping'))
					  ->join(array('ctg'=>'tbl_course_tagging_group'),'ctg.IdCourseTaggingGroup=cgsm.IdCourseTaggingGroup')
					  ->where('cgsm.IdStudent = ?',$IdStudentRegistration)
					  ->where('ctg.IdSemester = ?',$idSemester)
					  ->where('ctg.IdSubject = ?',$idSubject);					  
		 $row = $db->fetchRow($select);	
		 
		 return $row;
	}
	
}