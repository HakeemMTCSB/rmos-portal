<?php
class App_Model_Common {

	public function fnPagination($larrresult,$page,$lintpagecount) { // Function for pagination
		$paginator = Zend_Paginator::factory($larrresult); //instance of the pagination
		$paginator->setItemCountPerPage($lintpagecount);
		$paginator->setCurrentPageNumber($page);
		return $paginator;
	}

	public function fnGetStaff($idStaff){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								->from(array("a"=>"tbl_staffmaster"),array("a.*") )
								->where('a.IdStaff = ?',$idStaff);
								
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}


}