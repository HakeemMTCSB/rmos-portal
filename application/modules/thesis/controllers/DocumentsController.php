<?php

class Thesis_DocumentsController extends  Zend_Controller_Action
{
	public function init() {
		
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		
		$this->defModel = new App_Model_General_DbTable_Definationms();
		$this->emailModel = new App_Model_Email();
		$this->thesisModel = new Thesis_Model_DbTable_General();
		$this->regModel = new Thesis_Model_DbTable_Registration();
		$this->docModel = new Thesis_Model_DbTable_Documents();
		$this->trackModel = new Thesis_Model_DbTable_Tracking();
		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
													  );

		
		$this->auth = Zend_Auth::getInstance();
		$this->uploadDir = DOCUMENT_PATH.'/thesis';

		$this->userId = $this->view->userId = $this->auth->getIdentity()->id;
		$this->userInfo = $this->view->userInfo = $this->auth->getIdentity()->info;

		Zend_Layout::getMvcInstance()->assign('navActive', 'documents');
	}
	
	public function indexAction()
	{
		$this->view->title = 'Documents';

		$documents = $this->docModel->getFiles( $this->auth->getIdentity()->role );

		$this->view->documents = $documents;
	}

	public function viewAction()
	{
		$this->view->title = 'Documents';

		$id = $this->_getParam('id');
		
		$info = $this->thesisModel->getDownloadSingle($id);


		$files = $this->view->files = $this->regModel->getFiles($info['id'], 'download');

		
		$this->view->id = $id;
		$this->view->info = $info;
		$this->view->files = $files;
	}
}