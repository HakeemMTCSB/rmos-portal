<?php

class Thesis_ApprovalController extends Zend_Controller_Action
{
    public function init()
    {

        $session = new Zend_Session_Namespace('sis');
        $this->session = $session;

        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->emailModel = new App_Model_Email();
        $this->thesisModel = new Thesis_Model_DbTable_General();
        $this->regModel = new Thesis_Model_DbTable_Registration();

        $this->trackModel = new Thesis_Model_DbTable_Tracking();
        //locale setup
        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->locales = $this->view->locales = array(
            'en_US' => 'English',
            'ms_MY' => 'Malay'
        );


        $this->auth = Zend_Auth::getInstance();
        $this->uploadDir = DOCUMENT_PATH . '/thesis';

        $this->userId = $this->view->userId = $this->auth->getIdentity()->iduser;
        $this->userInfo = $this->view->userInfo = $this->auth->getIdentity()->info;

        Zend_Layout::getMvcInstance()->assign('navActive', 'approval');
    }

    /*
        Final Submission Approval
    */
    public function finalAction()
    {
        $this->view->title = 'Final Submission Approval';

        //check submission
        $results = $this->thesisModel->getSubmissionSupervisee($this->userId, 'final');

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $i = 0;

            $count = count($formData['app']);

            if ($count > 0) {
                foreach ($formData['app'] as $app_id) {

                    if ($this->_request->getPost('approve')) {
                        $data = array(
                            'approved_date' => new Zend_Db_Expr('NOW()'),
                            'approved_id' => $this->auth->getIdentity()->iduser,
                            'approved_role' => $this->auth->getIdentity()->role,
                            'approved' => 1
                        );

                        $this->thesisModel->updateSubmission($data, array('id = ?' => $app_id));

                        $this->_helper->flashMessenger->addMessage(array('success' => "Final Submission(s) successfully approved"));
                    } elseif ($this->_request->getPost('reject')) {

                        $data = array(
                            'rejected_date' => new Zend_Db_Expr('NOW()'),
                            'rejected_id' => $this->auth->getIdentity()->iduser,
                            'rejected_role' => $this->auth->getIdentity()->role,
                            'approved' => 2
                        );

                        $this->thesisModel->updateSubmission($data, array('id = ?' => $app_id));

                        $this->_helper->flashMessenger->addMessage(array('success' => "Final Submission(s) successfully rejected"));
                    }


                }


                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'approval', 'action' => 'final'), 'default', true));
            }

            $i++;
        }


        //views
        $this->view->results = $results;
    }

    /*
        Pre Submission Approval
    */
    public function preAction()
    {
        $this->view->title = 'Approval';

        //check submission
        $results = $this->thesisModel->getSubmissionSupervisee($this->userId, 'pre');

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $i = 0;

            $count = count($formData['app']);

            if ($count > 0) {
                foreach ($formData['app'] as $app_id) {

                    echo $app_id;
                    $research = $this->regModel->getProposal($app_id);

                    //upload file
                    try {

                        $uploadDir = DOCUMENT_PATH . '/thesis/' . $research['student_id'];

                        if (!is_dir($uploadDir)) {
                            if (mkdir_p($uploadDir) === false) {
                                throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                            }
                        }

                        $adapter = new Zend_File_Transfer_Adapter_Http();

                        $files = $adapter->getFileInfo();

                        $adapter->addValidator('NotExists', false, $uploadDir);
                        $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                        foreach ($files as $no => $fileinfo) {

                            $pid = (explode("_", $no));
                            if ($pid[1] == $app_id ) {

                                if($fileinfo['name'] == '') {
                                    $this->_helper->flashMessenger->addMessage(array('error' => "Please attach file for this student"." | Name : ".$research['student_name']." | Activity : ".$research['DefinitionDesc']));
                                    $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'approval', 'action' => 'pre'), 'default', true));
                                }

                                $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                                if ($adapter->isUploaded($no)) {
                                    $ext = getext($fileinfo['name']);

                                    $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                                    $fileUrl = $uploadDir . '/' . $fileName;

                                    $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                                    $adapter->setOptions(array('useByteString' => false));

                                    $size = $adapter->getFileSize($no);

                                    if (!$adapter->receive($no)) {
                                        $errmessage = array();
                                        if (is_array($adapter->getMessages())) {
                                            foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                                $errmessage[] = $errmsg;
                                            }

                                            throw new Exception(implode('<br />', $errmessage));
                                        }
                                    }

                                    $datetime = new Zend_Db_Expr('NOW()');
                                    $user = $this->auth->getIdentity()->iduser;

                                    //save file into db
                                    $data2 = array(
                                        'ef_pid' => $research['p_id'],
                                        'type' => 'approval',
                                        'ef_student_id' => $research['student_id'],
                                        'ef_created_by' => $user,
                                        'ef_created_at' => $datetime,
                                        'ef_fileurl' => '/thesis/' . $research['student_id'] . '/' . $fileName,
                                        'ef_filename' => $fileinfo['name'],
                                        'ef_filesize' => $size
                                    );

                                    $lastid = $this->regModel->addEvaluationFile($data2);

                                } //isuploaded
                            }
                        } //foreach
                    } catch
                    (Exception $e) {
                        throw new Exception($e->getMessage());
                    }

                    if ($this->_request->getPost('approve')) {

                        $pd_status = 'approved';
                        $status = $this->thesisModel->getStatusByCode($pd_status, 1);
                        $data = array(
                            'approved_date' => new Zend_Db_Expr('NOW()'),
                            'approved_by' => $this->auth->getIdentity()->iduser,
                            'pd_update_by' => $this->auth->getIdentity()->iduser,
                            'pd_updated_date' => new Zend_Db_Expr('NOW()'),
                            'pd_status' => $status,
                        );

                        //send registered email

                        $emailDb = new App_Model_Email();
                        $commDb = new App_Model_Template();

//                        $research = $this->regModel->getProposal($app_id);

                        if($research['DefinitionCode'] == 'viva') {
                            $gettemplate = $commDb->getTemplatesByCategory('thesis', 'proposal-approved-viva', 0, 1);
                        }else{
                            $gettemplate = $commDb->getTemplatesByCategory('thesis', 'proposal-approved', 0, 1);
                        }
                        $template = $gettemplate[0];

                        $research['research_type'] = 'proposal';

                        if (!isset($research)) {
                            throw new Exception('Invalid Research');
                        }

                        if (empty($template)) {
                            throw new Exception('Invalid Template. Did you change the template name recently?');
                        }

                        //get supervisors
                        $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($research['p_id'], 'proposal');

                        $supervisor_name = array();

                        foreach ($supervisors as $key => $sup_name) {
                            $newkey = $key + 1;
                            $supervisor_name[] = $newkey . '. '.$sup_name['supervisor_name'];
                        }

                        $sname = "<p>" . implode('<br>', $supervisor_name) . "</p>";

                        $research['supervisor_info'] = $sname;

                        //email
                        $dataEmail = array(
                            'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $research['student_email'],
                            'subject' => $template['tpl_name'],
                            'content' => Thesis_Model_DbTable_Registration::parseContent($research, $template['tpl_content']),
                            'date_que' => date('Y-m-d H:i:s')
                        );

                        $emailDb->add($dataEmail);
                        //send registered email

                        //send pd notification

                        if($research['DefinitionCode'] == 'viva'){
                            $pdtemplate = $commDb->getTemplatesByCategory('thesis', 'director-approval-viva', 0, 1);
                        }else{
                            $pdtemplate = $commDb->getTemplatesByCategory('thesis', 'director-approval', 0, 1);
                        }

                        $template2 = $pdtemplate[0];

                        $research['research_type'] = 'proposal';

                        if (empty($template2)) {
                            throw new Exception('Invalid Template. Did you change the template name recently?');
                        }

                        //get director
                        $director = $this->view->director = $this->regModel->getDirector($research['registrationId']);

                        $research['director_info'] = $director['Salutation'].' '.$director['FullName'];

                        if($director['Email']) {
                            //email
                            $dataEmail2 = array(
                                'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $director['Email'],
                                'subject' => $template2['tpl_name'],
                                'content' => Thesis_Model_DbTable_Registration::parseContent($research, $template2['tpl_content']),
                                'date_que' => date('Y-m-d H:i:s')
                            );
                            $emailDb->add($dataEmail2);
                        }

                        if($director['Email2']) {
                            //email
                            $dataEmail3 = array(
                                'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $director['Email2'],
                                'subject' => $template2['tpl_name'],
                                'content' => Thesis_Model_DbTable_Registration::parseContent($research, $template2['tpl_content']),
                                'date_que' => date('Y-m-d H:i:s')
                            );
                            $emailDb->add($dataEmail3);
                        }

                        //send pd notification

                        $this->_helper->flashMessenger->addMessage(array('success' => "Proposal Defence successfully approved"));

                    } elseif ($this->_request->getPost('reject')) {

                        $pd_status = 'rejected';
                        $status = $this->thesisModel->getStatusByCode($pd_status, 1);

                        $data = array(
                            'pd_update_by' => $this->auth->getIdentity()->iduser,
                            'pd_updated_date' => new Zend_Db_Expr('NOW()'),
                            'pd_status' => $status,
                        );

                        //send registered email

                        $emailDb = new App_Model_Email();
                        $commDb = new App_Model_Template();

                        $research = $this->regModel->getProposal($app_id);

                        if($research['DefinitionCode'] == 'viva') {
                            $gettemplate = $commDb->getTemplatesByCategory('thesis', 'proposal-rejected-viva', 0, 1);
                        }else{
                            $gettemplate = $commDb->getTemplatesByCategory('thesis', 'proposal-rejected', 0, 1);
                        }
                        $template = $gettemplate[0];

                        $research['research_type'] = 'proposal';

                        if (!isset($research)) {
                            throw new Exception('Invalid Research');
                        }

                        if (empty($template)) {
                            throw new Exception('Invalid Template. Did you change the template name recently?');
                        }

                        //get supervisors
                        $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($research['p_id'], 'proposal');

                        $supervisor_name = array();

                        foreach ($supervisors as $key => $sup_name) {
                            $newkey = $key + 1;
                            $supervisor_name[] = $newkey . '. '.$sup_name['Salutation'] .' '. $sup_name['supervisor_name'];
                        }

                        $sname = "<p>" . implode('<br>', $supervisor_name) . "</p>";

                        $research['supervisor_info'] = $sname;

                        //email
                        $dataEmail = array(
                            'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $research['student_email'],
                            'subject' => $template['tpl_name'],
                            'content' => Thesis_Model_DbTable_Registration::parseContent($research, $template['tpl_content']),
                            'date_que' => date('Y-m-d H:i:s')
                        );

                        $emailDb->add($dataEmail);
                        //send registered email

                        $this->_helper->flashMessenger->addMessage(array('success' => "Proposal Defence successfully rejected"));
                    }

                    $this->regModel->updateProposal($data, 'p_id = ' . (int)$app_id);
                    $this->regModel->saveSubmission($app_id, 'proposal', $pd_status);
                }

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'approval', 'action' => 'pre'), 'default', true));
            }

            $i++;
        }


        //views
        $this->view->results = $results;
    }

    /*
        Colloquium Approval
    */
    public function colloquiumAction()
    {
        $form = new Thesis_Form_ReportProgress();

        $this->view->title = 'Colloquium Approval';

        $results = $this->thesisModel->getColloquiumSupervisee($this->userId);

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $i = 0;

            $count = count($formData['app']);

            if ($count > 0) {
                foreach ($formData['app'] as $app_id) {

                    if ($this->_request->getPost('approve')) {
                        $data = array(
                            'approved_date' => new Zend_Db_Expr('NOW()'),
                            'approved_by' => $this->auth->getIdentity()->iduser,
                            'approved_role' => $this->auth->getIdentity()->role,
                            'status' => 'APPROVED',
                            'remarks' => $formData['remarks'][$i]
                        );

                        $this->thesisModel->updateColloquiumApplication($data, array('id = ?' => $app_id));

                        $this->_helper->flashMessenger->addMessage(array('success' => "Colloquium(s) successfully approved"));
                    } elseif ($this->_request->getPost('reject')) {

                        $data = array(
                            'rejected_date' => new Zend_Db_Expr('NOW()'),
                            'rejected_by' => $this->auth->getIdentity()->iduser,
                            'rejected_role' => $this->auth->getIdentity()->role,
                            'status' => 'REJECTED',
                            'remarks' => $formData['remarks'][$i]
                        );

                        $this->thesisModel->updateColloquiumApplication($data, array('id = ?' => $app_id));

                        $this->_helper->flashMessenger->addMessage(array('success' => "Colloquium(s) successfully rejected"));
                    }


                }


                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'approval', 'action' => 'colloquium'), 'default', true));
            }

            $i++;
        }


        $this->view->results = $results;
    }
}
