<?php
/*
	Munzir Rosdi
	9/18/2014
*/
class Thesis_AjaxController extends Zend_Controller_Action 
{
	public function init() 
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$this->db = getDB();
	}

	public function studentAction()
	{
		$query = $this->_getParam('term');

		$db = $this->db;

		$select = $db->select()->from(array('sa' => 'tbl_studentregistration'), array('sa.IdStudentRegistration as value'))        		
							  ->join(array('p'=>'student_profile'),'p.id=sa.sp_id',array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as label'));

		if ( $query != '' )
		{
			$select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?','%'.$query.'%'); 
		}


		$results = $db->fetchAll($select);

		echo Zend_Json::encode($results);
	}

	public function deleteSupervisorAction()
	{
		$db = $this->db;

		$id = $this->_getParam('id');

		if ( $id != '' )
		{
			$db->delete('thesis_article_supervisor', 'ps_id = '.(int) $id);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
			exit;
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
			exit;
		}
	}

	public function deleteFileAction()
	{
		$db = $this->db;

		$id = $this->_getParam('id');

		if ( $id != '' )
		{
			$regDB = new Thesis_Model_DbTable_Registration();
			$file = $regDB->getFile($id);

			if ( empty($file) )
			{
				$data = array('msg' => 'Invalid ID');

				echo Zend_Json::encode($data);
			}

			$db->delete('thesis_article_files', 'af_id = '.(int) $id);

			//unlink
			@unlink(DOCUMENT_PATH.$file['af_fileurl']);

			$data = array('msg' => '');

			echo Zend_Json::encode($data);
		}
		else
		{
			$data = array('msg' => 'Invalid ID');

			echo Zend_Json::encode($data);
		}
	}
}