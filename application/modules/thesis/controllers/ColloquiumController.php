<?php

class Thesis_ColloquiumController extends  Zend_Controller_Action
{
	public function init() {
		
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		
		$this->defModel = new App_Model_General_DbTable_Definationms();
		$this->emailModel = new App_Model_Email();
		$this->thesisModel = new Thesis_Model_DbTable_General();
		$this->regModel = new Thesis_Model_DbTable_Registration();
		$this->docModel = new Thesis_Model_DbTable_Documents();
		$this->trackModel = new Thesis_Model_DbTable_Tracking();
		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
													  );

		
		$this->auth = Zend_Auth::getInstance();
		$this->uploadDir = DOCUMENT_PATH.'/thesis';

		$this->userId = $this->view->userId = $this->auth->getIdentity()->id;
		$this->userInfo = $this->view->userInfo = $this->auth->getIdentity()->info;

		Zend_Layout::getMvcInstance()->assign('navActive', 'colloquium');
	}
	
	public function indexAction()
	{
		$this->view->title = 'Colloquium';

		$data = $this->thesisModel->getColloquiumSetupData($this->userInfo['IdStudentRegistration']);

		$this->view->results = $data;
	}

	public function viewAction()
	{
		$this->view->title = 'Colloquium Details';

		$id = $this->_getParam('id');

		$info = $this->thesisModel->getColloquiumSetupSingle($id);

		if ( empty($info) )
		{
			throw new Exception('Invalid Colloquium ID');
		}

		$application = $this->thesisModel->getColloquiumApplication($id, $this->userInfo['IdStudentRegistration']);
		

		$this->view->application = $application;
		$this->view->info = $info;
		$this->view->id = $id;
	}

	public function registerAction()
	{
		$this->view->title = 'Colloquium Registration';

		$id = $this->_getParam('id');

		$info = $this->thesisModel->getColloquiumSetupSingle($id);

		if ( empty($info) )
		{
			throw new Exception('Invalid Colloquium ID');
		}

		$application = $this->thesisModel->getColloquiumApplication($id, $this->userInfo['IdStudentRegistration']);
		
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$data = array(
                            'colloquium_id'				=> $id,
                            'student_id'				=> $this->userInfo['IdStudentRegistration'],
							'status'					=> 'APPLIED',
							'applied_date'				=> new Zend_Db_Expr('NOW()'),
							'applied_by'				=> $this->userInfo['IdStudentRegistration']
                        );
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Colloquium registration successful"));

			$status_id = $this->thesisModel->addColloquiumApplication($data);
         
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'colloquium', 'action'=>'index'),'default',true));
		}

		$this->view->application = $application;
		$this->view->info = $info;
		$this->view->id = $id;
	}
}