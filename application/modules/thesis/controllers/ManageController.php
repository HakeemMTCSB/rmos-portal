<?php

class Thesis_ManageController extends  Zend_Controller_Action
{
	public function init() {
		
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		
		$this->defModel = new App_Model_General_DbTable_Definationms();
		$this->emailModel = new App_Model_Email();
		$this->thesisModel = new Thesis_Model_DbTable_General();
		$this->regModel = new Thesis_Model_DbTable_Registration();

		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
													  );

		
		$this->auth = Zend_Auth::getInstance();
		$this->uploadDir = DOCUMENT_PATH.'/thesis';

		$this->userId = $this->view->userId = $this->auth->getIdentity()->iduser;

		Zend_Layout::getMvcInstance()->assign('navActive', 'manage');
	
	}
	
	/*
		Registration - Proposal
	*/
	public function proposalAction()
	{
		//title
    	$this->view->title = "Supervisee List";

		$proposal = $this->regModel->getProposalSuperviseeList($this->userId);
    	
    	$this->view->proposal = $proposal;
	}

	public function proposalEditAction()
	{
		$this->view->title = "Edit Proposal";
		
		$form = new Thesis_Form_Proposal();
		
	
		//options
		$thesisModel = new Thesis_Model_DbTable_General();
        $catList = $thesisModel->getCategory(null, true);
		$form->p_category->addMultiOption('', '-- Select --');
		foreach ($catList as $cat)
		{
			$form->p_category->addMultiOption($cat['rc_id'],  $cat['rc_name']);
		}

		$topicList = $thesisModel->getTopics(true);
	

		$form->p_topic->addMultiOption('', '-- Select --');
		foreach ( $topicList as $topic )
		{
			$form->p_topic->addMultiOption($topic['r_id'], $topic['r_topicname']);
		}


		//add Others
		$form->p_category->setAttrib('disabled','disabled');
		$form->p_topic->setAttrib('disabled','disabled');
		
		$id = $this->_getParam('id');

		//get proposal info
		$proposal = $this->regModel->getProposal($id);
		
		if ( empty($proposal) )
		{
			throw new Exception('Invalid Proposal ID');
		}

		//get files
		$uploadedfiles = $this->view->uploadedfiles = $this->regModel->getFiles($proposal['p_id'], 'proposal');
	
		//get supervisors
		$supervisors = $this->view->supervisors = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');
		$supervisor_ids = array();
		foreach ( $supervisors as $sup )
		{
			$supervisor_ids[] = $sup['ps_id'];
		}

		$this->view->supervisor_ids = implode(',',$supervisor_ids);

	
	
		//repopulate form
		$form->populate($proposal);

		$this->view->proposal = $proposal;

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$form->p_category->setRequired(false);
			$form->p_topic->setRequired(false);
			
			if ($form->isValid ( $formData )) 
			{
				//add proposal
				$data = array(
								'p_description'				=> $formData['p_description'],
								'p_status'					=> $this->thesisModel->getStatusByCode('APPLIED'),
								'created_by'				=> 1,
								'created_date'				=> new Zend_Db_Expr('NOW()')
							);
				
				$proposal_id = $id;
				
				$this->regModel->updateProposal($data,'p_id='.(int)$id);
				
				//upload file
				try 
				{
					$uploadDir = $this->uploadDir.'/'.$formData['student_id'];
					
				
					if ( !is_dir( $uploadDir ) )
					{
						if ( mkdir_p($uploadDir) === false )
						{
							throw new Exception('Cannot create student document folder ('.$uploadDir.')');
						}
					}

					$adapter = new Zend_File_Transfer_Adapter_Http();
					
				
					$files = $adapter->getFileInfo();
					$adapter->addValidator('NotExists', false, $uploadDir );
					$adapter->setDestination( $this->uploadDir );
					$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
					$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
			
					foreach ($files as $no => $fileinfo) 
					{
						$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
					
						if ($adapter->isUploaded($no))
						{
							
							$ext = getext($fileinfo['name']);

							$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
							$fileUrl = $uploadDir.'/'.$fileName;

							$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
							$adapter->setOptions(array('useByteString' => false));
							
							$size = $adapter->getFileSize($no);
						
							if( !$adapter->receive($no))
							{
								$errmessage = array();
								if ( is_array($adapter->getMessages() ))
								{	
									foreach(  $adapter->getMessages() as $errtype => $errmsg )
									{
										$errmessage[] = $errmsg;
									}
									
									throw new Exception(implode('<br />',$errmessage));
								}
							}
							
							//save file into db
							$data = array(
											'af_pid'			=> $proposal_id,
											'af_type'			=> 'proposal',
											'af_filename'		=> $fileinfo['name'],
											'af_fileurl'		=> '/thesis/'.$formData['student_id'].'/'.$fileName,
											'af_filesize'		=> $size
										);
						
							$this->regModel->addFile($data);
							
						} //isuploaded

						
					} //foreach


				}
				catch (Exception $e) 
				{
					throw new Exception( $e->getMessage() );
				}
				
				$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
			 
				$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'registration', 'action'=>'proposal-edit', 'id' => $id),'default',true));

			} // valid
			
			
		}

		//views
		$this->view->form = $form;
	}

    public function proposalViewAction()
    {

        $id = $this->_getParam('id');

        //get proposal info
        $proposal = $this->regModel->getProposal($id);

        if ( empty($proposal) )
        {
            throw new Exception('Invalid Proposal ID');
        }

        $this->view->title = "Activity Details ($proposal[ActivityName])";


        //get files
        $this->view->uploadedfiles = $this->regModel->getFiles($proposal['p_id'], 'proposal');

        //evaluation files supervisors
        $rr = $this->view->evaluationfiles = $this->regModel->getEvaluationFiles($proposal['p_id'],'approval');

        //get supervisors
        $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');

        //get assessor
        $assessor = $this->view->assessors = $this->regModel->getExaminers($proposal['p_id'], 'proposal');

        $gradingList = $this->view->grade = $this->defModel->getDataByTypeCode('Research Grading');
//        $supervisor_ids = array();
//        foreach ( $supervisors as $sup )
//        {
//            $supervisor_ids[] = $sup['ps_id'];
//        }
//
//        //views
//        $this->view->supervisor_ids = implode(',',$supervisor_ids);
        $this->view->proposal = $proposal;

    }

    public function proposalActivityAction()
    {
        $this->view->title = "Registration Details";

        $id = $this->_getParam('id');

        //get proposal info
        $proposal = $this->regModel->getProposal($id);

        if ( empty($proposal) )
        {
            throw new Exception('Invalid Proposal ID');
        }

        //get files
        $this->view->uploadedfiles = $this->regModel->getFiles($proposal['p_id'], 'proposal');

        //evaluation files supervisors
        $this->view->evaluationfiles = $this->regModel->getEvaluationFiles($proposal['p_id'], 2);

        //get supervisors
        $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');

        //get assessor
        $assessor = $this->view->assessors = $this->regModel->getExaminers($proposal['p_id'], 'proposal');

        $gradingList = $this->view->grade = $this->defModel->getDataByTypeCode('Research Grading');

        $this->view->proposal = $proposal;

        //get activities
        $activities = $this->view->activities = $this->regModel->getActivities($id, $proposal['course_id']);

    }

	/*
		ARTICLESHIP
	*/
	public function articleshipAction()
	{
		//title
    	$this->view->title = "Articleship";

		$articleship = $this->regModel->getArticleshipSupervisee($this->userId);

    	$this->view->articleship = $articleship;
	}

	public function articleshipEditAction()
	{
		$this->view->title = "Edit Articleship";
		
		$form = new Thesis_Form_Articleship();
	
		//
		$interest = $this->thesisModel->getInterest();
		$this->view->interests = $interest;
		
		$id = $this->_getParam('id');

		//get articleship info
		$articleship = $this->regModel->getArticleship($id);
		
		if ( empty($articleship) )
		{
			throw new Exception('Invalid Articleship ID');
		}

		//get files
		$uploadedfiles = $this->view->uploadedfiles = $this->regModel->getFiles($articleship['a_id'], 'articleship');



		//interest
		$_interest = array();
		$this->view->interest = json_decode($articleship['fieldofinterest'], true);
		
		
		//repopulate form
		$form->populate($articleship);

		$this->view->articleship = $articleship;
		
		//disable semester_id
		$form->semester_id->setAttrib('disabled','disabled');

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$form->semester_id->setRequired(false);

			if ($form->isValid ( $formData )) 
			{
				$data = array(
								//'semester_id'				=> $formData['semester_id'],
								'fieldofinterest'			=> json_encode($formData['interest']),
								'fieldofinterest_others'	=> $formData['fieldofinterest_others'],
								'company'					=> $formData['company'],
								'designation'				=> $formData['designation'],
								'contactperson'				=> $formData['contactperson'],
								'address'					=> $formData['address'],
								'city'						=> $formData['city'],
								'city_others'				=> $formData['city_others'],
								'state'						=> $formData['state'],
								'state_others'				=> $formData['state_others'],
								'country'					=> $formData['country'],
								'postcode'					=> $formData['postcode'],
								'phoneno'					=> $formData['phoneno'],
								'faxno'						=> $formData['faxno'],
								'email'						=> $formData['email'],
								'isemployee'				=> $formData['isemployee'],
								'yearsofservice'			=> $formData['yearsofservice'],
								//'status'					=> $this->thesisModel->getStatusByCode('APPLIED'),
								'updated_by'				=> 1,
								'updated_date'				=> new Zend_Db_Expr('NOW()')
							);
				
				$articleship_id = $id;

				
				$this->regModel->updateArticleship($data, 'a_id = '.(int) $id );

				//upload file
				try 
				{
					$uploadDir = $this->uploadDir.'/'.$formData['student_id'];

					if ( !is_dir( $uploadDir ) )
					{
						if ( mkdir_p($uploadDir) === false )
						{
							throw new Exception('Cannot create student document folder ('.$uploadDir.')');
						}
					}

					$adapter = new Zend_File_Transfer_Adapter_Http();
					
				
					$files = $adapter->getFileInfo();
					$adapter->addValidator('NotExists', false, $uploadDir );
					$adapter->setDestination( $this->uploadDir );
					$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
					$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));

					foreach ($files as $no => $fileinfo) 
					{
						$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
					
						if ($adapter->isUploaded($no))
						{
							$ext = getext($fileinfo['name']);

							$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
							$fileUrl = $uploadDir.'/'.$fileName;

							$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
							$adapter->setOptions(array('useByteString' => false));
							
							$size = $adapter->getFileSize($no);
						
							if( !$adapter->receive($no))
							{
								$errmessage = array();
								if ( is_array($adapter->getMessages() ))
								{	
									foreach(  $adapter->getMessages() as $errtype => $errmsg )
									{
										$errmessage[] = $errmsg;
									}
									
									throw new Exception(implode('<br />',$errmessage));
								}
							}
							
							//save file into db
							$data = array(
											'af_pid'			=> $articleship_id,
											'af_type'			=> 'articleship',
											'af_filename'		=> $fileinfo['name'],
											'af_fileurl'		=> '/thesis/'.$formData['student_id'].'/'.$fileName,
											'af_filesize'		=> $size
										);
								
							$this->regModel->addFile($data);
							
						} //isuploaded
						
					} //foreach
				}
				catch (Exception $e) 
				{
					throw new Exception( $e->getMessage() );
				}

				
				
				$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
			 
				$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'registration', 'action'=>'articleship-edit','id'=>$id),'default',true));

			} // valid
			
			
		}

		//views
		$this->view->form = $form;
	}

	/*
		EXEMPTION
	*/
	public function exemptionAction()
	{
		//title
    	$this->view->title = "Professional Practice Paper";

		$exemption = $this->regModel->getExemptionSupervisee($this->userId);
    	
    	$this->view->exemption = $exemption;
	}

	public function exemptionEditAction()
	{
		$this->view->title = "Edit Professional Practice Paper";
		
		$form = new Thesis_Form_Exemption();
		$id = $this->_getParam('id');

		//get exemption info
		$exemption = $this->regModel->getExemption($id);
		
		if ( empty($exemption) )
		{
			throw new Exception('Invalid PPP ID');
		}
		
		
		//$form->semester_id->setRequired(false);
		//$form->subject_id->setRequired(false);

		//get files
		$uploadedfiles = $this->view->uploadedfiles = $this->regModel->getFiles($exemption['e_id'], 'exemption');

		//get supervisors
		$supervisors = $this->view->supervisors = $this->regModel->getSupervisors($exemption['e_id'], 'exemption');
		$supervisor_ids = array();
		foreach ( $supervisors as $sup )
		{
			$supervisor_ids[] = $sup['ps_id'];
		}

		$this->view->supervisor_ids = implode(',',$supervisor_ids);

		//employment list
		$employment = $this->regModel->getEmployment($id);
		$this->view->employment = $employment;

		//reason list
		$reasons = $this->thesisModel->getReason();
		$this->view->reasons = $reasons;

		//$student = $this->studentInfo;
		//$this->view->studentInfo = $student;
	
		//repopulate form
		$form->populate($exemption);

		$this->view->exemption = $exemption;

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid ( $formData )) 
			{
				$data = array(
								'reason'					=> $formData['reason'],
								'reason_selected'			=> json_encode($formData['reason_selected']),
								'proposedarea'				=> $formData['proposedarea'],
								'status'					=> $this->thesisModel->getStatusByCode('APPLIED'),
								'created_by'				=> $this->auth->getIdentity()->iduser,
								'created_date'				=> new Zend_Db_Expr('NOW()'),
								'start_date'				=> $formData['start_date'],
								'end_date'					=> $formData['end_date']
							);
				
				$this->regModel->updateExemption($data,'e_id='.(int) $id );
				$exemption_id = $id;


				//upload file
				try 
				{
					$uploadDir = $this->uploadDir.'/'.$formData['student_id'];

					if ( !is_dir( $uploadDir ) )
					{
						if ( mkdir_p($uploadDir) === false )
						{
							throw new Exception('Cannot create student document folder ('.$uploadDir.')');
						}
					}

					$adapter = new Zend_File_Transfer_Adapter_Http();
					
				
					$files = $adapter->getFileInfo();
					$adapter->addValidator('NotExists', false, $uploadDir );
					$adapter->setDestination( $this->uploadDir );
					$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
					$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));

					foreach ($files as $no => $fileinfo) 
					{
						$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
					
						if ($adapter->isUploaded($no))
						{
							$ext = getext($fileinfo['name']);

							$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
							$fileUrl = $uploadDir.'/'.$fileName;

							$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
							$adapter->setOptions(array('useByteString' => false));
							
							$size = $adapter->getFileSize($no);
						
							if( !$adapter->receive($no))
							{
								$errmessage = array();
								if ( is_array($adapter->getMessages() ))
								{	
									foreach(  $adapter->getMessages() as $errtype => $errmsg )
									{
										$errmessage[] = $errmsg;
									}
									
									throw new Exception(implode('<br />',$errmessage));
								}
							}
							
							//save file into db
							$data = array(
											'af_pid'			=> $exemption_id,
											'af_type'			=> 'exemption',
											'af_filename'		=> $fileinfo['name'],
											'af_fileurl'		=> '/thesis/'.$formData['student_id'].'/'.$fileName,
											'af_filesize'		=> $size
										);
								
							$this->regModel->addFile($data);
							
						} //isuploaded
						
					} //foreach
				}
				catch (Exception $e) 
				{
					throw new Exception( $e->getMessage() );
				}

				//add employment detail
				for($i=0;$i<count($formData['employment_compname']);)
				{
					//save file into db
					$data = array(
									'ae_pid'			=> $exemption_id,
									'ae_compname'		=> $formData['employment_compname'][$i],
									'ae_compaddress'	=> $formData['employment_compaddress'][$i],
									'ae_position'		=> $formData['employment_position'][$i],
									'ae_jobfunction'	=> $formData['employment_jobfunction'][$i],
									'ae_year_from'		=> $formData['employment_year_from'][$i],
									'ae_year_to'		=> $formData['employment_year_to'][$i],
									'ae_refname'		=> $formData['employment_refname'][$i],
									'ae_refaddress'		=> $formData['employment_refaddress'][$i],
									'ae_addedby'		=> $this->auth->getIdentity()->iduser,
									'ae_addeddate'		=> new Zend_Db_Expr('NOW()')
								);
						
					$this->regModel->addEmployment($data);

					$i++;
				}

				$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
			 
				$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'registration', 'action'=>'exemption-edit', 'id' => $id),'default',true));

			} // valid
			
			
		}

		//views
		$this->view->form = $form;
	}
	
	public function exemptionViewempAction()
	{
		if( $this->getRequest()->isXmlHttpRequest() )
		{
			$this->_helper->layout->disableLayout();
		}

		$id = $this->_getParam('id');

		$employment = $this->regModel->getEmploymentSingle($id);

		$this->view->emp = $employment;
	}

    public function notifyStudentAction()
    {
        $this->view->title = "Notification";

        $id = $this->_getParam('id');
        $supervisor_id = $this->auth->getIdentity()->iduser;

        //get proposal info
        $proposal = $this->regModel->getProposal($id);

        //get messages
        $messages = $this->regModel->getMessage($id,$proposal['student_id'],$supervisor_id);

        $this->view->proposal = $proposal;
        $this->view->messages = $messages;

        $supervisor = $this->view->supervisors = $this->regModel->getSupervisor($proposal['p_id'], 'proposal',$supervisor_id);
//        dd($proposal);

        if ( $this->getRequest()->isPost() ) {
            $formData = $this->getRequest()->getPost();

        //send email
        $emailDb = new App_Model_Email();
        $commDb = new App_Model_Template();

        $gettemplate = $commDb->getTemplatesByCategory('thesis', 'research-notification', 0, 1);
        $template = $gettemplate[0];
        $larrResult['supervisor_info'] = strtoupper($supervisor['supervisor_name']);
        $larrResult['student_name'] = $proposal['student_name'];
        $larrResult['message'] = $formData['message'];

        $dataEmail = array(
            'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $proposal['student_email'],
            'subject' => $template['tpl_name'],
            'content' => Thesis_Model_DbTable_Registration::parseContent($larrResult, $template['tpl_content']),
            'date_que' => date('Y-m-d H:i:s')
        );

        $emailDb->add($dataEmail);

        //email new

            $data = array(
                'research_id'		=> $id,
                'research_type'	=> 'proposal',
                'student_id'		=> $proposal['student_id'],
                'supervisor_id'		=> $supervisor_id,
                'semester_id'	=> $proposal['semester_start'],
                'message'		=> $formData['message'],
                'created_by'		=> $this->auth->getIdentity()->iduser,
                'created_date'		=> new Zend_Db_Expr('NOW()')
            );

            $this->regModel->addNotification($data);

            $this->_helper->flashMessenger->addMessage(array('success' => "Notification sent"));

            $this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'manage', 'action'=>'notify-student', 'id' => $id),'default',true));
        }

    }
}
