<?php

class Thesis_SubmissionController extends Zend_Controller_Action
{
    public function init()
    {

        $session = new Zend_Session_Namespace('sis');
        $this->session = $session;

        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->emailModel = new App_Model_Email();
        $this->thesisModel = new Thesis_Model_DbTable_General();
        $this->regModel = new Thesis_Model_DbTable_Registration();
        $this->docModel = new Thesis_Model_DbTable_Documents();
        $this->trackModel = new Thesis_Model_DbTable_Tracking();
        //locale setup
        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->locales = $this->view->locales = array(
            'en_US' => 'English',
            'ms_MY' => 'Malay'
        );


        $this->auth = Zend_Auth::getInstance();
        $this->uploadDir = DOCUMENT_PATH . '/thesis/finalreport';

        $this->userId = $this->view->userId = $this->auth->getIdentity()->id;
        $this->userInfo = $this->view->userInfo = $this->auth->getIdentity()->info;

        Zend_Layout::getMvcInstance()->assign('navActive', 'submission');
    }

    public function indexAction()
    {
    }

    public function finalAction()
    {
        $this->view->title = 'Final Report Submission';

        $errMsg = '';

        $research = $this->thesisModel->getResearchType($this->userInfo['IdStudentRegistration'], 1);

        if (empty($research)) {
            throw new Exception('No Research Found');
        }

        //check submission
        $submit = $this->thesisModel->getSubmission($this->userInfo['IdStudentRegistration'], 'final');

        if (!empty($submit)) {
            $errMsg = 'You already submitted your final report.';
        }

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            //upload file
            try {
                $uploadDir = $this->uploadDir;

                if (!is_dir($uploadDir)) {
                    if (mkdir_p($uploadDir) === false) {
                        throw new Exception('Cannot create final report folder (' . $uploadDir . ')');
                    }
                }

                $adapter = new Zend_File_Transfer_Adapter_Http();


                $files = $adapter->getFileInfo();
                $adapter->addValidator('NotExists', false, $uploadDir);
                $adapter->setDestination($uploadDir);
                $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                foreach ($files as $no => $fileinfo) {
                    $adapter->addValidator('Extension', false, array('extension' => 'xls,xlsx,jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                    if ($adapter->isUploaded($no)) {
                        $ext = getext($fileinfo['name']);

                        $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                        $fileUrl = $uploadDir . '/' . $fileName;

                        $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                        $adapter->setOptions(array('useByteString' => false));

                        $size = $adapter->getFileSize($no);

                        if (!$adapter->receive($no)) {
                            $errmessage = array();
                            if (is_array($adapter->getMessages())) {
                                foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                    $errmessage[] = $errmsg;
                                }

                                throw new Exception(implode('<br />', $errmessage));
                            }
                        }

                    } //isuploaded

                } //foreach
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }


            //student_id, research_id, research_type, submit_type, agree, submit_date, approved
            $data = array(
                'student_id'    => $this->userInfo['IdStudentRegistration'],
                'research_id'   => $research['info'][$this->thesisModel->researchId($research['type'])],
                'research_type' => $research['type'],
                'submit_type'   => 'final',
                'submit_date'   => '0000-00-00',
                'agree'         => 1,
                'approved'      => 1,
                'created_by'    => $this->userInfo['IdStudentRegistration'],
                'created_date'  => new Zend_Db_Expr('NOW()'),
                'file_name'     => $fileinfo['name'],
                'file_url'      => '/thesis/finalreport/' . $fileName,
            );

            if (isset($formData['perm_public']) && $formData['perm_public'] == 1) {
                $data['perm_public'] = 1;
            }

            if (isset($formData['perm_student']) && $formData['perm_student'] == 1) {
                $data['perm_student'] = 1;
            }

            $id = $this->thesisModel->addSubmission($data);

            $this->_helper->flashMessenger->addMessage(array('success' => "Final report successfully submitted."));

            $this->_redirect($this->view->baseUrl());

        }

        //view
        $this->view->research = $research;
        $this->view->submit = $submit;
        $this->view->errMsg = $errMsg;
    }

    public function preAction()
    {
        $this->view->title = 'Pre-Submission';
        $errMsg = '';

        $research = $this->thesisModel->getResearchType($this->userInfo['IdStudentRegistration'], 1);

        if (empty($research)) {
            throw new Exception('No Research Found');
        }

        //check submission
        $submit = $this->thesisModel->getSubmission($this->userInfo['IdStudentRegistration'], 'pre');

        if (!empty($submit)) {
            $errMsg = 'You already submitted your pre-submission date.';
        }

        if (!empty($research)) {
            $this->view->supervisors = $this->regModel->getSupervisors($research['info']['p_id'], 'proposal');
        }


        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            //student_id, research_id, research_type, submit_type, agree, submit_date, approved
            $data = array(
                'student_id'    => $this->userInfo['IdStudentRegistration'],
                'research_id'   => $research['info'][$this->thesisModel->researchId($research['type'])],
                'research_type' => $research['type'],
                'submit_type'   => 'pre',
                'submit_date'   => $formData['submit_date'],
                'agree'         => 1,
                'approved'      => 0,
                'created_by'    => $this->userInfo['IdStudentRegistration'],
                'created_date'  => new Zend_Db_Expr('NOW()')
            );


            $id = $this->thesisModel->addSubmission($data);

            $this->_helper->flashMessenger->addMessage(array('success' => "Pre-submission date successfully submitted."));

            $this->_redirect($this->view->baseUrl());

        }

        //view
        $this->view->research = $research;
        $this->view->submit = $submit;
        $this->view->errMsg = $errMsg;
    }

    public function proposalDefenceAction()
    {
        $this->view->title = 'Application';
        $errMsg = '';

        $proposalId = $this->_getParam('id');

        $submit = $this->regModel->getProposal($proposalId);
        $this->view->supervisors = $this->regModel->getSupervisors($proposalId, 'proposal');

        $error = 0;
//        if (isset($submit['pd_status'])) {
//            $errMsg = 'You already submitted your proposal defence.';
//            $error = 1;
//        }

        //get files
        $uploadedfiles = $this->view->uploadedfiles = $this->thesisModel->getFiles($submit['p_id'], 'proposal');

        $form = new Thesis_Form_AddActivity(array('p_id' => $submit['p_id'],'course_id' => $submit['course_id'],'student_id' => $submit['student_id']));

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $defModel = new App_Model_General_DbTable_Definationms();
            $getResearchType = $defModel->getIdByDefType('Research Type', 'proposal'); // research type

            //student_id, research_id, research_type, submit_type, agree, submit_date, approved
            $data = array(
                'student_id'    => $this->userInfo['IdStudentRegistration'],
                'research_id'   => $proposalId,
                'research_type' => $getResearchType['key'],
                'submit_type'   => 'pre',
                'submit_date'   => $formData['submit_date'] ?? null,
                'status'        => $this->thesisModel->getStatusByCode('applied', 1),
                'agree'         => 1,
                'approved'      => 0,
                'created_role'  => 'student',
                'created_by'    => $this->userInfo['IdStudentRegistration'],
                'created_date'  => new Zend_Db_Expr('NOW()')
            );

            $id = $this->thesisModel->addSubmission($data);

            $dataUpd = array(
                'pd_status' => $this->thesisModel->getStatusByCode('applied',1),
                'pd_role'   => 'student',
                'pd_by'     => $this->userInfo['IdStudentRegistration'],
                'pd_date'   => new Zend_Db_Expr('NOW()')
            );

            $this->thesisModel->updateProposal($dataUpd, 'p_id = ' . (int)$proposalId);

            $pidmain = $this->thesisModel->getPidMain($this->userInfo['IdStudentRegistration'],$submit['course_id']);

            //add activity
            $data = array(
//                'pid' => $pidmain['p_id'],
                'pid' => $proposalId,
                'student_id'    => $this->userInfo['IdStudentRegistration'],
                'p_title' => $submit['p_title'],
                'p_category' => $submit['p_category'] ?? null,
                'p_topic' => $submit['p_topic'] ?? null,
                'p_description' => $submit['p_description'] ?? null,
                'meeting_date' => $submit['meeting_date'] ?? null,
                'p_status' => $submit['p_status'],
                'activity_id' => $formData['activity_id'],
                'created_by' => $this->userInfo['IdStudentRegistration'],
                'created_date' => new Zend_Db_Expr('NOW()'),
                'created_role' => $submit['created_role'],
                'completion_date' => $submit['completion_date'] ?? null,
                'semester_start' => $submit['semester_start'] ?? null,
                'semester_end' => $submit['semester_end'] ?? null,
                'course_id' => $submit['course_id'],
                'applied_date' => $submit['applied_date'],
                'pd_status' => $this->thesisModel->getStatusByCode('applied',1),
                'pd_role'   => 'student',
                'pd_by'     => $this->userInfo['IdStudentRegistration'],
                'pd_date'   => new Zend_Db_Expr('NOW()')
            );
//            print_r($proposalId);exit;
            $proposal_id = $this->thesisModel->addProposal($data);
            $this->view->auth = Zend_Auth::getInstance();
            //add supervisor
            $supervisors = $this->regModel->getSupervisors($proposalId, 'proposal');

            if(count($supervisors) > 0) {

                foreach ($supervisors as $sps) {

                    $data = array(
                        'ps_pid'             => $proposal_id,
                        'ps_type'            => 'proposal',
                        'ps_supervisor_id'   => $sps['ps_supervisor_id'],
                        'ps_supervisor_type' => $sps['ps_supervisor_type'],
                        'ps_supervisor_role' => $sps['ps_supervisor_role'],
                        'ps_active'          => $sps['ps_active'],
                        'ps_addedby'         => $this->userInfo['IdStudentRegistration'],
                        'ps_addeddate'       => new Zend_Db_Expr('NOW()')
                    );

                    $supervisor_id = $this->regModel->addSupervisor($data);

                    $this->regModel->addSupervisorHistory([
                        'pid'          => $proposal_id,
                        'ps_id'          => $supervisor_id,
                        'action'       => 'ADD',
                        'reason'      => $sps['reason'],
                        'type'         => 'proposal',
                        'user_id'      => $sps['ps_supervisor_id'],
                        'created_date' => new Zend_Db_Expr('NOW()'),
                        'created_by'   => $this->userInfo['IdStudentRegistration'],
                    ]);
                }
            }
            $this->uploadDir2 = DOCUMENT_PATH . '/thesis';
            //upload file
            try {
                $uploadDir = $this->uploadDir2 . '/' . $this->userInfo['IdStudentRegistration'];

                if (!is_dir($uploadDir)) {
                    if (mkdir_p($uploadDir) === false) {
                        throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                    }
                }

                $adapter = new Zend_File_Transfer_Adapter_Http();

                $files = $adapter->getFileInfo();
                $adapter->addValidator('NotExists', false, $uploadDir);
                $adapter->setDestination($this->uploadDir2);
                //$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
                $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                foreach ($files as $no => $fileinfo) {
                    $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                    if ($adapter->isUploaded($no)) {
                        $ext = getext($fileinfo['name']);

                        $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                        $fileUrl = $uploadDir . '/' . $fileName;

                        $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                        $adapter->setOptions(array('useByteString' => false));

                        $size = $adapter->getFileSize($no);

                        if (!$adapter->receive($no)) {
                            $errmessage = array();
                            if (is_array($adapter->getMessages())) {
                                foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                    $errmessage[] = $errmsg;
                                }

                                throw new Exception(implode('<br />', $errmessage));
                            }
                        }

                        //save file into db
                        $data = array(
                            'af_pid' => $submit['p_id'],
                            'af_type' => 'proposal',
                            'af_filename' => $fileinfo['name'],
                            'af_fileurl' => '/thesis/' . $this->userInfo['IdStudentRegistration'] . '/' . $fileName,
                            'af_filesize' => $size
                        );

                        $this->thesisModel->addFile($data);

                    } //isuploaded

                } //foreach
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }

            //send email

            $getactivity = $this->regModel->getActivity($formData['activity_id']);
//print_r($getactivity);exit;
            //get supervisors
            $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($submit['p_id'], 'proposal');

            foreach ($supervisors as $sup) {

                $emailDb = new App_Model_Email();
                $commDb = new App_Model_Template();

                if($getactivity['ActivityName'] == 'VIVA VOCE') {
                    $gettemplate = $commDb->getTemplatesByCategory('thesis', 'proposal-application-viva', 0, 1);
                }else {
                    $gettemplate = $commDb->getTemplatesByCategory('thesis', 'proposal-application', 0, 1);
                }
                $template = $gettemplate[0];

                $submit['ActivityName'] = $getactivity['ActivityName'];

                $submit['research_type'] = 'proposal';

                if (!isset($submit)) {
                    throw new Exception('Invalid Research');
                }

                if (empty($template)) {
                    throw new Exception('Invalid Template. Did you change the template name recently?');
                }

                $submit['supervisor_info'] = $sup['supervisor_name'];

                //email
                $dataEmail = array(
                    'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $sup['supervisor_email'],
                    'subject' => $template['tpl_name'],
                    'content' => Thesis_Model_DbTable_Registration::parseContent($submit, $template['tpl_content']),
                    'date_que' => date('Y-m-d H:i:s')
                );

                $emailDb->add($dataEmail);

                if($sup['supervisor_email2'] != NULL){
                    $dataEmail2 = array(
                        'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $sup['supervisor_email2'],
                        'subject' => $template['tpl_name'],
                        'content' => Thesis_Model_DbTable_Registration::parseContent($submit, $template['tpl_content']),
                        'date_que' => date('Y-m-d H:i:s')
                    );

                    $emailDb->add($dataEmail2);
                }
            }
            //send email

            $this->regModel->saveSubmission($proposalId, 'proposal', 'applied');

            $this->_helper->flashMessenger->addMessage(array('success' => "Proposal Defence application successfully submitted."));

            $this->_redirect($this->view->baseUrl().'/thesis/submission/proposal-list');

        }

        //view
        $this->view->submit = $submit;
        $this->view->errMsg = $errMsg;
        $this->view->error = $error;
        $this->view->form = $form;
    }

    public function proposalListAction()
    {
        $this->view->title = 'Application List';
        $errMsg = '';

        $research = $this->thesisModel->getResearchType($this->userInfo['IdStudentRegistration'], 1);

        if (empty($research)) {
            throw new Exception('No Research Found');
        }

        //check submission
        $proposal = $this->regModel->getProposalListByStudent($this->userInfo['IdStudentRegistration']);
//print_r($proposal);exit;
        //view
        $this->view->research = $research;
        $this->view->proposal = $proposal;
        $this->view->errMsg = $errMsg;
    }

    public function proposalResultAction()
    {
        $proposalId = $this->_getParam('id');

        $proposal = $this->regModel->getProposal($proposalId);

        $this->view->title = $proposal['DefinitionDesc'].' Result';

        $this->view->grade = $this->defModel->getDataByTypeCode('Research Grading');

        //evaluation files student
        $this->view->evaluationfiles = $this->regModel->getEvaluationFiles($proposal['p_id'], 1);

        //view
        $this->view->proposal = $proposal;
    }

    public function proposalEventAction()
    {
        $this->view->title = 'Proposal Event';

        $eventId = $this->_getParam('id');

        $event = $this->regModel->getEvent($eventId);
//print_r($event);exit;
        //view
        $this->view->event = $event;
    }

    public function getDefAction()
    {
        $this->_helper->layout->disableLayout();

        $defDB = new Thesis_Model_DbTable_General();

        $id = $this->_getParam('id');

        $result = $defDB->getDef($id);

        $json = Zend_Json::encode($result);

        echo $json;
        exit();
    }
}