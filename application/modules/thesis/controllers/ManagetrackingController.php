<?php

class Thesis_ManagetrackingController extends  Zend_Controller_Action
{
	public function init() {
		
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		
		$this->defModel = new App_Model_General_DbTable_Definationms();
		$this->emailModel = new App_Model_Email();
		$this->thesisModel = new Thesis_Model_DbTable_General();
		$this->regModel = new Thesis_Model_DbTable_Registration();
		$this->trackModel = new Thesis_Model_DbTable_Tracking();

		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
													  );

		
		$this->auth = Zend_Auth::getInstance();
		$this->uploadDir = DOCUMENT_PATH.'/thesis';

		$this->userId = $this->view->userId = $this->auth->getIdentity()->iduser;
		$this->userInfo = $this->view->userInfo = $this->auth->getIdentity()->info;

		Zend_Layout::getMvcInstance()->assign('navActive', 'managetracking');
		
	}

	public function indexAction()
	{
		$this->view->title = 'Progress Tracking';

		//activities
		$proposal = $this->regModel->getProposalSupervisee($this->userId);
		$articleship = $this->regModel->getArticleshipSupervisee($this->userId);
		$exemption = $this->regModel->getExemptionSupervisee($this->userId);

		$maindata = array();

		foreach ( $proposal as $prop )
		{
			$maindata[] = array(	
									'student_id'	=>	$prop['student_id'],
									'type'			=>	'Proposal',
									'status_code'	=>	$prop['status_code'],
									'registrationId'=>	$prop['registrationId'],
									'student_name'	=>	$prop['student_name'],
									'created_date'	=>	$prop['created_date'],
									'updated_date'	=>	$prop['updated_date'],
									'research_id'	=>	$prop['research_id']
								);
		}

		foreach ( $articleship as $art )
		{
			$maindata[] = array(	
									'student_id'	=>	$art['student_id'],
									'type'			=>	'Articleship',
									'status_code'	=>	$art['status_code'],
									'registrationId'=>	$art['registrationId'],
									'student_name'	=>	$art['student_name'],
									'created_date'	=>	$art['created_date'],
									'updated_date'	=>	$art['updated_date'],
									'research_id'	=>	$art['research_id']
								);
		}

		foreach ( $exemption as $exm )
		{
			$maindata[] = array(	
									'student_id'	=>	$exm['student_id'],
									'type'			=>	'PPP',
									'status_code'	=>	$exm['status_code'],
									'registrationId'=>	$exm['registrationId'],
									'student_name'	=>	$exm['student_name'],
									'created_date'	=>	$exm['created_date'],
									'updated_date'	=>	$exm['updated_date'],
									'research_id'	=>	$exm['research_id']
								);
		}

		$this->view->tracking = $maindata;
	}
	
	public function detailAction()
	{
		$this->view->title = 'Tracking Detail';

		$id = $this->_getParam('id');
		$student_id = $this->_getParam('student_id');

		$research = $this->thesisModel->getResearchType($student_id, 1);
		$this->view->research = $research;

		//activities
		$_getactivities = $this->trackModel->getActivitiesByResearch($id);
		
		//rebuild array
		$getactivities = array();
		foreach ( $_getactivities as $row)
		{
			if ( $row['start_date'] != '' )
			{
				if ( time() > strtotime($row['end_date']) )
				{
					$getdate = $this->dateDifference(strtotime($row['end_date']),time());
					$row['time_ago'] = '<small class="red">'.$getdate['days_total'].' days past</small>';
					
				}
				else
				{
					$getdate = $this->dateDifference(time(),strtotime($row['end_date']));
					$row['time_ago'] = '<small class="normal">'.($getdate['days_total']+1).' days left</small>';
				}
			}
			else
			{
				$row['time_ago'] = '';
			}

			$getactivities[] = $row;
		}

		//levels by sub
		$this->activities = $this->acttree = array();
		
		$this->_levellist = $getactivities;

		foreach ( $this->_levellist as $activity )
		{
			$this->activities[$activity['pid']][] = $activity;
		}
		
		
		$this->view->jsonGantt = $this->gantify($this->activities);
		$this->view->colors = implode(' ', $this->colors );

		$this->getChildren(0);
	
		$this->view->activities = $this->acttree;
	}
	
	//build json for gantt
	protected function gantify($data=array())
	{
		$this->colors = array();

		$out = array();
		/*
		{
			name: "Sprint 0",
			desc: "Analysis",
			values: [{
				from: "/Date(1320192000000)/",
				to: "/Date(1322401600000)/",
				label: "Requirement Gathering",
				desc: "",
				customClass: "ganttRed"
			}]
		}*/
		
		if ( !empty($data[0]) && isset($data[0]) )
		{
			foreach(  $data[0] as $row )
			{
				if ( $row['start_date'] != '' )
				{
					$class = $row['color'] == '' ? 'ganttOrange' : 'color-'.$row['id'];
					if ( $row['color'] != '' )
					{
						$this->colors[] = '.fn-gantt .color-'.$row['id'].' {  background-color: '.$row['color'].' } ';
					}
		
					$out[] = array(
									'name'		=> $row['title'],
									'desc'		=> '',
									'values'	=>array( array(
															'from'			=>	'/Date('.datetomili($row['start_date']).')/',
															'to'			=>	'/Date('.datetomili($row['end_date']).')/',
															'label'			=>	$row['title'],
															'desc'			=>	$row['description'],
															'customClass'	=>	$class
														))
									);
		
					if ( isset($data[$row['id']]) && !empty($data[$row['id']]) )
					{
						foreach( $data[$row['id']] as $child )
						{
							$class = $child['color'] == '' ? 'ganttOrange' : 'color-'.$child['id'];
							if ( $child['color'] != '' )
							{
								$this->colors[] = '.fn-gantt .color-'.$child['id'].' {  background-color: '.$child['color'].' } ';
							}
		
							$out[] = array(
									'name'		=> '',
									'desc'		=> $child['title'],
									'values'	=>array( array(
															'from'			=>	'/Date('.datetomili($child['start_date']).')/',
															'to'			=>	'/Date('.datetomili($child['end_date']).')/',
															'label'			=>	$child['title'],
															'desc'			=>	$child['description'],
															'customClass'	=>	$class
														))
									);
						}
					}
				}
			}
		}

		return json_encode($out);
	}

	protected function getChildren($pid=0, $depth=1)
	{
		$sub = $this->activities;
			
		if(!isset($sub[$pid])) return;
	
		while (list($parent,$category) = each($sub[$pid]))
		{
			
			$this->acttree[] = array('info' => $category, 'depth' => $depth);
			
			$this->getChildren($category['id'],$depth+1);
		}
	}

	public function viewAction()
	{

		$id = $this->_getParam('id');

		$activity = $this->trackModel->getActivity($id);
		
		if ( empty($activity) )
		{
			throw new Exception('Invalid Activity ID');
		}
		
		$this->view->title = "View Activity";

		$form = new Thesis_Form_Activity();
		
		//populate
		$activityList = $this->trackModel->getActivitiesForSelect($activity['created_by']);
		foreach ( $activityList as $act_id => $act_name )
		{
			$form->pid->addMultiOption($act_id, $act_name);
		}
		
		$form->populate($activity);

		//comments
		$this->view->comments = $this->trackModel->getComments($id);
		$this->view->activity = $activity;
		
		//disabled
		$form->pid->setAttrib('disabled','disabled');
		$form->title->setAttrib('disabled','disabled');
		$form->description->setAttrib('disabled','disabled');
		$form->start_date->setAttrib('disabled','disabled');
		$form->end_date->setAttrib('disabled','disabled');

		//post
		if ( $this->getRequest()->isPost() && $this->_request->getPost ( 'save' ) )
		{
			$formData = $this->getRequest()->getPost();
			

			//add 
			$data = array(
							
							'status'			=> $formData['status'],
							'updated_by'		=> $this->userId,
							'updated_date'		=> new Zend_Db_Expr('NOW()'),
							'updated_type'		=> 'supervisor'
						);
			
			$activity_id = $this->trackModel->updateActivity($data, array('id=?'=>$id));
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "Activity updated"));
		 
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'managetracking', 'action'=>'view', 'id' => $id),'default',true),array('prependBase'=>false));

			
			
		}

		//comment
		if ( $this->getRequest()->isPost() && $this->_request->getPost ( 'addcomment' ) )
		{
			$formData = $this->getRequest()->getPost();
			
			$postedName = $this->userInfo['FullName'];

			//add feedback
			$data = array(
							'c_pid'			=>	$id,
							'comment'		=>	$formData['comment'], 
							'posted_type'	=>	'supervisor', 
							'posted_date'	=>  new Zend_Db_Expr('NOW()'), 
							'posted_by'		=>  $this->userId, 
							'posted_name'	=>	$postedName
						);

			$comment_id = $this->trackModel->addComments($data);

			//update parent

			$data2 = array(
							'totalcomments'		=> new Zend_Db_Expr('totalcomments+1'),
							'lastcomment_name'	=> $postedName,
							'lastcomment_date'	=> new Zend_Db_Expr('NOW()'),
							'lastcomment_id'	=> $this->userId,
							'lastcomment_type'	=> 'supervisor'
					);

			$this->trackModel->updateActivity($data2, 'id='.(int)$id);
			

			//upload file
			try 
			{
				$uploadDir = $this->uploadDir.'/'.$activity['created_by'];

				if ( !is_dir( $uploadDir ) )
				{
					if ( mkdir_p($uploadDir) === false )
					{
						throw new Exception('Cannot create student document folder ('.$uploadDir.')');
					}
				}

				$adapter = new Zend_File_Transfer_Adapter_Http();
			
				$files = $adapter->getFileInfo();
				$adapter->addValidator('NotExists', false, $uploadDir );
				$adapter->setDestination( $this->uploadDir );
				$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
				$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));

				foreach ($files as $no => $fileinfo) 
				{
					$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
				
					if ($adapter->isUploaded($no))
					{
						$ext = getext($fileinfo['name']);

						$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
						$fileUrl = $uploadDir.'/'.$fileName;

						$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
						$adapter->setOptions(array('useByteString' => false));
						
						$size = $adapter->getFileSize($no);
					
						if( !$adapter->receive($no))
						{
							$errmessage = array();
							if ( is_array($adapter->getMessages() ))
							{	
								foreach(  $adapter->getMessages() as $errtype => $errmsg )
								{
									$errmessage[] = $errmsg;
								}
								
								throw new Exception(implode('<br />',$errmessage));
							}
						}
						
						//save file into db
						$data = array(
										'af_pid'			=> $comment_id,
										'af_type'			=> 'comments',
										'af_filename'		=> $fileinfo['name'],
										'af_fileurl'		=> '/thesis/'.$activity['created_by'].'/'.$fileName,
										'af_filesize'		=> $size
									);
							
						$this->trackModel->addFile($data);
						
					} //isuploaded
					
				} //foreach
			}
			catch (Exception $e) 
			{
				throw new Exception( $e->getMessage() );
			}

			
			$this->_helper->flashMessenger->addMessage(array('success' => "New feedback posted"));
		 
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'managetracking', 'action'=>'view', 'id' => $id),'default',true),array('prependBase'=>false));
		}
			

		//views
		$this->view->form = $form;
	}

	public function dateDifference($date1, $date2)
	{
	    $d1 = (is_string($date1) ? strtotime($date1) : $date1);
	    $d2 = (is_string($date2) ? strtotime($date2) : $date2);
	
	    $diff_secs = abs($d1 - $d2);
	    $base_year = min(date("Y", $d1), date("Y", $d2));
	
	    $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
	
	    return array
	    (
	        "years"                 => abs(substr(date('Ymd', $d1) - date('Ymd', $d2), 0, -4)),
	        "months_total"  => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,
	        "months"                => date("n", $diff) - 1,
	        "days_total"    => floor($diff_secs / (3600 * 24)),
	        "days"                  => date("j", $diff) - 1,
	        "hours_total"   => floor($diff_secs / 3600),
	        "hours"                 => date("G", $diff),
	        "minutes_total" => floor($diff_secs / 60),
	        "minutes"               => (int) date("i", $diff),
	        "seconds_total" => $diff_secs,
	        "seconds"               => (int) date("s", $diff)
	    );
	}
}