<?php

class Thesis_ReportController extends Zend_Controller_Action
{
    public function init()
    {

        $session = new Zend_Session_Namespace('sis');
        $this->session = $session;

        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->emailModel = new App_Model_Email();
        $this->thesisModel = new Thesis_Model_DbTable_General();
        $this->regModel = new Thesis_Model_DbTable_Registration();

        $this->trackModel = new Thesis_Model_DbTable_Tracking();
        //locale setup
        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->locales = $this->view->locales = array(
            'en_US' => 'English',
            'ms_MY' => 'Malay'
        );


        $this->auth = Zend_Auth::getInstance();
        $this->uploadDir = DOCUMENT_PATH . '/thesis';

        $this->userId = $this->view->userId = $this->auth->getIdentity()->iduser;
        $this->userInfo = $this->view->userInfo = $this->auth->getIdentity()->info;

        Zend_Layout::getMvcInstance()->assign('navActive', 'report');
    }

    public function indexAction()
    {
        $this->view->title = 'Progress Report';

        //activities
        $proposal = $this->regModel->getProposalSuperviseeRep($this->userId,null,null,1);
//		$articleship = $this->regModel->getArticleshipSupervisee($this->userId);
//		$exemption = $this->regModel->getExemptionSupervisee($this->userId);
        $form = new App_Form_Supervisee();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);
//            $proposal = $this->regModel->getProposalSupervisee($this->userId,$formData);
            $proposal = $this->regModel->getProposalSuperviseeRep($this->userId,$formData);
        }


        foreach ($proposal as $index => $prop) {
            $proposal[$index]['type'] = 'Proposal';
        }

        /*foreach ( $articleship as $art )
        {
            $maindata[] = array(
                                    'research_id'	=>	$art['a_id'],
                                    'student_id'	=>	$art['student_id'],
                                    'type'			=>	'Articleship',
                                    'status_code'	=>	$art['status_code'],
                                    'registrationId'=>	$art['registrationId'],
                                    'student_name'	=>	$art['student_name'],
                                    'created_date'	=>	$art['created_date'],
                                    'updated_date'	=>	$art['updated_date']
                                );
        }

        foreach ( $exemption as $exm )
        {
            $maindata[] = array(
                                    'research_id'	=>	$exm['e_id'],
                                    'student_id'	=>	$exm['student_id'],
                                    'type'			=>	'PPP',
                                    'status_code'	=>	$exm['status_code'],
                                    'registrationId'=>	$exm['registrationId'],
                                    'student_name'	=>	$exm['student_name'],
                                    'created_date'	=>	$exm['created_date'],
                                    'updated_date'	=>	$exm['updated_date']
                                );
        }*/

        $this->view->tracking = $proposal;
        $this->view->form = $form;
    }

    public function listAction()
    {
        $this->view->title = 'Progress Report List';

        $id = $this->_getParam('id');
        $type = $this->_getParam('type');
        $studid = $this->_getParam('studid');
        $courseid = $this->_getParam('courseid');

        $sup = $this->auth->getIdentity()->iduser;

//        $listing = $this->trackModel->getReports($id, $type);
        $listing = $this->trackModel->getReportsSupervisor(null,$type,$studid, $courseid);
        $proposal = $this->regModel->getProposal($id);

        $this->view->id = $id;
        $this->view->type = $type;
        $this->view->listing = $listing;
        $this->view->studid = $studid;
        $this->view->proposal = $proposal;
    }

    public function addAction()
    {
        $form = new Thesis_Form_ReportProgress();

        $this->view->title = 'New Progress Report';

        $id = $this->_getParam('id');
        $type = $this->_getParam('type');
        $studid = $this->_getParam('studid');

        $proposal = $this->regModel->getProposal($id);
        $semesterModel = new App_Model_General_DbTable_Semestermaster();
        $currentSemester = $semesterModel->getCurrentSemesterScheme($proposal['IdScheme']);

        //timeoptions
        $form->time_from->addMultiOption('', 'From');
        $form->time_to->addMultiOption('', 'To');

        for ($hours = 0; $hours < 24; $hours++) {
            for ($mins = 0; $mins < 60; $mins += 30) {
                $curtime = str_pad($hours, 2, '0', STR_PAD_LEFT) . ':' . str_pad($mins, 2, '0', STR_PAD_LEFT);
                $form->time_from->addMultiOption($curtime, $curtime);
                $form->time_to->addMultiOption($curtime, $curtime);
            }
        }


        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();


            if ($form->isValid($formData)) {

                $data = array(
                    'student_id'    => $formData['student_id'],
                    'research_id'   => $formData['research_id'],
                    'research_type' => $formData['research_type'],
                    'semester_id'   => $formData['semester_id'],
                    'meeting_date'  => $formData['meeting_date'],
                    'next_date'     => $formData['next_date'],
                    'itemdiscussed' => $formData['itemdiscussed'],
                    'nextaction'    => $formData['nextaction'],
                    'time_from'     => $formData['time_from'],
                    'time_to'       => $formData['time_to'],
                    'status'        => 1,
                    'created_role'  => 'supervisor',
                    'created_by'    => $this->auth->getIdentity()->iduser,
                    'created_date'  => new Zend_Db_Expr('NOW()')
                );

                $report_id = $this->trackModel->addReport($data);


                $this->_helper->flashMessenger->addMessage(array('success' => "New progress report succesfully created"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'report', 'action' => 'list', 'id' => $formData['research_id'], 'type' => $formData['research_type'], 'studid' => $formData['student_id']), 'default', true));

            } // valid


        }

        $this->view->id = $id;
        $this->view->type = $type;
        $this->view->studid = $studid;
        $this->view->form = $form;
        $this->view->currentSemester = $currentSemester;
    }

    public function viewAction()
    {
        $form = new Thesis_Form_ReportProgress();

        $this->view->title = 'View Progress Report';

        $id = $this->_getParam('id');
        $sup = $this->auth->getIdentity()->iduser;
        $report = $this->trackModel->getReport($id);

        $this->view->comments = $this->trackModel->getComments($id, 'report',$report['research_id'],$report['research_type']);

        $tt = $this->view->super = $this->regModel->getSupervisor($report['research_id'],$report['research_type'],$sup);

        //timeoptions
        $form->time_from->addMultiOption('', 'From');
        $form->time_to->addMultiOption('', 'To');

        for ($hours = 0; $hours < 24; $hours++) {
            for ($mins = 0; $mins < 60; $mins += 30) {
                $curtime = str_pad($hours, 2, '0', STR_PAD_LEFT) . ':' . str_pad($mins, 2, '0', STR_PAD_LEFT);
                $form->time_from->addMultiOption($curtime, $curtime);
                $form->time_to->addMultiOption($curtime, $curtime);
            }
        }

        $form->populate($report);

        //comment
        if ($this->getRequest()->isPost() && $this->_request->getPost('addcomment')) {
            $formData = $this->getRequest()->getPost();

            $postedName = $this->userInfo['FullName'];

            //add feedback
            $data = array(
                'c_pid'       => $id,
                'c_type'      => 'report',
                'comment'     => $formData['comment'],
                'posted_type' => 'supervisor',
                'posted_date' => new Zend_Db_Expr('NOW()'),
                'posted_by'   => $this->userId,
                'posted_name' => $postedName,
                'tag_student' => 'supervisor'
            );

            $comment_id = $this->trackModel->addComments($data);

            //update parent

            $data2 = array(
                'totalcomments'    => new Zend_Db_Expr('totalcomments+1'),
                'lastcomment_name' => $postedName,
                'lastcomment_date' => new Zend_Db_Expr('NOW()'),
                'lastcomment_id'   => $this->userId,
                'lastcomment_type' => 'supervisor'
            );

            $this->trackModel->updateReport($data2, 'id=' . (int)$id);


            //upload file
            try {
                $uploadDir = $this->uploadDir . '/' . $this->studentInfo['IdStudentRegistration'];

                if (!is_dir($uploadDir)) {
                    if (mkdir_p($uploadDir) === false) {
                        throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                    }
                }

                $adapter = new Zend_File_Transfer_Adapter_Http();

                $files = $adapter->getFileInfo();
                $adapter->addValidator('NotExists', false, $uploadDir);
                $adapter->setDestination($this->uploadDir);
                $adapter->addValidator('Count', false, array('min' => 1, 'max' => 100));
                $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                foreach ($files as $no => $fileinfo) {
                    $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                    if ($adapter->isUploaded($no)) {
                        $ext = getext($fileinfo['name']);

                        $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                        $fileUrl = $uploadDir . '/' . $fileName;

                        $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                        $adapter->setOptions(array('useByteString' => false));

                        $size = $adapter->getFileSize($no);

                        if (!$adapter->receive($no)) {
                            $errmessage = array();
                            if (is_array($adapter->getMessages())) {
                                foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                    $errmessage[] = $errmsg;
                                }

                                throw new Exception(implode('<br />', $errmessage));
                            }
                        }

                        //save file into db
                        $data = array(
                            'af_pid'      => $comment_id,
                            'af_type'     => 'comments',
                            'af_filename' => $fileinfo['name'],
                            'af_fileurl'  => '/thesis/' . $this->studentInfo['IdStudentRegistration'] . '/' . $fileName,
                            'af_filesize' => $size
                        );

                        $this->trackModel->addFile($data);

                    } //isuploaded

                } //foreach
            } catch (Exception $e) {
//                throw new Exception($e->getMessage());
                $this->_helper->flashMessenger->addMessage(array('error' => "Attachment format cannot be accepted"));
                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'report', 'action' => 'view', 'id' => $id), 'default', true), array('prependBase' => false));
            }


            $this->_helper->flashMessenger->addMessage(array('success' => "New feedback posted"));

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'report', 'action' => 'view', 'id' => $id), 'default', true), array('prependBase' => false));
        }

        //comment
        if ($this->getRequest()->isPost() && $this->_request->getPost('addcommentstudent')) {
            $formData = $this->getRequest()->getPost();

            $postedName = $this->userInfo['FullName'];

            //add feedback
            $data = array(
                'c_pid'       => $id,
                'c_type'      => 'report',
                'comment'     => $formData['comment'],
                'posted_type' => 'supervisor',
                'posted_date' => new Zend_Db_Expr('NOW()'),
                'posted_by'   => $this->userId,
                'posted_name' => $postedName,
                'tag_student' => 'student'
            );

            $comment_id = $this->trackModel->addComments($data);

            //update parent

            $data2 = array(
                'totalcomments'    => new Zend_Db_Expr('totalcomments+1'),
                'lastcomment_name' => $postedName,
                'lastcomment_date' => new Zend_Db_Expr('NOW()'),
                'lastcomment_id'   => $this->userId,
                'lastcomment_type' => 'supervisor'
            );

            $this->trackModel->updateReport($data2, 'id=' . (int)$id);


            //upload file
            try {
                $uploadDir = $this->uploadDir . '/' . $this->studentInfo['IdStudentRegistration'];

                if (!is_dir($uploadDir)) {
                    if (mkdir_p($uploadDir) === false) {
                        throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                    }
                }

                $adapter = new Zend_File_Transfer_Adapter_Http();

                $files = $adapter->getFileInfo();
                $adapter->addValidator('NotExists', false, $uploadDir);
                $adapter->setDestination($this->uploadDir);
                $adapter->addValidator('Count', false, array('min' => 1, 'max' => 100));
                $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                foreach ($files as $no => $fileinfo) {
                    $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                    if ($adapter->isUploaded($no)) {
                        $ext = getext($fileinfo['name']);

                        $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                        $fileUrl = $uploadDir . '/' . $fileName;

                        $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                        $adapter->setOptions(array('useByteString' => false));

                        $size = $adapter->getFileSize($no);

                        if (!$adapter->receive($no)) {
                            $errmessage = array();
                            if (is_array($adapter->getMessages())) {
                                foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                    $errmessage[] = $errmsg;
                                }

                                throw new Exception(implode('<br />', $errmessage));
                            }
                        }

                        //save file into db
                        $data = array(
                            'af_pid'      => $comment_id,
                            'af_type'     => 'comments',
                            'af_filename' => $fileinfo['name'],
                            'af_fileurl'  => '/thesis/' . $this->studentInfo['IdStudentRegistration'] . '/' . $fileName,
                            'af_filesize' => $size
                        );

                        $this->trackModel->addFile($data);

                    } //isuploaded

                } //foreach
            } catch (Exception $e) {
//                throw new Exception($e->getMessage());
                $this->_helper->flashMessenger->addMessage(array('error' => "Attachment format cannot be accepted"));
                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'report', 'action' => 'view', 'id' => $id), 'default', true), array('prependBase' => false));
            }


            $this->_helper->flashMessenger->addMessage(array('success' => "New feedback posted"));

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'report', 'action' => 'view', 'id' => $id), 'default', true), array('prependBase' => false));
        }

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if($formData['approval'] == 'Approve')
            {
                //send email

                $emailDb = new App_Model_Email();
                $commDb = new App_Model_Template();

                $research = $this->regModel->getProposal($report['research_id']);

                $gettemplate = $commDb->getTemplatesByCategory('thesis', 'progress-report-verification', 0, 1);
                $template = $gettemplate[0];

                $research['research_type'] = 'proposal';

                if (!isset($research)) {
                    throw new Exception('Invalid Research');
                }

                if (empty($template)) {
                    throw new Exception('Invalid Template. Did you change the template name recently?');
                }

                //get supervisors
                $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($research['p_id'], 'proposal');

                $supervisor_name = array();

                foreach ($supervisors as $key => $sup_name) {
                    $newkey = $key + 1;
                    $supervisor_name[] = $newkey.'. '.$sup_name['supervisor_name'];
                }

                $sname = "<p>".implode('<br>', $supervisor_name)."</p>";

                $research['supervisor_info'] = $sname;

                //email
                $dataEmail = array(
                    'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $research['student_email'],
                    'subject' => $template['tpl_name'],
                    'content' => Thesis_Model_DbTable_Registration::parseContent($research, $template['tpl_content']),
                    'date_que' => date('Y-m-d H:i:s')
                );

                $emailDb->add($dataEmail);
                //send email
            }
            else
                {
                //send email

                $emailDb = new App_Model_Email();
                $commDb = new App_Model_Template();

                $research = $this->regModel->getProposal($report['research_id']);

                $gettemplate = $commDb->getTemplatesByCategory('thesis', 'progress-report-rejection', 0, 1);
                $template = $gettemplate[0];

                $research['research_type'] = 'proposal';

                if (!isset($research)) {
                    throw new Exception('Invalid Research');
                }

                if (empty($template)) {
                    throw new Exception('Invalid Template. Did you change the template name recently?');
                }

                //get supervisors
                $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($research['p_id'], 'proposal');

                $supervisor_name = array();

                foreach ($supervisors as $key => $sup_name) {
                    $newkey = $key + 1;
                    $supervisor_name[] = $newkey.'. '.$sup_name['supervisor_name'];
                }

                $sname = "<p>".implode('<br>', $supervisor_name)."</p>";

                $research['supervisor_info'] = $sname;

                //email
                $dataEmail = array(
                    'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $research['student_email'],
                    'subject' => $template['tpl_name'],
                    'content' => Thesis_Model_DbTable_Registration::parseContent($research, $template['tpl_content']),
                    'date_que' => date('Y-m-d H:i:s')
                );

                $emailDb->add($dataEmail);
                //send email
            }

            $data = array(
                'status'       => ($formData['approval'] == 'Approve') ? 1 : 0,
                'updated_by'   => $this->userId,
                'updated_role' => 'supervisor',
                'updated_date' => new Zend_Db_Expr('NOW()')
            );

            $this->trackModel->updateReport($data, 'id=' . (int)$id);

            $this->_helper->flashMessenger->addMessage(array('success' => "Progress report updated"));

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'report', 'action' => 'view', 'id' => $id), 'default', true));


        }

        $this->view->form = $form;
        $this->view->report = $report;
    }

    public function deleteAction()
    {

        $id = $this->_getParam('id');

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();


            if ($form->isValid($formData)) {

                $data = array(
                    'student_id'    => $formData['student_id'],
                    'research_id'   => $formData['research_id'],
                    'research_type' => $formData['research_type'],
                    'meeting_date'  => $formData['meeting_date'],
                    'next_date'     => $formData['next_date'],
                    'itemdiscussed' => $formData['itemdiscussed'],
                    'nextaction'    => $formData['nextaction'],
                    'time_from'     => $formData['time_from'],
                    'time_to'       => $formData['time_to'],
                    'created_by'    => $this->userId,
                    'created_date'  => new Zend_Db_Expr('NOW()')
                );

                $report_id = $this->trackModel->addReport($data);


                $this->_helper->flashMessenger->addMessage(array('success' => "New progress report succesfully created"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'report', 'action' => 'list', 'id' => $formData['research_id'], 'type' => $formData['research_type'], 'studid' => $formData['student_id']), 'default', true));

            } // valid


        }

        $this->view->id = $id;
        $this->view->type = $type;
        $this->view->studid = $studid;
        $this->view->form = $form;
    }

    public function downloadProgressReportSupervisorAction(){

        $this->_helper->layout->disableLayout();

        $id = $this->_getParam('id');
        $student_id = $this->_getParam('student_id');
        $type = $this->_getParam('type');
        $sem_id = $this->_getParam('sem');

        $listing = $this->trackModel->getReports($id, $type, $sem_id);
//dd($listing);
        $this->view->listing = $listing;

        $this->view->filename = $student_id.'_'.date('Ymd').'_progress_report.xls';
    }
}
