<?php

class Thesis_GradingController extends Zend_Controller_Action
{
    public function init()
    {

        $session = new Zend_Session_Namespace('sis');
        $this->session = $session;

        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->emailModel = new App_Model_Email();
        $this->thesisModel = new Thesis_Model_DbTable_General();
        $this->regModel = new Thesis_Model_DbTable_Registration();
        $this->evalModel = new Thesis_Model_DbTable_Evaluation();

        //locale setup
        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->locales = $this->view->locales = array(
            'en_US' => 'English',
            'ms_MY' => 'Malay'
        );


        $this->auth = Zend_Auth::getInstance();
        $this->uploadDir = DOCUMENT_PATH . '/thesis';

        $this->userId = $this->view->userId = $this->auth->getIdentity()->iduser;
        $this->userInfo = $this->view->userInfo = $this->auth->getIdentity()->info;

        Zend_Layout::getMvcInstance()->assign('navActive', 'grading');
    }

    public function indexAction()
    {
        $this->view->title = 'Grading';

        $status = $this->thesisModel->getStatusByCode('APPROVED');

        //activities
        $proposal = $this->regModel->getProposalSupervisee($this->userId, array('p_status' => $status));
        $articleship = $this->regModel->getArticleshipSupervisee($this->userId, array('status' => $status));
        $exemption = $this->regModel->getExemptionSupervisee($this->userId, array('status' => $status));

        $maindata = array();

        foreach ($proposal as $prop) {
            $maindata[] = array(
                'student_id'     => $prop['student_id'],
                'type'           => 'Proposal',
                'research_id'    => $prop['p_id'],
                'research_type'  => 'proposal',
                'status_code'    => $prop['status_code'],
                'registrationId' => $prop['registrationId'],
                'student_name'   => $prop['student_name'],
                'created_date'   => $prop['created_date'],
                'updated_date'   => $prop['updated_date']
            );
        }

        foreach ($articleship as $art) {
            $maindata[] = array(
                'student_id'     => $art['student_id'],
                'type'           => 'Articleship',
                'research_id'    => $art['a_id'],
                'research_type'  => 'articleship',
                'status_code'    => $art['status_code'],
                'registrationId' => $art['registrationId'],
                'student_name'   => $art['student_name'],
                'created_date'   => $art['created_date'],
                'updated_date'   => $art['updated_date']
            );
        }

        foreach ($exemption as $exm) {
            $maindata[] = array(
                'student_id'     => $exm['student_id'],
                'type'           => 'PPP',
                'research_id'    => $exm['e_id'],
                'research_type'  => 'exemption',
                'status_code'    => $exm['status_code'],
                'registrationId' => $exm['registrationId'],
                'student_name'   => $exm['student_name'],
                'created_date'   => $exm['created_date'],
                'updated_date'   => $exm['updated_date']
            );
        }

        $this->view->results = $maindata;
    }

    public function listAction()
    {
        $this->view->title = 'Grading Details';

        $set_id = $this->_getParam('set_id');
        $student_id = $this->_getParam('student_id');
        $errMsg = '';

        $results = $this->evalModel->getEvaluationSheet(0, $student_id);

        if (empty($results)) {
            $errMsg = 'No Evaluation Found For Selected Student.';
        }

        //get set
        $set = '';
        if (!empty($results)) {
            $set = $this->evalModel->getEvaluationSetById($results[0]['set_id']);

            switch ($results[0]['research_type']) {
                case 'proposal':
                    $research = $this->regModel->getProposal($results[0]['research_id']);
                    break;

                case 'articleship':
                    $research = $this->regModel->getArticleship($results[0]['research_id']);
                    break;

                case 'exemption':
                    $research = $this->regModel->getExemption($results[0]['research_id']);
                    break;
            }

            //updated
            $updated = array();
            if ($research['eval_by'] != '') {
                $updated = $this->regModel->getUserByRole($research['eval_by'], 'admin');
            }

            $this->view->updated = $updated;

            if (empty($research)) {
                $errMsg = 'Invalid Research';
            }

            $this->view->research = $research;
        }


        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if (empty($results)) {
                throw new Exception($errMsg);
            }

            $data = array(
                'eval_grade' => $formData['eval'],
                'eval_marks' => $formData['marks'],
                'eval_role'  => $this->auth->getIdentity()->role,
                'eval_by'    => $this->auth->getIdentity()->iduser,
                'eval_date'  => new Zend_Db_Expr('NOW()')
            );

            switch ($results[0]['research_type']) {
                case 'proposal':
                    $this->regModel->updateProposal($data, array('p_id = ?' => $results[0]['research_id']));
                    break;
                case 'articleship':
                    $this->regModel->updateArticleship($data, array('a_id = ?' => $results[0]['research_id']));
                    break;
                case 'exemption':
                    $this->regModel->updateExemption($data, array('e_id = ?' => $results[0]['research_id']));
                    break;

            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Grading successfully updated"));


            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'grading', 'action' => 'list', 'student_id' => $results[0]['student_id']), 'default', true));

        }


        $this->view->set_id = $set_id;
        $this->view->student_id = $student_id;
        $this->view->results = $results;
        $this->view->set = $set;
        $this->view->errMsg = $errMsg;
    }

    public function ipAction()
    {
        $db = getDb();

        //title
        $this->view->title = "Grading";

        $type = $this->_getParam('type', 'proposal');

        $status = $this->thesisModel->getStatusByCode('approved', 1);

        $gradingList = $this->defModel->getDataByTypeCode('Research Grading');

        $icem = new Thesis_Model_DbTable_General();

        $icem_grade = $icem->researchIcemData();

//        print_r($icem_grade);exit;

        $form = new Thesis_Form_SearchGradingIP();

        $this->view->data = array();

        if ($this->_request->isPost() && $this->_request->getPost('gradesubmit')) {
            $formData = $this->getRequest()->getPost();

            foreach ($formData['student'] as $i => $proposal_id) {

                $db->update('thesis_proposal', array(
                    'grade_remarks' => $formData['remarks'][$proposal_id],
                    'total_marks'   => $formData['total_marks'][$proposal_id],
                    'grade_status'  => $formData['grade'][$proposal_id],
                    'grade_by'      => $this->auth->getIdentity()->iduser,
                    'grade_date'    => new Zend_Db_Expr('NOW()')
                ), array('p_id = ?' => $proposal_id));

            }

            $this->_helper->flashMessenger->addMessage(array('success' => "Grading successfully updated"));

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'grading', 'action' => 'ip'), 'default', true));


        }

        if ($this->getRequest()->isPost() || ($this->_getParam('semester_id') != '' && $this->_getParam('course_id') != '')) {
            $formData = $this->getRequest()->getPost();

            $form->populate($formData);

            $searchdata = array(
                'supervisor_id' => $this->userId,
                'p_status'      => $status,
                'semester_id'   => $formData['semester_id'],
                'course_id'     => $formData['course_id'],
                'student_id'    => $formData['student_id'],
                'student_name'  => $formData['student_name']
            );

            $p_data = $this->evalModel->getGradingIP($searchdata);

            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($p_data));
            $paginator->setItemCountPerPage(25);
            $paginator->setCurrentPageNumber($this->_getParam('page', 1));

            $this->view->data = $formData;
        } else {
            $paginator = Zend_Paginator::factory(array());
        }


        $this->view->paginator = $paginator;
        $this->view->type = $type;
        $this->view->form = $form;
        $this->view->gradinglist = $gradingList;
    }
}
