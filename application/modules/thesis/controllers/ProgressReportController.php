<?php

class Thesis_ProgressreportController extends Zend_Controller_Action
{
    public function init()
    {

        $session = new Zend_Session_Namespace('sis');
        $this->session = $session;

        $this->defModel = new App_Model_General_DbTable_Definationms();
        $this->emailModel = new App_Model_Email();
        $this->thesisModel = new Thesis_Model_DbTable_General();
        $this->regModel = new Thesis_Model_DbTable_Registration();

        $this->trackModel = new Thesis_Model_DbTable_Tracking();
        //locale setup
        $this->currLocale = Zend_Registry::get('Zend_Locale');
        $this->locales = $this->view->locales = array(
            'en_US' => 'English',
            'ms_MY' => 'Malay'
        );


        $this->auth = Zend_Auth::getInstance();
        $this->uploadDir = DOCUMENT_PATH . '/thesis';

        $this->studentInfo = $this->view->userInfo = $this->auth->getIdentity()->info;
        $this->studentId = $this->view->studentId = $this->studentInfo['IdStudentRegistration'];

        Zend_Layout::getMvcInstance()->assign('navActive', 'progressreport');
    }

    public function indexAction()
    {
        $this->view->title = 'Progress Report';

        //activities
        $proposal = $this->regModel->getProposalListByStudent($this->studentId);
//		$articleship = $this->regModel->getArticleshipByStudent($this->studentId);
//		$exemption = $this->regModel->getExemptionByStudent($this->studentId);


//		$this->view->articleship = $articleship;
//		$this->view->exemption = $exemption;

        if (!empty($proposal)) {

            foreach ($proposal as $index => $pro) {
                $proposal[$index]['supervisors'] = $this->regModel->getSupervisors($pro['p_id'], 'proposal');
            }
//			$this->view->supervisors = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');
        }


        $this->view->proposal = $proposal;

        /*if ( !empty($articleship) )
        {
            $this->view->supervisors = $this->regModel->getSupervisors($articleship['a_id'], 'articleship');
        }

        if ( !empty($exemption) )
        {
            $this->view->supervisors = $this->regModel->getSupervisors($exemption['e_id'], 'exemption');
        }*/

        /*$maindata = array();

        if ( !empty($proposal) )
        {
            $maindata[] = array(
                                        'research_id'	=>	$proposal['p_id'],
                                        'student_id'	=>	$proposal['student_id'],
                                        'type'			=>	'Proposal',
                                        'status_code'	=>	$proposal['status_name'],
                                        'registrationId'=>	$proposal['registrationId'],
                                        'student_name'	=>	$proposal['student_name'],
                                        'created_date'	=>	$proposal['created_date'],
                                        'updated_date'	=>	$proposal['updated_date']
                                    );
        }


        if ( !empty($articleship) )
        {
            $maindata[] = array(
                                        'research_id'	=>	$articleship['a_id'],
                                        'student_id'	=>	$articleship['student_id'],
                                        'type'			=>	'Articleship',
                                        'status_code'	=>	$articleship['status_code'],
                                        'registrationId'=>	$articleship['registrationId'],
                                        'student_name'	=>	$articleship['student_name'],
                                        'created_date'	=>	$articleship['created_date'],
                                        'updated_date'	=>	$articleship['updated_date']
                                    );
        }

        if ( !empty($exemption) )
        {
            $maindata[] = array(
                                        'research_id'	=>	$exemption['e_id'],
                                        'student_id'	=>	$exemption['student_id'],
                                        'type'			=>	'PPP',
                                        'status_code'	=>	$exemption['status_code'],
                                        'registrationId'=>	$exemption['registrationId'],
                                        'student_name'	=>	$exemption['student_name'],
                                        'created_date'	=>	$exemption['created_date'],
                                        'updated_date'	=>	$exemption['updated_date']
                                    );
        }*/


        //$this->view->tracking = $maindata;
    }

    public function listAction()
    {
        $this->view->title = 'Progress Report List';

        $id = $this->_getParam('id');
        $type = $this->_getParam('type');
        $studid = $this->_getParam('studid');
        $sem_id = $this->_getParam('semid', null);

        $semesters = [];

        $latestSem = $this->trackModel->getStudentLatestSemester($studid);

        foreach ($this->trackModel->getReportSemesters($id, $type) as $sem) {
            $semesters[$sem['semester_id']] = $sem['SemesterMainCode'];
        }


        $listing = $this->trackModel->getReports($id, $type, $sem_id);
        $proposal = $this->regModel->getProposal($id);

        $this->view->id = $id;
        $this->view->type = $type;
        $this->view->listing = $listing;
        $this->view->studid = $studid;
        $this->view->semesters = $semesters;
        $this->view->sem_id = $sem_id;
        $this->view->proposal = $proposal;

    }

    public function addAction()
    {
        $form = new Thesis_Form_ReportProgress();

        $this->view->title = 'New Progress Report';

        $id = $this->_getParam('id');
        $type = $this->_getParam('type');
        $studid = $this->_getParam('studid');
        $sem_id = $this->_getParam('sem_id', null);

        $proposal = $this->regModel->getProposal($id);
        $semesterModel = new App_Model_General_DbTable_Semestermaster();
        $currentSemester = $semesterModel->getCurrentSemesterScheme($proposal['IdScheme']);

        $this->view->schemeid = $proposal['IdScheme'];

        //timeoptions
        $form->time_from->addMultiOption('', 'From');
        $form->time_to->addMultiOption('', 'To');

        for ($hours = 0; $hours < 24; $hours++) {
            for ($mins = 0; $mins < 60; $mins += 30) {
                $curtime = str_pad($hours, 2, '0', STR_PAD_LEFT) . ':' . str_pad($mins, 2, '0', STR_PAD_LEFT);
                $form->time_from->addMultiOption($curtime, $curtime);
                $form->time_to->addMultiOption($curtime, $curtime);
            }
        }

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();


            if ($form->isValid($formData)) {

                $data = array(
                    'student_id'    => $formData['student_id'],
                    'research_id'   => $formData['research_id'],
                    'research_type' => $formData['research_type'],
                    'semester_id'   => $formData['semester_id'],
                    'meeting_date'  => $formData['meeting_date'],
                    'next_date'     => $formData['next_date'],
                    'itemdiscussed' => $formData['itemdiscussed'],
                    'nextaction'    => $formData['nextaction'],
                    'time_from'     => $formData['time_from'],
                    'time_to'       => $formData['time_to'],
                    'created_role'  => 'student',
                    'created_by'    => $formData['student_id'],
                    'created_date'  => new Zend_Db_Expr('NOW()')
                );

                $report_id = $this->trackModel->addReport($data);

                //send email

                //get supervisors
                $supervisors = $this->view->supervisors = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');

                foreach ($supervisors as $sup) {

                    $emailDb = new App_Model_Email();
                    $commDb = new App_Model_Template();

                    $gettemplate = $commDb->getTemplatesByCategory('thesis', 'progress-report-submission', 0, 1);
                    $template = $gettemplate[0];

                    $research['research_type'] = 'proposal';

                    if (!isset($research)) {
                        throw new Exception('Invalid Research');
                    }

                    if (empty($template)) {
                        throw new Exception('Invalid Template. Did you change the template name recently?');
                    }

                    $proposal['supervisor_info'] = $sup['supervisor_name'];

                    //email
                    $dataEmail = array(
                        'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $sup['supervisor_email'],
                        'subject' => $template['tpl_name'],
                        'content' => Thesis_Model_DbTable_Registration::parseContent($proposal, $template['tpl_content']),
                        'date_que' => date('Y-m-d H:i:s')
                    );

                    $emailDb->add($dataEmail);

                    if($sup['supervisor_email2'] != NULL){
                        $dataEmail2 = array(
                            'recepient_email' => (SMTP_RECEPIENT_DEFAULT == 1) ? SMTP_RECEPIENT : $sup['supervisor_email2'],
                            'subject' => $template['tpl_name'],
                            'content' => Thesis_Model_DbTable_Registration::parseContent($proposal, $template['tpl_content']),
                            'date_que' => date('Y-m-d H:i:s')
                        );

                        $emailDb->add($dataEmail2);
                    }
                }
                //send email

                $this->_helper->flashMessenger->addMessage(array('success' => "New progress report succesfully created"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'progress-report', 'action' => 'list', 'id' => $formData['research_id'], 'type' => $formData['research_type'], 'studid' => $formData['student_id']), 'default', true));

            } // valid


        }

        $this->view->id = $id;
        $this->view->type = $type;
        $this->view->studid = $studid;
        $this->view->form = $form;
        $this->view->sem_id = $sem_id;
        $this->view->currentSemester = $currentSemester;
    }

    public function viewAction()
    {
        $form = new Thesis_Form_ReportProgress();

        $this->view->title = 'View Progress Report';

        $id = $this->_getParam('id');

        $report = $this->trackModel->getReport($id);

        //comments
        $tt = $this->view->comments = $this->trackModel->getCommentsStudent($id, 'report', 'student');
//        print_r($tt);exit;
        //timeoptions
        $form->time_from->addMultiOption('', 'From');
        $form->time_to->addMultiOption('', 'To');

        for ($hours = 0; $hours < 24; $hours++) {
            for ($mins = 0; $mins < 60; $mins += 30) {
                $curtime = str_pad($hours, 2, '0', STR_PAD_LEFT) . ':' . str_pad($mins, 2, '0', STR_PAD_LEFT);
                $form->time_from->addMultiOption($curtime, $curtime);
                $form->time_to->addMultiOption($curtime, $curtime);
            }
        }

        $form->populate($report);

        //comment
        if ($this->getRequest()->isPost() && $this->_request->getPost('addcomment')) {
            $formData = $this->getRequest()->getPost();

            $postedName = $this->studentInfo['appl_fname'] . ' ' . $this->studentInfo['appl_lname'];

            //add feedback
            $data = array(
                'c_pid'       => $id,
                'c_type'      => 'report',
                'comment'     => $formData['comment'],
                'posted_type' => 'student',
                'posted_date' => new Zend_Db_Expr('NOW()'),
                'posted_by'   => $this->studentInfo['IdStudentRegistration'],
                'posted_name' => $postedName
            );

            $comment_id = $this->trackModel->addComments($data);

            //update parent

            $data2 = array(
                'totalcomments'    => new Zend_Db_Expr('totalcomments+1'),
                'lastcomment_name' => $postedName,
                'lastcomment_date' => new Zend_Db_Expr('NOW()'),
                'lastcomment_id'   => $this->studentInfo['IdStudentRegistration'],
                'lastcomment_type' => 'student'
            );

            $this->trackModel->updateReport($data2, 'id=' . (int)$id);


            //upload file
            try {
                $uploadDir = $this->uploadDir . '/' . $this->studentInfo['IdStudentRegistration'];

                if (!is_dir($uploadDir)) {
                    if (mkdir_p($uploadDir) === false) {
                        throw new Exception('Cannot create student document folder (' . $uploadDir . ')');
                    }
                }

                $adapter = new Zend_File_Transfer_Adapter_Http();

                $files = $adapter->getFileInfo();
                $adapter->addValidator('NotExists', false, $uploadDir);
                $adapter->setDestination($this->uploadDir);
                $adapter->addValidator('Count', false, array('min' => 1, 'max' => 100));
                $adapter->addValidator('Size', false, array('min' => 400, 'max' => 4194304, 'bytestring' => true));

                foreach ($files as $no => $fileinfo) {
                    $adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));

                    if ($adapter->isUploaded($no)) {
                        $ext = getext($fileinfo['name']);

                        $fileName = time() . '-' . md5($fileinfo['name']) . '.' . $ext;
                        $fileUrl = $uploadDir . '/' . $fileName;

                        $adapter->addFilter('Rename', array('target' => $fileUrl, 'overwrite' => true));
                        $adapter->setOptions(array('useByteString' => false));

                        $size = $adapter->getFileSize($no);

                        if (!$adapter->receive($no)) {
                            $errmessage = array();
                            if (is_array($adapter->getMessages())) {
                                foreach ($adapter->getMessages() as $errtype => $errmsg) {
                                    $errmessage[] = $errmsg;
                                }

                                throw new Exception(implode('<br />', $errmessage));
                            }
                        }

                        //save file into db
                        $data = array(
                            'af_pid'      => $comment_id,
                            'af_type'     => 'comments',
                            'af_filename' => $fileinfo['name'],
                            'af_fileurl'  => '/thesis/' . $this->studentInfo['IdStudentRegistration'] . '/' . $fileName,
                            'af_filesize' => $size
                        );

                        $this->trackModel->addFile($data);

                    } //isuploaded

                } //foreach
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }


            $this->_helper->flashMessenger->addMessage(array('success' => "New feedback posted"));

            $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'progress-report', 'action' => 'view', 'id' => $id), 'default', true), array('prependBase' => false));
        }

        //post
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();


            if ($form->isValid($formData)) {

                $data = array(
                    'semester_id'   => $formData['semester_id'],
                    'meeting_date'  => $formData['meeting_date'],
                    'next_date'     => $formData['next_date'],
                    'itemdiscussed' => $formData['itemdiscussed'],
                    'nextaction'    => $formData['nextaction'],
                    'time_from'     => $formData['time_from'],
                    'time_to'       => $formData['time_to'],
                    'updated_by'    => $this->userId,
                    'updated_date'  => new Zend_Db_Expr('NOW()')
                );

                $report_id = $this->trackModel->updateReport($data, 'id=' . (int)$id);


                $this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));

                $this->_redirect($this->view->url(array('module' => 'thesis', 'controller' => 'progress-report', 'action' => 'view', 'id' => $id), 'default', true));

            } // valid


        }

        $this->view->form = $form;
        $this->view->report = $report;
    }

    public function downloadProgressReportAction(){

        $this->_helper->layout->disableLayout();

        $id = $this->_getParam('id');
        $student_id = $this->_getParam('student_id');
        $type = $this->_getParam('type');
        $sem_id = $this->_getParam('sem');

        $listing = $this->trackModel->getReports($id, $type, $sem_id);

        $this->view->listing = $listing;

        $this->view->filename = $student_id.'_'.date('Ymd').'_progress_report.xls';
    }

    public
    function getSemesterAction()
    {
        $date = $this->_getParam('date', 0);
        $IdScheme = $this->_getParam('IdScheme', 0);

        $this->_helper->layout->disableLayout();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('view', 'html');
        $ajaxContext->initContext();

        $result = $this->regModel->getSemesterBasedOnDate($date, $IdScheme);

        $ajaxContext->addActionContext('view', 'html')
            ->addActionContext('form', 'html')
            ->addActionContext('process', 'json')
            ->initContext();

        $json = Zend_Json::encode($result);

        echo $json;
        exit();
    }

}
