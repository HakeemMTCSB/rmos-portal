<?php

class Thesis_EvaluationController extends  Zend_Controller_Action
{
	public function init() {
		
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		
		$this->defModel = new App_Model_General_DbTable_Definationms();
		$this->emailModel = new App_Model_Email();
		$this->thesisModel = new Thesis_Model_DbTable_General();
		$this->regModel = new Thesis_Model_DbTable_Registration();
		$this->evalModel = new Thesis_Model_DbTable_Evaluation();

		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
													  );

		
		$this->auth = Zend_Auth::getInstance();
		$this->uploadDir = DOCUMENT_PATH.'/thesis';
		
		$this->examinerInfo = $this->view->examinerInfo = $this->auth->getIdentity()->info;
		$this->userId = $this->examinerInfo['id'];

		Zend_Layout::getMvcInstance()->assign('navActive', 'evaluation');
	}

	public function indexAction()
	{
		$this->view->title = 'Evaluation';

		$proposal = $this->regModel->getProposalExaminee($this->userId);
		$articleship = $this->regModel->getArticleshipExaminee($this->userId);
		$exemption = $this->regModel->getExemptionExaminee($this->userId);

		$maindata = array();

		foreach ( $proposal as $prop )
		{
			$maindata[] = array(	
									'research_id'	=>	$prop['p_id'],
									'student_id'	=>	$prop['student_id'],
									'type'			=>	'Proposal',
									'status_code'	=>	$prop['status_code'],
									'registrationId'=>	$prop['registrationId'],
									'student_name'	=>	$prop['student_name'],
									'created_date'	=>	$prop['created_date'],
									'updated_date'	=>	$prop['updated_date']
								);
		}

		foreach ( $articleship as $art )
		{
			$maindata[] = array(	
									'research_id'	=>	$art['a_id'],
									'student_id'	=>	$art['student_id'],
									'type'			=>	'Articleship',
									'status_code'	=>	$art['status_code'],
									'registrationId'=>	$art['registrationId'],
									'student_name'	=>	$art['student_name'],
									'created_date'	=>	$art['created_date'],
									'updated_date'	=>	$art['updated_date']
								);
		}

		foreach ( $exemption as $exm )
		{
			$maindata[] = array(	
									'research_id'	=>	$exm['e_id'],
									'student_id'	=>	$exm['student_id'],
									'type'			=>	'PPP',
									'status_code'	=>	$exm['status_code'],
									'registrationId'=>	$exm['registrationId'],
									'student_name'	=>	$exm['student_name'],
									'created_date'	=>	$exm['created_date'],
									'updated_date'	=>	$exm['updated_date']
								);
		}

		$this->view->tracking = $maindata;
	}

	public function viewAction()
	{
		$this->view->title = 'Research Evaluation';

		$id = $this->_getParam('id');
		$type = $this->_getParam('type');
		$studid = $this->_getParam('studid');

		$sheets = $this->evalModel->getEvaluation($this->userId, $id, $type);
		
		$this->view->id = $id;
		$this->view->type = $type;
		$this->view->studid = $studid;
		
		$this->view->sheets = $sheets;
	}

	public function addAction()
	{
		$this->view->title = 'New Evaluation Sheet';

		$id = $this->_getParam('id');
		$type = $this->_getParam('type');
		$studid = $this->_getParam('studid');

		$this->view->id = $id;
		$this->view->type = $type;
		$this->view->studid = $studid;

		$errMsg = '';
		
		//get search
		$info = $this->getResearchInfo($id, $type);
		$this->view->info = $info;

		$form = new Thesis_Form_Evaluation();

		$this->view->form = $form;

		//set options
		$reg_sql = $this->thesisModel->subjectRegistered($studid);
	
		$sets = $this->evalModel->getEvaluationSetData($reg_sql);
		
		if ( empty($sets) )
		{
			$errMsg = 'No evaluation set found.';
		}
		else
		{
			foreach( $sets as $set )
			{
				$form->set_id->addMultiOption($set['id'], $set['name']);
			}
		}

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
		
			$data = array(
								'set_id'					=> $formData['set_id'],
								'examiner_id'				=> $this->userId,
								'student_id'				=> $formData['student_id'],
								'research_id'				=> $formData['research_id'],
								'research_type'				=> $formData['research_type'],
								'sheet_data'				=> '',
								'created_by'				=> $this->userId,
								'created_date'				=> new Zend_Db_Expr('NOW()'),
								'created_role'				=> $this->auth->getIdentity()->role
							);

			$eval_id = $this->evalModel->addEvaluation($data);
			
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'evaluation', 'action'=>'detail', 'id' => $eval_id ),'default',true));

		}

		$this->view->errMsg = $errMsg;

	}
	
	public function submitAction()
	{
		$this->view->title = 'Evaluation Sheet';

		$id = $this->_getParam('id');

		$info = $this->evalModel->getEvaluationById($id);
		$data = array(
						'locked'				=> 1
					);
		
		$eval_id = $this->evalModel->updateEvaluation($data, array('id= ?'=>$id) );
			
		$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'evaluation'),'default',true));
	}

	public function detailAction()
	{
		$this->view->title = 'Evaluation Sheet';

		$id = $this->_getParam('id');

		$info = $this->evalModel->getEvaluationById($id);

		$this->view->info = $info;
		$this->view->researchInfo = $this->getResearchInfo($info['research_id'],$info['research_type']);
		
		$data = $this->view->data = json_decode($info['sheet_data'], true);

		//data
		$items = $this->evalModel->getEvaluationItemsBySet($info['set_id']);
		$groups = $this->evalModel->getEvaluationGroupsBySet($info['set_id']);

		$itemsbygroup=array();
		foreach ( $items as $item )
		{
			$itemsbygroup[$item['group_id']][] = $item;
		}


		$this->view->items = $itemsbygroup;
		$this->view->groups = $groups;

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();

			if ( $info['locked'] == 1 )
			{
				throw new Exception('Evaluation Sheet confirmed submission');
			}
			
			if ( $info['evaluation_type'] == 0 )
			{

				$sheet_data = json_encode($formData['cur_item']);

				$data = array(
									'sheet_data'				=> $sheet_data,
									'updated_by'				=> $this->userId,
									'updated_date'				=> new Zend_Db_Expr('NOW()'),
									'updated_role'				=> $this->auth->getIdentity()->role
								);

				
			}
			else
			{
				$sheet_data = json_encode(array(
													'item'				=> $formData['item'],
													'comments'			=> $formData['comments'],
													'section'			=> $formData['section'],
													'recommendation'	=> $formData['recommendation']
									));
				$data = array(
								'sheet_data'				=> $sheet_data,
								'updated_by'				=> $this->userId,
								'updated_date'				=> new Zend_Db_Expr('NOW()'),
								'updated_role'				=> $this->auth->getIdentity()->role
							);
			}

			$eval_id = $this->evalModel->updateEvaluation($data, array('id=?' => $id) );
			
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'evaluation', 'action'=>'detail', 'id' => $id ),'default',true));

		}

	}

	public function getResearchInfo( $id, $type )
	{
		switch( $type )
		{
			case 'proposal':
				$info = $this->regModel->getProposal($id);
			break;

			case 'articleship':
				$info = $this->regModel->getArticleship($id);
			break;

			case 'ppp':
				$info = $this->regModel->getExemption($id);
			break;
		}
		
		return $info;
	}
}
