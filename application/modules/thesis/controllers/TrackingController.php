<?php

class Thesis_TrackingController extends  Zend_Controller_Action
{
	public function init() {
		
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		
		$this->defModel = new App_Model_General_DbTable_Definationms();
		$this->emailModel = new App_Model_Email();
		$this->thesisModel = new Thesis_Model_DbTable_General();
		$this->regModel = new Thesis_Model_DbTable_Registration();
		$this->trackModel = new Thesis_Model_DbTable_Tracking();
		$this->notifyModel = new Thesis_Model_DbTable_Notify();

		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
													  );

		
		$this->auth = Zend_Auth::getInstance();
		$this->uploadDir = DOCUMENT_PATH.'/thesis';

		$this->studentInfo = $this->auth->getIdentity()->info;
		$this->studentId = $this->view->studentId = $this->studentInfo['IdStudentRegistration'];

		Zend_Layout::getMvcInstance()->assign('navActive', 'tracking');
	}
	
	public function indexAction()
	{
		$this->view->title = 'Tracking';

		//activities
		$proposal = $this->regModel->getProposalByStudent($this->studentId);
		$articleship = $this->regModel->getArticleshipByStudent($this->studentId);
		$exemption = $this->regModel->getExemptionByStudent($this->studentId);

		$this->view->proposal = $proposal;
		$this->view->articleship = $articleship;
		$this->view->exemption = $exemption;
		
		if ( !empty($proposal) )
		{
			$this->view->supervisors = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');
		}

		if ( !empty($proposal) )
		{
			$this->view->supervisors = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');
		}

		if ( !empty($articleship) )
		{
			$this->view->supervisors = $this->regModel->getSupervisors($articleship['a_id'], 'articleship');
		}

		if ( !empty($exemption) )
		{
			$this->view->supervisors = $this->regModel->getSupervisors($exemption['e_id'], 'exemption');
		}
	}

	private function setupActivities( $id, $student_id, $type )
	{
		$activities = $this->trackModel->getActivitySetupData($type);
		
		$status = $this->defModel->getIdByDefType('Research Activity Status', 'INPROGRESS');

		foreach( $activities as $row )
		{	
			$data = array(	
							'research_id'		=> $id,
							'research_type'		=> $type,
							'status'			=> $status['key'],
							'title'				=> $row['title'],
							'description'		=> $row['description'],
							'start_date'		=> null,
							'end_date'			=> null,
							'color'				=> null,
							'created_date'		=> new Zend_Db_Expr('NOW()'),
							'created_by'		=> $student_id,
							'pid'				=> 0,
							'setup_id'			=> $row['id']
						);

			$this->trackModel->addActivity( $data );
		}
	}

	public function overviewAction()
	{
		$this->view->title = 'Tracking';
		
		$this->view->type = $this->_getParam('type');
		$this->view->id = $this->_getParam('id');

		//activities
		$_getactivities = $this->trackModel->getActivitiesByResearch($this->view->id);

		if ( empty($_getactivities) )
		{
			//first time?
			$this->setupActivities( $this->view->id, $this->studentInfo['IdStudentRegistration'], $this->view->type );

			$_getactivities = $this->trackModel->getActivitiesByResearch($this->view->id);
		}

		
		//rebuild array
		$getactivities = array();
		foreach ( $_getactivities as $row)
		{

			if ( $row['start_date'] != '' )
			{
				if ( time() > strtotime($row['end_date']) )
				{
					$getdate = $this->dateDifference(strtotime($row['end_date']),time());
					$row['time_ago'] = '<small class="red">'.$getdate['days_total'].' days past</small>';
					
				}
				else
				{
					$getdate = $this->dateDifference(time(),strtotime($row['end_date']));
					$row['time_ago'] = '<small class="normal">'.($getdate['days_total']+1).' days left</small>';
				}
			}
			else
			{
				$row['time_ago'] = '';
			}

			$getactivities[] = $row;
		}

		//levels by sub
		$this->activities = $this->acttree = array();
		
		$this->_levellist = $getactivities;

		foreach ( $this->_levellist as $activity )
		{
			$this->activities[$activity['pid']][] = $activity;
		}
		
		
		$this->view->jsonGantt = $this->gantify($this->activities);

		//echo $this->view->jsonGantt;
		//exit;

		$this->view->colors = implode(' ', $this->colors );

		$this->getChildren(0);
	
		$this->view->activities = $this->acttree;
	}
	
	//build json for gantt
	protected function gantify($data=array())
	{
		$this->colors = array();

		$out = array();
		/*
		{
			name: "Sprint 0",
			desc: "Analysis",
			values: [{
				from: "/Date(1320192000000)/",
				to: "/Date(1322401600000)/",
				label: "Requirement Gathering",
				desc: "",
				customClass: "ganttRed"
			}]
		}*/
		
		if ( !empty($data[0]) && isset($data[0]) )
		{
			foreach(  $data[0] as $row )
			{
				if ( $row['start_date'] != '' )
				{
					$class = $row['color'] == '' ? 'ganttOrange' : 'color-'.$row['id'];
					if ( $row['color'] != '' )
					{
						$this->colors[] = '.fn-gantt .color-'.$row['id'].' {  background-color: '.$row['color'].' } ';
					}
		
					$out[] = array(
									'name'		=> $row['title'],
									'desc'		=> '',
									'values'	=>array( array(
															'from'			=>	'/Date('.datetomili($row['start_date']).')/',
															'to'			=>	'/Date('.datetomili($row['end_date']).')/',
															'label'			=>	$row['title'],
															'desc'			=>	$row['description'],
															'customClass'	=>	$class
														))
									);
		
					if ( isset($data[$row['id']]) && !empty($data[$row['id']]) )
					{
						foreach( $data[$row['id']] as $child )
						{
							$class = $child['color'] == '' ? 'ganttOrange' : 'color-'.$child['id'];
							if ( $child['color'] != '' )
							{
								$this->colors[] = '.fn-gantt .color-'.$child['id'].' {  background-color: '.$child['color'].' } ';
							}
		
							$out[] = array(
									'name'		=> '',
									'desc'		=> $child['title'],
									'values'	=>array( array(
															'from'			=>	'/Date('.datetomili($child['start_date']).')/',
															'to'			=>	'/Date('.datetomili($child['end_date']).')/',
															'label'			=>	$child['title'],
															'desc'			=>	$child['description'],
															'customClass'	=>	$class
														))
									);
						}
					}
				}
			}
		}

		return json_encode($out);
	}

	protected function getChildren($pid=0, $depth=1)
	{
		$sub = $this->activities;
			
		if(!isset($sub[$pid])) return;
	
		while (list($parent,$category) = each($sub[$pid]))
		{
			
			$this->acttree[] = array('info' => $category, 'depth' => $depth);
			
			$this->getChildren($category['id'],$depth+1);
		}
	}

	public function addAction()
	{
		$this->view->title = "New Activity";
		$this->view->ajax = 0;

		$this->view->type = $this->_getParam('type');
		$this->view->id = $this->_getParam('id');

		if( $this->getRequest()->isXmlHttpRequest() )
		{
			$this->view->ajax = 1;
			$this->_helper->layout->disableLayout();
		}

		$form = new Thesis_Form_Activity();
		
		//populate
		$activityList = $this->trackModel->getActivitiesForSelect($this->studentInfo['IdStudentRegistration']);
		foreach ( $activityList as $act_id => $act_name )
		{
			$form->pid->addMultiOption($act_id, $act_name);
		}

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			
			if ($form->isValid ( $formData )) 
			{
				
				//add 
				$data = array(
								'pid'				=> $formData['pid'],
								'title'				=> $formData['title'],
								'research_type'		=> $formData['research_type'],
								'research_id'		=> $formData['research_id'],
								'status'			=> $formData['status'],
								'description'		=> $formData['description'],
								'start_date'		=> $formData['start_date'],
								'end_date'			=> $formData['end_date'],
								'color'				=> $formData['color'],
								'created_by'		=> $this->studentInfo['IdStudentRegistration'],
								'created_date'		=> new Zend_Db_Expr('NOW()')
							);
				
				$activity_id = $this->trackModel->addActivity($data);
					
				//upload file
				try 
				{
					$uploadDir = $this->uploadDir.'/'.$this->studentInfo['IdStudentRegistration'];
	
					if ( !is_dir( $uploadDir ) )
					{
						if ( mkdir_p($uploadDir) === false )
						{
							throw new Exception('Cannot create student document folder ('.$uploadDir.')');
						}
					}
	
					$adapter = new Zend_File_Transfer_Adapter_Http();
				
					$files = $adapter->getFileInfo();
					$adapter->addValidator('NotExists', false, $uploadDir );
					$adapter->setDestination( $this->uploadDir );
					$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
					$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
	
					foreach ($files as $no => $fileinfo) 
					{
						$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
					
						if ($adapter->isUploaded($no))
						{
							$ext = getext($fileinfo['name']);
	
							$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
							$fileUrl = $uploadDir.'/'.$fileName;
	
							$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
							$adapter->setOptions(array('useByteString' => false));
							
							$size = $adapter->getFileSize($no);
						
							if( !$adapter->receive($no))
							{
								$errmessage = array();
								if ( is_array($adapter->getMessages() ))
								{	
									foreach(  $adapter->getMessages() as $errtype => $errmsg )
									{
										$errmessage[] = $errmsg;
									}
									
									throw new Exception(implode('<br />',$errmessage));
								}
							}
							
							//save file into db
							$data = array(
											'af_pid'			=> $activity_id,
											'af_type'			=> 'activity',
											'af_filename'		=> $fileinfo['name'],
											'af_fileurl'		=> '/thesis/'.$this->studentInfo['IdStudentRegistration'].'/'.$fileName,
											'af_filesize'		=> $size
										);
								
							$this->trackModel->addFile($data);
							
						} //isuploaded
						
					} //foreach
				}
				catch (Exception $e) 
				{
					throw new Exception( $e->getMessage() );
				}
				
				$this->_helper->flashMessenger->addMessage(array('success' => "New activity added"));
			 
				$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'tracking', 'action'=>'overview','id'=>$this->id,'type'=>$this->type),'default',true),array('prependBase'=>false));

			} // valid
			
		}

		//views
		$this->view->form = $form;
	}

	public function viewAction()
	{

		$id = $this->_getParam('id');

		$activity = $this->trackModel->getActivity($id);
		
		if ( empty($activity) )
		{
			throw new Exception('Invalid Activity ID');
		}

		if ( $activity['created_by'] != $this->studentInfo['IdStudentRegistration'] )
		{
			throw new Exception('This Activity doesn\'t belong to you');
		}

		$this->view->title = "View Activity";

		$form = new Thesis_Form_Activity();
		
		//populate
		$activityList = $this->trackModel->getActivitiesForSelect($this->studentInfo['IdStudentRegistration']);
		foreach ( $activityList as $act_id => $act_name )
		{
			$form->pid->addMultiOption($act_id, $act_name);
		}
		
		$form->populate($activity);

		//comments
		$this->view->comments = $this->trackModel->getComments($id,'tracking');
		$this->view->activity = $activity;
		
		//post
		if ( $this->getRequest()->isPost() && $this->_request->getPost ( 'save' ) )
		{
			$formData = $this->getRequest()->getPost();
			
			
			if ($form->isValid ( $formData )) 
			{
				//add 
				$data = array(
								//'pid'				=> $formData['pid'],
								//'title'				=> $formData['title'],
								'description'		=> $formData['description'],
								'start_date'		=> $formData['start_date'],
								'end_date'			=> $formData['end_date'],
								'color'				=> $formData['color'],
								//'status'			=> $formData['status'],
								'updated_by'		=> $this->studentInfo['IdStudentRegistration'],
								'updated_date'		=> new Zend_Db_Expr('NOW()'),
								'updated_type'		=> 'student'
							);
			
				$activity_id = $this->trackModel->updateActivity($data, array('id=?'=>$id));
				
				
				$this->_helper->flashMessenger->addMessage(array('success' => "Activity updated"));
			 
				$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'tracking', 'action'=>'view', 'id' => $id),'default',true),array('prependBase'=>false));

			} // valid
			
		}

		//comment
		if ( $this->getRequest()->isPost() && $this->_request->getPost ( 'addcomment' ) )
		{
			$formData = $this->getRequest()->getPost();
			
			$postedName = $this->studentInfo['appl_fname'].' '.$this->studentInfo['appl_lname'];

			//notify supervisor
			$supervisors = $this->regModel->getSupervisors($activity['research_id'], $activity['research_type']);
			$notify_data = array(
									'research_id'	=> $activity['research_id'],
									'research_type'	=> $activity['research_type'],
									'activity_id'	=> $activity['id'],
									'student_id'	=> $this->studentInfo['IdStudentRegistration'],
									'supervisors'	=> $supervisors,
									'date'			=> new Zend_Db_Expr('NOW()'),
									'url'			=> '/thesis/managetracking/view/id/'.$activity['id']
								);
			$this->notifyModel->notifySupervisors($notify_data);

			//add feedback
			$data = array(
							'c_pid'			=>	$id,
							'c_type'		=>  'tracking',
							'comment'		=>	$formData['comment'], 
							'posted_type'	=>	'student', 
							'posted_date'	=>  new Zend_Db_Expr('NOW()'), 
							'posted_by'		=>  $this->studentInfo['IdStudentRegistration'], 
							'posted_name'	=>	$postedName
						);

			$comment_id = $this->trackModel->addComments($data);

			//update parent

			$data2 = array(
							'totalcomments'		=> new Zend_Db_Expr('totalcomments+1'),
							'lastcomment_name'	=> $postedName,
							'lastcomment_date'	=> new Zend_Db_Expr('NOW()'),
							'lastcomment_id'	=> $this->studentInfo['IdStudentRegistration'],
							'lastcomment_type'	=> 'student'
					);

			$this->trackModel->updateActivity($data2, 'id='.(int)$id);
			

			//upload file
			try 
			{
				$uploadDir = $this->uploadDir.'/'.$this->studentInfo['IdStudentRegistration'];

				if ( !is_dir( $uploadDir ) )
				{
					if ( mkdir_p($uploadDir) === false )
					{
						throw new Exception('Cannot create student document folder ('.$uploadDir.')');
					}
				}

				$adapter = new Zend_File_Transfer_Adapter_Http();
			
				$files = $adapter->getFileInfo();
				$adapter->addValidator('NotExists', false, $uploadDir );
				$adapter->setDestination( $this->uploadDir );
				//$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
				$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));

				foreach ($files as $no => $fileinfo) 
				{
					$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
				
					if ($adapter->isUploaded($no))
					{
						$ext = getext($fileinfo['name']);

						$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
						$fileUrl = $uploadDir.'/'.$fileName;

						$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
						$adapter->setOptions(array('useByteString' => false));
						
						$size = $adapter->getFileSize($no);
					
						if( !$adapter->receive($no))
						{
							$errmessage = array();
							if ( is_array($adapter->getMessages() ))
							{	
								foreach(  $adapter->getMessages() as $errtype => $errmsg )
								{
									$errmessage[] = $errmsg;
								}
								
								throw new Exception(implode('<br />',$errmessage));
							}
						}
						
						//save file into db
						$data = array(
										'af_pid'			=> $comment_id,
										'af_type'			=> 'comments',
										'af_filename'		=> $fileinfo['name'],
										'af_fileurl'		=> '/thesis/'.$this->studentInfo['IdStudentRegistration'].'/'.$fileName,
										'af_filesize'		=> $size
									);
							
						$this->trackModel->addFile($data);
						
					} //isuploaded
					
				} //foreach
			}
			catch (Exception $e) 
			{
				throw new Exception( $e->getMessage() );
			}
			
			
			
			$this->_helper->flashMessenger->addMessage(array('success' => "New feedback posted"));
		 
			$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'tracking', 'action'=>'view', 'id' => $id),'default',true),array('prependBase'=>false));
		}
			

		//views
		$this->view->form = $form;
	}

	public function dateDifference($date1, $date2)
	{
	    $d1 = (is_string($date1) ? strtotime($date1) : $date1);
	    $d2 = (is_string($date2) ? strtotime($date2) : $date2);
	
	    $diff_secs = abs($d1 - $d2);
	    $base_year = min(date("Y", $d1), date("Y", $d2));
	
	    $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
	
	    return array
	    (
	        "years"                 => abs(substr(date('Ymd', $d1) - date('Ymd', $d2), 0, -4)),
	        "months_total"  => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,
	        "months"                => date("n", $diff) - 1,
	        "days_total"    => floor($diff_secs / (3600 * 24)),
	        "days"                  => date("j", $diff) - 1,
	        "hours_total"   => floor($diff_secs / 3600),
	        "hours"                 => date("G", $diff),
	        "minutes_total" => floor($diff_secs / 60),
	        "minutes"               => (int) date("i", $diff),
	        "seconds_total" => $diff_secs,
	        "seconds"               => (int) date("s", $diff)
	    );
	}
}