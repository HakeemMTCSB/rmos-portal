<?php

class Thesis_RegistrationController extends  Zend_Controller_Action
{
	public function init() {
		
		$session = new Zend_Session_Namespace('sis');		
		$this->session = $session;
		
		$this->defModel = new App_Model_General_DbTable_Definationms();
		$this->emailModel = new App_Model_Email();
		$this->thesisModel = new Thesis_Model_DbTable_General();
		$this->regModel = new Thesis_Model_DbTable_Registration();

		//locale setup
		$this->currLocale = Zend_Registry::get('Zend_Locale');
		$this->locales = $this->view->locales = array(
															'en_US'	=> 'English',
															'ms_MY'	=> 'Malay'
													  );

		
		$this->auth = Zend_Auth::getInstance();
		$this->uploadDir = DOCUMENT_PATH.'/thesis';

		$this->studentInfo = $this->auth->getIdentity()->info;
		
	}
	
	/*
		Registration - Proposal
	*/
	public function proposalAction()
	{
		//title
    	$this->view->title = "Proposal";

		$proposal = $this->regModel->getProposalByStudent($this->studentInfo['IdStudentRegistration']);
    	
    	$this->view->proposal = $proposal;
	}

	public function proposalAddAction()
	{
		$this->view->title = "New Proposal";
		
		$form = new Thesis_Form_Proposal();
		
		$this->view->studentInfo = $this->studentInfo;
		
		//options
		$thesisModel = new Thesis_Model_DbTable_General();
        $catList = $thesisModel->getCategoryByProgram($this->studentInfo['IdProgram']);
		$form->p_category->addMultiOption('', '-- Select --');
		foreach ($catList as $cat)
		{
			$form->p_category->addMultiOption($cat['rc_id'],  $cat['rc_name']);
		}

		$topicList = $thesisModel->getTopicsByProgram($this->studentInfo['IdProgram']);

		$form->p_topic->addMultiOption('', '-- Select --');
		foreach ( $topicList as $topic )
		{
			$form->p_topic->addMultiOption($topic['r_id'], $topic['r_topicname']);
		}


		//add Others
		$form->p_category->addMultiOption(99,'Others');
		$form->p_category->setAttrib('onchange','checkOthers(this)');
		$form->p_topic->addMultiOption(99,'Others');
		$form->p_topic->setAttrib('onchange','checkOthers(this)');



		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			
			if ($form->isValid ( $formData )) 
			{
				
				//check category
				if ( $formData['p_category'] == 99)
				{
					$data = array(
                            'rc_name'					=> $formData['p_category_others'],
                            'rc_program'				=> $this->studentInfo['IdProgram'],
                            'approved'					=> 0,
							'created_by'				=> 1,
                            'created_date'				=> new Zend_Db_Expr('NOW()')
                        );
			
					$category_id = $this->thesisModel->addCategories($data);
				}
				else
				{
					$category_id = $formData['p_category'];
				}

				//check topic
				if ( $formData['p_topic'] == 99)
				{
					$data = array(
                            'r_topicname'				=> $formData['p_topic_others'],
                            'r_category'				=> $category_id,
                            'r_supervisor'				=> '',
                            'r_maxcandidate'			=> 1,
							'r_assignmentrequirement'	=> '',
                            'r_createdby'				=> 1,
							'r_approved'				=> 0,
                            'r_createddate'				=> new Zend_Db_Expr('NOW()')
                        );
			
					$topic_id = $this->thesisModel->addTopic($data);
				}
				else
				{
					$topic_id = $formData['p_topic'];
				}

				//add proposal
				$data = array(
								'student_id'				=> $this->studentInfo['IdStudentRegistration'],
								'p_category'				=> $category_id,
								'p_topic'					=> $topic_id,
								'p_description'				=> $formData['p_description'],
								'p_status'					=> $this->thesisModel->getStatusByCode('APPLIED'),
								'created_by'				=> 1,
								'created_date'				=> new Zend_Db_Expr('NOW()')
							);
				
				$proposal_id = $this->regModel->addProposal($data);
				

				//upload file
				try 
				{
					$uploadDir = $this->uploadDir.'/'.$formData['student_id'];

					if ( !is_dir( $uploadDir ) )
					{
						if ( mkdir_p($uploadDir) === false )
						{
							throw new Exception('Cannot create student document folder ('.$uploadDir.')');
						}
					}

					$adapter = new Zend_File_Transfer_Adapter_Http();
					
				
					$files = $adapter->getFileInfo();
					$adapter->addValidator('NotExists', false, $uploadDir );
					$adapter->setDestination( $this->uploadDir );
					$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
					$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));

					foreach ($files as $no => $fileinfo) 
					{
						$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
					
						if ($adapter->isUploaded($no))
						{
							$ext = getext($fileinfo['name']);

							$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
							$fileUrl = $uploadDir.'/'.$fileName;

							$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
							$adapter->setOptions(array('useByteString' => false));
							
							$size = $adapter->getFileSize($no);
						
							if( !$adapter->receive($no))
							{
								$errmessage = array();
								if ( is_array($adapter->getMessages() ))
								{	
									foreach(  $adapter->getMessages() as $errtype => $errmsg )
									{
										$errmessage[] = $errmsg;
									}
									
									throw new Exception(implode('<br />',$errmessage));
								}
							}
							
							//save file into db
							$data = array(
											'af_pid'			=> $proposal_id,
											'af_type'			=> 'proposal',
											'af_filename'		=> $fileinfo['name'],
											'af_fileurl'		=> '/thesis/'.$formData['student_id'].'/'.$fileName,
											'af_filesize'		=> $size
										);
								
							$this->regModel->addFile($data);
							
						} //isuploaded
						
					} //foreach
				}
				catch (Exception $e) 
				{
					throw new Exception( $e->getMessage() );
				}

				
				$this->_helper->flashMessenger->addMessage(array('success' => "New research proposal succesfully added"));
			 
				$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'registration', 'action'=>'proposal'),'default',true));

			} // valid
			
			
		}

		//views
		$this->view->form = $form;
	}

	public function proposalEditAction()
	{
		$this->view->title = "Review Proposal";
		
		$form = new Thesis_Form_Proposal();
		
		$this->view->studentInfo = $this->studentInfo;
		
		//options
		$thesisModel = new Thesis_Model_DbTable_General();
        $catList = $thesisModel->getCategoryByProgram($this->studentInfo['IdProgram'], true);
		$form->p_category->addMultiOption('', '-- Select --');
		foreach ($catList as $cat)
		{
			$form->p_category->addMultiOption($cat['rc_id'],  $cat['rc_name']);
		}

		$topicList = $thesisModel->getTopicsByProgram($this->studentInfo['IdProgram'], true);
	

		$form->p_topic->addMultiOption('', '-- Select --');
		foreach ( $topicList as $topic )
		{
			$form->p_topic->addMultiOption($topic['r_id'], $topic['r_topicname']);
		}


		//add Others
		$form->p_category->setAttrib('disabled','disabled');
		$form->p_topic->setAttrib('disabled','disabled');
		
		$id = $this->_getParam('id');

		//get proposal info
		$proposal = $this->regModel->getProposal($id);
		
		if ( empty($proposal) )
		{
			throw new Exception('Invalid Proposal ID');
		}

		//ownership
		if ( $proposal['student_id'] != $this->studentInfo['IdStudentRegistration'] )
		{
			throw new Exception('This Proposal Does Not Belong To You');
		}

		//get files
		$uploadedfiles = $this->view->uploadedfiles = $this->regModel->getFiles($proposal['p_id'], 'proposal');
	
		//get supervisors
		$supervisors = $this->view->supervisors = $this->regModel->getSupervisors($proposal['p_id'], 'proposal');
		$supervisor_ids = array();
		foreach ( $supervisors as $sup )
		{
			$supervisor_ids[] = $sup['ps_id'];
		}

		$this->view->supervisor_ids = implode(',',$supervisor_ids);

	
	
		//repopulate form
		$form->populate($proposal);

		$this->view->proposal = $proposal;

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$form->p_category->setRequired(false);
			$form->p_topic->setRequired(false);
			
			if ($form->isValid ( $formData )) 
			{
				//add proposal
				$data = array(
								'p_description'				=> $formData['p_description'],
								'p_status'					=> $this->thesisModel->getStatusByCode('APPLIED'),
								'created_by'				=> 1,
								'created_date'				=> new Zend_Db_Expr('NOW()')
							);
				
				$proposal_id = $id;
				
				$this->regModel->updateProposal($data,'p_id='.(int)$id);
				
				//upload file
				try 
				{
					$uploadDir = $this->uploadDir.'/'.$formData['student_id'];
					
				
					if ( !is_dir( $uploadDir ) )
					{
						if ( mkdir_p($uploadDir) === false )
						{
							throw new Exception('Cannot create student document folder ('.$uploadDir.')');
						}
					}

					$adapter = new Zend_File_Transfer_Adapter_Http();
					
				
					$files = $adapter->getFileInfo();
					$adapter->addValidator('NotExists', false, $uploadDir );
					$adapter->setDestination( $this->uploadDir );
					$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
					$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));
			
					foreach ($files as $no => $fileinfo) 
					{
						$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
					
						if ($adapter->isUploaded($no))
						{
							
							$ext = getext($fileinfo['name']);

							$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
							$fileUrl = $uploadDir.'/'.$fileName;

							$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
							$adapter->setOptions(array('useByteString' => false));
							
							$size = $adapter->getFileSize($no);
						
							if( !$adapter->receive($no))
							{
								$errmessage = array();
								if ( is_array($adapter->getMessages() ))
								{	
									foreach(  $adapter->getMessages() as $errtype => $errmsg )
									{
										$errmessage[] = $errmsg;
									}
									
									throw new Exception(implode('<br />',$errmessage));
								}
							}
							
							//save file into db
							$data = array(
											'af_pid'			=> $proposal_id,
											'af_type'			=> 'proposal',
											'af_filename'		=> $fileinfo['name'],
											'af_fileurl'		=> '/thesis/'.$formData['student_id'].'/'.$fileName,
											'af_filesize'		=> $size
										);
						
							$this->regModel->addFile($data);
							
						} //isuploaded

						
					} //foreach


				}
				catch (Exception $e) 
				{
					throw new Exception( $e->getMessage() );
				}
				
				$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
			 
				$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'registration', 'action'=>'proposal-edit', 'id' => $id),'default',true));

			} // valid
			
			
		}

		//views
		$this->view->form = $form;
	}

	/*
		ARTICLESHIP
	*/
	public function articleshipAction()
	{
		//title
    	$this->view->title = "Articleship";

		$articleship = $this->regModel->getArticleshipByStudent($this->studentInfo['IdStudentRegistration']);

    	$this->view->articleship = $articleship;
	}

	public function articleshipAddAction()
	{
		$this->view->title = "New Articleship";
		
		$form = new Thesis_Form_Articleship();
		
		
		$this->view->studentInfo = $this->studentInfo;
		

		//
		$interest = $this->thesisModel->getInterest();
		$this->view->interests = $interest;
		
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid ( $formData )) 
			{
				$data = array(
								'student_id'				=> $this->studentInfo['IdStudentRegistration'],
								'semester_id'				=> $formData['semester_id'],
								'fieldofinterest'			=> json_encode($formData['interest']),
								'fieldofinterest_others'	=> $formData['fieldofinterest_others'],
								'company'					=> $formData['company'],
								'designation'				=> $formData['designation'],
								'contactperson'				=> $formData['contactperson'],
								'address'					=> $formData['address'],
								'city'						=> $formData['city'],
								'city_others'				=> $formData['city_others'],
								'state'						=> $formData['state'],
								'state_others'				=> $formData['state_others'],
								'country'					=> $formData['country'],
								'postcode'					=> $formData['postcode'],
								'phoneno'					=> $formData['phoneno'],
								'faxno'						=> $formData['faxno'],
								'email'						=> $formData['email'],
								'isemployee'				=> $formData['isemployee'],
								'yearsofservice'			=> $formData['yearsofservice'],
								'status'					=> $this->thesisModel->getStatusByCode('APPLIED'),
								'created_by'				=> 1,
								'created_date'				=> new Zend_Db_Expr('NOW()')
							);
				
				$articleship_id = $this->regModel->addArticleship($data);

				//upload file
				try 
				{
					$uploadDir = $this->uploadDir.'/'.$formData['student_id'];

					if ( !is_dir( $uploadDir ) )
					{
						if ( mkdir_p($uploadDir) === false )
						{
							throw new Exception('Cannot create student document folder ('.$uploadDir.')');
						}
					}

					$adapter = new Zend_File_Transfer_Adapter_Http();
					
				
					$files = $adapter->getFileInfo();
					$adapter->addValidator('NotExists', false, $uploadDir );
					$adapter->setDestination( $this->uploadDir );
					$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
					$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));

					foreach ($files as $no => $fileinfo) 
					{
						$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
					
						if ($adapter->isUploaded($no))
						{
							$ext = getext($fileinfo['name']);

							$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
							$fileUrl = $uploadDir.'/'.$fileName;

							$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
							$adapter->setOptions(array('useByteString' => false));
							
							$size = $adapter->getFileSize($no);
						
							if( !$adapter->receive($no))
							{
								$errmessage = array();
								if ( is_array($adapter->getMessages() ))
								{	
									foreach(  $adapter->getMessages() as $errtype => $errmsg )
									{
										$errmessage[] = $errmsg;
									}
									
									throw new Exception(implode('<br />',$errmessage));
								}
							}
							
							//save file into db
							$data = array(
											'af_pid'			=> $articleship_id,
											'af_type'			=> 'articleship',
											'af_filename'		=> $fileinfo['name'],
											'af_fileurl'		=> '/thesis/'.$formData['student_id'].'/'.$fileName,
											'af_filesize'		=> $size
										);
								
							$this->regModel->addFile($data);
							
						} //isuploaded
						
					} //foreach
				}
				catch (Exception $e) 
				{
					throw new Exception( $e->getMessage() );
				}

				$this->_helper->flashMessenger->addMessage(array('success' => "New articleship succesfully registered"));
			 
				$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'registration', 'action'=>'articleship'),'default',true));

			} // valid
			
			
		}

		//views
		$this->view->form = $form;
	}

	public function articleshipEditAction()
	{
		$this->view->title = "Edit Articleship";
		
		$form = new Thesis_Form_Articleship();

		$this->view->studentInfo = $this->studentInfo;
		
		//
		$interest = $this->thesisModel->getInterest();
		$this->view->interests = $interest;
		
		$id = $this->_getParam('id');

		//get articleship info
		$articleship = $this->regModel->getArticleship($id);
		
		if ( empty($articleship) )
		{
			throw new Exception('Invalid Articleship ID');
		}

		//ownership
		if ( $articleship['student_id'] != $this->studentInfo['IdStudentRegistration'] )
		{
			throw new Exception('This Articleship Does Not Belong To You');
		}

		//get files
		$uploadedfiles = $this->view->uploadedfiles = $this->regModel->getFiles($articleship['a_id'], 'articleship');



		//interest
		$_interest = array();
		$this->view->interest = json_decode($articleship['fieldofinterest'], true);
		
		
		//repopulate form
		$form->populate($articleship);

		$this->view->articleship = $articleship;
		
		//disable semester_id
		$form->semester_id->setAttrib('disabled','disabled');

		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			$form->semester_id->setRequired(false);

			if ($form->isValid ( $formData )) 
			{
				$data = array(
								//'semester_id'				=> $formData['semester_id'],
								'fieldofinterest'			=> json_encode($formData['interest']),
								'fieldofinterest_others'	=> $formData['fieldofinterest_others'],
								'company'					=> $formData['company'],
								'designation'				=> $formData['designation'],
								'contactperson'				=> $formData['contactperson'],
								'address'					=> $formData['address'],
								'city'						=> $formData['city'],
								'city_others'				=> $formData['city_others'],
								'state'						=> $formData['state'],
								'state_others'				=> $formData['state_others'],
								'country'					=> $formData['country'],
								'postcode'					=> $formData['postcode'],
								'phoneno'					=> $formData['phoneno'],
								'faxno'						=> $formData['faxno'],
								'email'						=> $formData['email'],
								'isemployee'				=> $formData['isemployee'],
								'yearsofservice'			=> $formData['yearsofservice'],
								//'status'					=> $this->thesisModel->getStatusByCode('APPLIED'),
								'updated_by'				=> 1,
								'updated_date'				=> new Zend_Db_Expr('NOW()')
							);
				
				$articleship_id = $id;

				
				$this->regModel->updateArticleship($data, 'a_id = '.(int) $id );

				//upload file
				try 
				{
					$uploadDir = $this->uploadDir.'/'.$formData['student_id'];

					if ( !is_dir( $uploadDir ) )
					{
						if ( mkdir_p($uploadDir) === false )
						{
							throw new Exception('Cannot create student document folder ('.$uploadDir.')');
						}
					}

					$adapter = new Zend_File_Transfer_Adapter_Http();
					
				
					$files = $adapter->getFileInfo();
					$adapter->addValidator('NotExists', false, $uploadDir );
					$adapter->setDestination( $this->uploadDir );
					$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
					$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));

					foreach ($files as $no => $fileinfo) 
					{
						$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
					
						if ($adapter->isUploaded($no))
						{
							$ext = getext($fileinfo['name']);

							$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
							$fileUrl = $uploadDir.'/'.$fileName;

							$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
							$adapter->setOptions(array('useByteString' => false));
							
							$size = $adapter->getFileSize($no);
						
							if( !$adapter->receive($no))
							{
								$errmessage = array();
								if ( is_array($adapter->getMessages() ))
								{	
									foreach(  $adapter->getMessages() as $errtype => $errmsg )
									{
										$errmessage[] = $errmsg;
									}
									
									throw new Exception(implode('<br />',$errmessage));
								}
							}
							
							//save file into db
							$data = array(
											'af_pid'			=> $articleship_id,
											'af_type'			=> 'articleship',
											'af_filename'		=> $fileinfo['name'],
											'af_fileurl'		=> '/thesis/'.$formData['student_id'].'/'.$fileName,
											'af_filesize'		=> $size
										);
								
							$this->regModel->addFile($data);
							
						} //isuploaded
						
					} //foreach
				}
				catch (Exception $e) 
				{
					throw new Exception( $e->getMessage() );
				}

				
				
				$this->_helper->flashMessenger->addMessage(array('success' => "Information updated"));
			 
				$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'registration', 'action'=>'articleship-edit','id'=>$id),'default',true));

			} // valid
			
			
		}

		//views
		$this->view->form = $form;
	}

	/*
		EXEMPTION
	*/
	public function exemptionAction()
	{
		//title
    	$this->view->title = "Articleship Exemption";

		$exemption = $this->regModel->getExemptionByStudent($this->studentInfo['IdStudentRegistration']);
    	
    	$this->view->exemption = $exemption;
	}

	public function exemptionAddAction()
	{
		$this->view->title = "Articleship Exemption Registration";
		
		$form = new Thesis_Form_Exemption();
		
		$this->view->studentInfo = $this->studentInfo;
	
		//post
		if ( $this->getRequest()->isPost() )
		{
			$formData = $this->getRequest()->getPost();
			
			if ($form->isValid ( $formData )) 
			{
				$data = array(
								'student_id'				=> $this->studentInfo['IdStudentRegistration'],
								'reason'					=> $formData['reason'],
								'proposedarea'				=> $formData['proposedarea'],
								'status'					=> $this->thesisModel->getStatusByCode('APPLIED'),
								'created_by'				=> 1,
								'created_date'				=> new Zend_Db_Expr('NOW()')
							);
				
				$exemption_id = $this->regModel->addExemption($data);

				//upload file
				try 
				{
					$uploadDir = $this->uploadDir.'/'.$formData['student_id'];

					if ( !is_dir( $uploadDir ) )
					{
						if ( mkdir_p($uploadDir) === false )
						{
							throw new Exception('Cannot create student document folder ('.$uploadDir.')');
						}
					}

					$adapter = new Zend_File_Transfer_Adapter_Http();
					
				
					$files = $adapter->getFileInfo();
					$adapter->addValidator('NotExists', false, $uploadDir );
					$adapter->setDestination( $this->uploadDir );
					$adapter->addValidator('Count', false, array('min' => 1 , 'max' => 100));
					$adapter->addValidator('Size', false, array('min' => 400 , 'max' => 4194304 , 'bytestring' => true));

					foreach ($files as $no => $fileinfo) 
					{
						$adapter->addValidator('Extension', false, array('extension' => 'jpg,jpeg,png,gif,doc,pdf,docx', 'case' => false));
					
						if ($adapter->isUploaded($no))
						{
							$ext = getext($fileinfo['name']);

							$fileName = time().'-'.md5($fileinfo['name']).'.'.$ext;
							$fileUrl = $uploadDir.'/'.$fileName;

							$adapter->addFilter('Rename', array('target' => $fileUrl,'overwrite' => true));			
							$adapter->setOptions(array('useByteString' => false));
							
							$size = $adapter->getFileSize($no);
						
							if( !$adapter->receive($no))
							{
								$errmessage = array();
								if ( is_array($adapter->getMessages() ))
								{	
									foreach(  $adapter->getMessages() as $errtype => $errmsg )
									{
										$errmessage[] = $errmsg;
									}
									
									throw new Exception(implode('<br />',$errmessage));
								}
							}
							
							//save file into db
							$data = array(
											'af_pid'			=> $exemption_id,
											'af_type'			=> 'exemption',
											'af_filename'		=> $fileinfo['name'],
											'af_fileurl'		=> '/thesis/'.$formData['student_id'].'/'.$fileName,
											'af_filesize'		=> $size
										);
								
							$this->regModel->addFile($data);
							
						} //isuploaded
						
					} //foreach
				}
				catch (Exception $e) 
				{
					throw new Exception( $e->getMessage() );
				}

				//add employment detail
				for($i=0;$i<count($formData['employment_compname']);)
				{
					//save file into db
					$data = array(
									'ae_pid'			=> $exemption_id,
									'ae_compname'		=> $formData['employment_compname'][$i],
									'ae_compaddress'	=> $formData['employment_compaddress'][$i],
									'ae_position'		=> $formData['employment_position'][$i],
									'ae_jobfunction'	=> $formData['employment_jobfunction'][$i],
									'ae_year'			=> $formData['employment_year'][$i],
									'ae_refname'		=> $formData['employment_refname'][$i],
									'ae_refaddress'		=> $formData['employment_refaddress'][$i],
									'ae_addedby'		=> 1,
									'ae_addeddate'		=> new Zend_Db_Expr('NOW()')
								);
						
					$this->regModel->addEmployment($data);

					$i++;
				}

			
				
				$this->_helper->flashMessenger->addMessage(array('success' => "New articleship exemption succesfully added"));
			 
				$this->_redirect($this->view->url(array('module'=>'thesis','controller'=>'registration', 'action'=>'exemption'),'default',true));

			} // valid
			
			
		}

		//views
		$this->view->form = $form;
	}
}
