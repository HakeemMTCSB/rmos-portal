<?php
class Thesis_Form_Proposal extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		 //code
        $student_id = new Zend_Form_Element_Hidden('student_id');
		$student_id->setAttrib('class', 'reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//p_category
		
	

		//category
		$p_category = new Zend_Form_Element_Select('p_category');
		$p_category->setAttrib('class', 'select reqfield')
					->setRequired(true)
					->removeDecorator("DtDdWrapper")
				  ->removeDecorator("Label")
				  ->removeDecorator('HtmlTag');
		

		//p_topic
		$p_topic = new Zend_Form_Element_Select('p_topic');
		$p_topic->setAttrib('class', 'select reqfield')
					->setRequired(true)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');
      

		//p_description
		$p_description = new Zend_Form_Element_Textarea('p_description');
		$p_description->setAttrib('class', 'textarea reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//file
		$uploadfile = new Zend_Form_Element_File('uploadfile');
		$uploadfile->setRequired(false)
		->removeDecorator("DtDdWrapper")
		->removeDecorator("Label")
		->removeDecorator('HtmlTag');

		//meeting date
        $meeting_date = new Zend_Form_Element_Text('meeting_date');
		$meeting_date->setAttrib('class', 'inputtext half datepicker')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");

		$p_title = new Zend_Form_Element_Text('p_title');
		$p_title->setAttrib('class', 'inputtext reqfield')
					//->setAttrib('style','width:500px')
					->setRequired(true)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label")
					->removeDecorator('HtmlTag');
		
		
		//form elements
        $this->addElements(array(
            $student_id,
			$uploadfile,
			$p_category,
			$p_description,
			$p_topic,
			$p_title,
			$meeting_date
			
		));
		
	}
}
?>