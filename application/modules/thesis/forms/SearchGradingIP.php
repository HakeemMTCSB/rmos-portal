<?php
class Thesis_Form_SearchGradingIP extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate = Zend_Registry::get('Zend_Translate');

		//semester_id
		$semester_id = new Zend_Form_Element_Select('semester_id');
		$semester_id->setAttrib('class', 'select reqfield')
					->setRequired(false)
					->setLabel('Semester')
					->setRegisterInArrayValidator(false);
		
		$semester_id->addMultiOption('','-- Select --');

		$semesterDB = new App_Model_General_DbTable_Semestermaster();
		
		foreach($semesterDB->fnGetSemesterList(array(5,6)) as $semester){
			$semester_id->addMultiOption($semester["key"],$semester["value"]);
		}

		//course
		$course_id = new Zend_Form_Element_Select('course_id');
		$course_id->setAttrib('class', 'select reqfield')
					->setRequired(false)
					->setLabel('Course')
					->setRegisterInArrayValidator(false);
		
		$course_id->addMultiOption('','-- Select --');
		
		$thesisDB = new Thesis_Model_DbTable_General();
		$coursesList = $thesisDB->getAllCourses();
		foreach ($coursesList as $c)
		{
			$course_id->addMultiOption($c['IdSubject'], $c['SubCode'].' - '.$c['SubjectName']);
		}

        //student id
        $student_id = new Zend_Form_Element_Text('student_id');
        $student_id->setAttrib('class', 'inputtext reqfield')
            ->setRequired(false)
            ->setLabel('Student ID');

        //student name
        $student_name = new Zend_Form_Element_Text('student_name');
        $student_name->setAttrib('class', 'inputtext reqfield')
            ->setRequired(false)
            ->setLabel('Student Name');

		//form elements
        $this->addElements(array(
            
			$semester_id,
			$course_id	,
            $student_id,
            $student_name
		));

		$this->addElement('submit', 'save', array(
			'label'=>'Search',
			'class' => 'btn-submit'
        ));
		
	}
}
?>