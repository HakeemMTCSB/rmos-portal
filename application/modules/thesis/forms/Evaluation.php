<?php
class Thesis_Form_Evaluation extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate = Zend_Registry::get('Zend_Translate');
		

		//set_id
		$set_id = new Zend_Form_Element_Select('set_id');
		$set_id->setAttrib('class', 'select reqfield')
					->setRequired(true)
					->setRegisterInArrayValidator(false)
					->removeDecorator("DtDdWrapper")
				  ->removeDecorator("Label")
				  ->removeDecorator('HtmlTag');
		
		$evalModel = new Thesis_Model_DbTable_Evaluation();
		//$setList = $evalModel->getEvaluationSetData();
		$set_id->addMultiOption('','-- Select --');
		/*foreach ( $setList as $set)
		{
			$set_id->addMultiOption($set['id'], $set['name']);
		}*/

		//form elements
        $this->addElements(array(
            
			$set_id
	
			
		));
		
	}
}
?>