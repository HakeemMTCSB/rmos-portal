<?php

class Thesis_Form_AddActivity extends Zend_Form
{

    public function init()
    {
        $pid = $this->_attribs['p_id'];
        $course_id = $this->_attribs['course_id'];
        $student_id = $this->_attribs['student_id'];

        $activityDB = new Thesis_Model_DbTable_General();

        $activity = new Zend_Form_Element_Radio('activity_id');
        $activity
            ->setAttrib('class', 'chk')
            ->setAttrib('onChange','getActivityId(this)')
            ->setRequired(true)
            ->removeDecorator("Label")
        ;

        $activityList = $activityDB->selectType('registed',$course_id,$student_id);

            foreach ($activityList as $a) {
                $activity->addMultiOption($a['idDefinition'], $a['DefinitionDesc']);
            }

        //file
        $uploadfile = new Zend_Form_Element_File('uploadfile');
        $uploadfile->setRequired(false)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array(

            $activity,
            $uploadfile


        ));

    }
}