<?php

class Thesis_Form_ReportProgress extends Zend_Form
{

    public function init()
    {

        $gstrtranslate = Zend_Registry::get('Zend_Translate');

        $semesterDB = new App_Model_General_DbTable_Semestermaster();

        $semester_id = new Zend_Form_Element_Select('semester_id');
        $semester_id->setAttrib('class', 'select reqfield')
            ->setRequired(true)
            ->setRegisterInArrayValidator(false)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        $semester_id->addMultiOption(null, "-- Please Select --");
        foreach ($semesterDB->fnGetSemesterList(6) as $semester) {
            $semester_id->addMultiOption($semester["key"], $semester["SemesterMainCode"] . ' - ' . $semester["value2"]);
        }


        //itemdiscussed
        $nextaction = new Zend_Form_Element_Textarea('nextaction');
        $nextaction->setAttrib('class', 'textarea large')
//            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //itemdiscussed
        $itemdiscussed = new Zend_Form_Element_Textarea('itemdiscussed');
        $itemdiscussed->setAttrib('class', 'textarea large')
            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');


        //meeting_date
        $meeting_date = new Zend_Form_Element_Text('meeting_date');
        $meeting_date->setAttrib('class', 'inputtext datepicker half reqfield')
            ->setAttrib('onChange','getSemester(this)')
            ->setRequired(true)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //next_date
        $next_date = new Zend_Form_Element_Text('next_date');
        $next_date->setAttrib('class', 'inputtext datepicker half')
            ->setRequired(false)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //time_from
        $time_from = new Zend_Form_Element_Select('time_from');
        $time_from->setAttrib('class', 'select reqfield small')
            ->setRequired(true)
            ->setRegisterInArrayValidator(false)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //time_to
        $time_to = new Zend_Form_Element_Select('time_to');
        $time_to->setAttrib('class', 'select reqfield small')
            ->setRequired(true)
            ->setRegisterInArrayValidator(false)
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label")
            ->removeDecorator('HtmlTag');

        //form elements
        $this->addElements([

            $semester_id,
            $meeting_date,
            $next_date,
            $itemdiscussed,
            $nextaction,
            $time_from,
            $time_to


        ]);

    }
}

?>