<?php
class Thesis_Form_Activity extends Zend_Form
{
	
	public function init()
	{
	
		$gstrtranslate = Zend_Registry::get('Zend_Translate');
		

		//parent_id
		$pid = new Zend_Form_Element_Select('pid');
		$pid->setAttrib('class', 'select reqfield')
					->setRequired(false)
					->removeDecorator("DtDdWrapper")
				  ->removeDecorator("Label")
				  ->removeDecorator('HtmlTag');
			
		$pid->addMultiOption(0, 'None');		
		
		//title
		$title = new Zend_Form_Element_Text('title');
		$title->setAttrib('class', 'inputtext reqfield')
				->setRequired(false)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//description
		$description = new Zend_Form_Element_Textarea('description');
		$description->setAttrib('class', 'textarea')
				->setRequired(false)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//color
		$color = new Zend_Form_Element_Text('color');
		$color->setAttrib('class', 'inputtext quarter')
				->setRequired(false)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//start_date
		$start_date = new Zend_Form_Element_Text('start_date');
		$start_date->setAttrib('class', 'inputtext datepicker  reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//end_date
		$end_date = new Zend_Form_Element_Text('end_date');
		$end_date->setAttrib('class', 'inputtext datepicker  reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');
		
		$defDB = new App_Model_Definitiontype();
		
		//status 
		$status = new Zend_Form_Element_Select('status');
		$status->setAttrib('class', 'select reqfield')
					->setRequired(false)
					->removeDecorator("DtDdWrapper")
				  ->removeDecorator("Label")
				  ->removeDecorator('HtmlTag');
			
		$statuslist = $defDB->fnGetDefinationMs('Research Activity Status');
		foreach ( $statuslist as $str )
		{
			$status->addMultiOption($str['key'], $str['value']);
			if ( $str['value'] == 'In Progress' )
			{
				$status->setValue($str['key']);
			}
		}

		//form elements
        $this->addElements(array(
            
			$pid,
			$title,
			$description,
			$start_date,
			$end_date,
			$color,
			$status
	
			
		));
		
	}
}
?>