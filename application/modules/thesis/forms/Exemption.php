<?php
class Thesis_Form_Exemption extends Zend_Form
{
	
	protected $_semesterid;
	protected $_subjectid;
	
	public function setSemesterid($value) {
		$this->_semesterid = $value;
	}
	
	public function setSubjectid($value) {
		$this->_subjectid = $value;
	}
	
	
	public function init()
	{
	
		$gstrtranslate = Zend_Registry::get('Zend_Translate');

		 //code
        $student_id = new Zend_Form_Element_Hidden('student_id');
		$student_id->setAttrib('class', 'reqfield')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//semesterid
		$semester_id = new Zend_Form_Element_Hidden('semester_id');
		$semester_id->setAttrib('class', 'hidden')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag')
				 ->setValue($this->_semesterid);
				 
		//subjectid
		$subject_id = new Zend_Form_Element_Hidden('subject_id');
		$subject_id->setAttrib('class', 'hidden')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag')
				 ->setValue($this->_subjectid);
		
		
		//reason
		$reason = new Zend_Form_Element_Textarea('reason');
		$reason->setAttrib('class', 'textarea reqfield large')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');

		//proposedarea
		$proposedarea = new Zend_Form_Element_Textarea('proposedarea');
		$proposedarea->setAttrib('class', 'textarea reqfield large')
				->setRequired(true)
				->removeDecorator("DtDdWrapper")
				->removeDecorator("Label")
				->removeDecorator('HtmlTag');
			
		//start date
        $start_date = new Zend_Form_Element_Text('start_date');
		$start_date->setAttrib('class', 'inputtext half datepicker')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
		
		 //end date
        $end_date = new Zend_Form_Element_Text('end_date');
		$end_date->setAttrib('class', 'inputtext half datepicker')
            ->removeDecorator("DtDdWrapper")
            ->removeDecorator("Label");
		
		
		//form elements
        $this->addElements(array(
            $student_id,
			$reason,
			$proposedarea,
			$subject_id,
			$semester_id,
			$start_date,
			$end_date
			
		));
		
	}
}
?>