<?php

class Thesis_Model_DbTable_General extends Zend_Db_Table
{

    protected $_topic = 'thesis_researchtopics';
    protected $_categories = 'thesis_categories';
    protected $_status = 'thesis_status';
    protected $_supervisor = 'thesis_supervisor';
    protected $_examinersem = 'thesis_examinersem';
    protected $_interest = 'thesis_fieldofinterest';
    protected $_reason = 'thesis_reasonapplying';
    protected $_downloads = 'thesis_download';
    protected $_file = 'thesis_article_files';

    protected $_submit = 'thesis_submission';

    protected $_colloquium = 'thesis_colloquium';
    protected $_colloquium_app = 'thesis_colloquium_application';

    protected $_proposal = 'thesis_proposal';
    protected $_article = 'thesis_articleship';
    protected $_exemption = 'thesis_exemption';


    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->locale = Zend_Registry::get('Zend_Locale');
    }


    /*
    *	Research Categories
    */
    public function getCategoriesData()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_categories))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('p' => 'tbl_program'), 'a.rc_program=p.IdProgram', array('p.ProgramName'));


        return $select;
    }

    public function updateProposal($data, $where)
    {
        $db = $this->db;

        $db->update($this->_proposal, $data, $where);
    }

    public function updateCategories($data, $where)
    {
        $db = $this->db;

        $db->update($this->_categories, $data, $where);
    }

    public function addCategories($data)
    {
        $db = $this->db;

        $db->insert($this->_categories, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getCategoryByProgram($programid, $all = false)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_categories))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('p' => 'tbl_program'), 'a.rc_program=p.IdProgram', array('p.ProgramName'));

        if ($all == false) {
            $select->where('a.approved = 1');
        }

        $select->where('a.rc_program = ?', $programid);


        $result = $db->fetchAll($select);
        return $result;
    }

    public function getCategory($id = null, $all = false)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_categories))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('p' => 'tbl_program'), 'a.rc_program=p.IdProgram', array('p.ProgramName'));

        if ($all == false) {
            $select->where('a.approved = 1');
        }

        if ($id) {
            $select->where('a.rc_id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    /*
    *	Research Topic
    */
    public function getTopicData()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_topic))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.r_createdby', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('cat' => $this->_categories), 'a.r_category=cat.rc_id', array('cat.rc_name as cat_name'))
            ->joinLeft(array('p' => 'tbl_program'), 'cat.rc_program=p.IdProgram', array('p.ProgramName'));


        return $select;
    }

    public function getTopicsByProgram($programid, $all = false)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_topic))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.r_createdby', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('cat' => $this->_categories), 'a.r_category=cat.rc_id', array('cat.rc_name as cat_name'))
            ->joinLeft(array('p' => 'tbl_program'), 'cat.rc_program=p.IdProgram', array('p.ProgramName'));

        if ($all == false) {
            $select->where('a.r_approved = 1');
        }
        $select->where('cat.rc_program = ?', $programid);


        $result = $db->fetchAll($select);
        return $result;
    }

    public function getTopics($all = false)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_topic))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.r_createdby', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('cat' => $this->_categories), 'a.r_category=cat.rc_id', array('cat.rc_name as cat_name'))
            ->joinLeft(array('p' => 'tbl_program'), 'cat.rc_program=p.IdProgram', array('p.ProgramName'));

        if ($all == false) {
            $select->where('a.r_approved = 1');
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getTopicSingle($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_topic))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.r_createdby', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('cat' => $this->_categories), 'a.r_category=cat.rc_id', array('cat.rc_name as cat_name'))
            ->joinLeft(array('p' => 'tbl_program'), 'cat.rc_program=p.IdProgram', array('p.ProgramName'))
            ->where('a.r_id=?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function updateTopic($data, $where)
    {
        $db = $this->db;

        $db->update($this->_topic, $data, $where);
    }

    public function addTopic($data)
    {
        $db = $this->db;

        $db->insert($this->_topic, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    /*
    *	Research Status
    */
    public function getStatusData()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_status))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('p' => 'tbl_program'), 'a.status_program=p.IdProgram', array('p.ProgramName'));


        return $select;
    }

    public function updateStatus($data, $where)
    {
        $db = $this->db;

        $db->update($this->_status, $data, $where);
    }

    public function addStatus($data)
    {
        $db = $this->db;

        $db->insert($this->_status, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getStatusByCode($name = '', $type = 0)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('a' => $this->_status), 'status_id')
            ->where('a.status_code = ?', $name)
            ->where('a.status_type = ?', $type);

        $result = $db->fetchRow($select);

        return $result['status_id'];

    }

    public function getStatus($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_status))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('p' => 'tbl_program'), 'a.status_program=p.IdProgram', array('p.ProgramName as status_program_name'))
            ->joinLeft(array('c' => 'tbl_subjectmaster'), 'a.status_course=c.IdSubject', array('SubjectName as status_course_name'));

        if ($id) {
            $select->where('a.status_id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }


    /*
    *	Supervisors
    */

    public function getSupervisorPaginate()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_supervisor));
        //->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.supervisor_userid', array())
        //->joinLeft(array('staff'=>'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff','FullName as supervisor_name');


        return $select;
    }

    public function updateSupervisor($data, $where)
    {
        $db = $this->db;

        $db->update($this->_supervisor, $data, $where);
    }

    public function addSupervisor($data)
    {
        $db = $this->db;

        $db->insert($this->_supervisor, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getSupervisor($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_supervisor));
        //->joinLeft(array('u'=>'tbl_user'), 'u.iduser = a.supervisor_userid', array())
        //->joinLeft(array('staff'=>'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff','FullName as supervisor_name');

        if ($id) {
            $select->where('a.supervisor_id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    /*
    *	Examiner
    */
    public function getExaminerSem($sem_id = '')
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('a' => $this->_examinersem));

        if ($sem_id) {
            $select->where('a.es_semesterid=?', $sem_id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    public function updateExaminerSem($data, $where)
    {
        $db = $this->db;

        $db->update($this->_examinersem, $data, $where);
    }

    public function addExaminerSem($data)
    {
        $db = $this->db;

        $db->insert($this->_examinersem, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }


    /* Other Functions */
    public function getCourses()
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("sm" => "tbl_subjectmaster"), array("sm.*"))
            ->join(array("ct" => "tbl_coursetype"), 'ct.IdCourseType =sm.CourseType', array("ct.CourseType"))
            ->where('ct.IdCourseType IN ( ? )', array(2, 3, 19));

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAllCourses()
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("sm" => "tbl_subjectmaster"), array("sm.*"))
            ->join(array("ct" => "tbl_coursetype"), 'ct.IdCourseType =sm.CourseType', array("ct.CourseType"))
            ->order('sm.SubCode asc');

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAvailableSupervisors()
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

        /*$select_not = $lobjDbAdpt->select()
                    ->distinct()
                    ->from(array('a'=>$this->_supervisor),'a.supervisor_userid');*/

        $select = $lobjDbAdpt->select()
            ->from(array('user' => 'tbl_user'))
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff')
            ->joinLeft(array('college' => 'tbl_collegemaster'), 'college.IdCollege  = staff.IdCollege')
            ->joinLeft(array('role' => 'tbl_definationms'), 'role.idDefinition  = user.IdRole')
            ->where('user.UserStatus = 1')
            //->where('user.iduser NOT IN (?)', $select_not)
            ->order('user.loginName ASC');


        $result = $lobjDbAdpt->fetchAll($select);
        return $result;
    }

    /*
        field of interest
    */


    public function getInterestData()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_interest))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name');


        return $select;
    }

    public function updateInterest($data, $where)
    {
        $db = $this->db;

        $db->update($this->_interest, $data, $where);
    }

    public function addInterest($data)
    {
        $db = $this->db;

        $db->insert($this->_interest, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }


    public function getInterest($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_interest))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name');

        if ($id) {
            $select->where('a.i_id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    public function getResearchType($student_id, $return = 0)
    {
        $regModel = new Thesis_Model_DbTable_Registration();

        $proposal = $regModel->getProposalByStudent($student_id);
        $articleship = $regModel->getArticleshipByStudent($student_id);
        $exemption = $regModel->getExemptionByStudent($student_id);

        if ($return) {
            if (!empty($proposal)) return array('type' => 'proposal', 'info' => $proposal);
            if (!empty($articleship)) return array('type' => 'articleship', 'info' => $articleship);
            if (!empty($exemption)) return array('type' => 'exemption', 'info' => $exemption);

            return '';
        } else {

            if (!empty($proposal)) return 'proposal';
            if (!empty($articleship)) return 'articleship';
            if (!empty($exemption)) return 'exemption';

            return '';
        }
    }




    public function getDownloadSingle($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_downloads))
            ->where('a.id=?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public static function researchType($type)
    {
        $return = '';

        switch ($type) {
            case 'articleship':
                $return = 'Articleship';
                break;
            case 'exemption':
                $return = 'PPP';
                break;
            case 'proposal':
                $return = 'Proposal';
                break;
        }

        return $return;
    }

    public static function researchId($type)
    {
        $return = '';

        switch ($type) {
            case 'articleship':
                $return = 'a_id';
                break;
            case 'exemption':
                $return = 'e_id';
                break;
            case 'proposal':
                $return = 'p_id';
                break;
        }

        return $return;
    }

    /* submission */

    public function getSubmission($student_id, $type)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_submit))
            ->where('a.student_id=?', $student_id)
            ->where('a.submit_type=?', $type)
            ->where('a.approved IN (?)', array(0, 1))
            ->order('a.id DESC');

        $result = $db->fetchRow($select);
        return $result;
    }


    public function updateSubmission($data, $where)
    {
        $db = $this->db;

        $db->update($this->_submit, $data, $where);
    }

    public function addSubmission($data)
    {
        $db = $this->db;

        $db->insert($this->_submit, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    /*
        Colloquium setup
    */


    public function getColloquiumSetupData($student_id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_colloquium))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name')
            ->joinLeft(array('b' => $this->_colloquium_app), "b.colloquium_id=a.id AND b.student_id='" . $student_id . "'", array('b.status as app_status'))
            ->group('a.id')
            ->order('a.id DESC');

        return $db->fetchAll($select);
    }

    public function getColloquiumSetupSingle($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_colloquium))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name')
            ->where('a.id = ?', $id);

        return $db->fetchRow($select);
    }

    public function getColloquiumApplication($id, $student_id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_colloquium_app))
            ->where('a.colloquium_id = ?', $id)
            ->where('a.student_id = ?', $student_id);

        return $db->fetchRow($select);
    }

    public function addColloquiumApplication($data)
    {

        $db = $this->db;

        $db->insert($this->_colloquium_app, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function updateColloquiumApplication($data, $where)
    {
        $db = $this->db;

        $db->update($this->_colloquium_app, $data, $where);
    }

    public function getReason($id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_reason))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name');

        if ($id) {
            $select->where('a.i_id=?', $id);
            $result = $db->fetchRow($select);
        } else {
            $result = $db->fetchAll($select);
        }

        return $result;
    }

    public function getColloquiumSupervisee($userid, $count = 0)
    {
        $db = $this->db;

        $proposal = $db->select()
            ->distinct()
            ->from(array('a' => $this->_proposal), array('a.student_id'))
            ->join(array('sup' => 'thesis_article_supervisor'), 'sup.ps_pid=a.p_id', array())
            ->where('sup.ps_type=\'proposal\' AND sup.ps_supervisor_id = ?', $userid);

        $articleship = $db->select()
            ->distinct()
            ->from(array('a' => $this->_article), array('a.student_id'))
            ->join(array('sup' => 'thesis_article_supervisor'), 'sup.ps_pid=a.a_id', array())
            ->where('sup.ps_type=\'articleship\' AND sup.ps_supervisor_id = ?', $userid);

        $exemption = $db->select()
            ->distinct()
            ->from(array('a' => $this->_exemption), array('a.student_id'))
            ->join(array('sup' => 'thesis_article_supervisor'), 'sup.ps_pid=a.e_id', array())
            ->where('sup.ps_type=\'exemption\' AND sup.ps_supervisor_id = ?', $userid);

        $select = $db->select()
            ->from(array('a' => $this->_colloquium_app))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId as student_id'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('b' => $this->_colloquium), 'a.colloquium_id=b.id', array('b.name', 'b.description', 'b.date_from', 'b.date_to', 'a.status as app_status'));

        $select->where(
            $db->quoteInto('a.student_id IN ?', $proposal) . ' OR ' . $db->quoteInto('a.student_id IN ?', $articleship) . ' OR ' . $db->quoteInto('a.student_id IN ?', $exemption)
        );


        if ($count == 1) {
            $select->where('a.status = ?', 'APPLIED');

            $getTotal = $db->query($select)->rowCount();

            return $getTotal;
        } else {
            return $db->fetchAll($select);
        }
    }

    public function getSubmissionSupervisee($userid, $type, $count = 0)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal), array('*'))
//            ->join(array('sub' => $this->_submit), 'sub.research_id = a.pid', array('*'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId as student_id'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=a.semester_start', array('SemesterMainName', 'SemesterMainCode'))
            ->joinLeft(array('subj' => 'tbl_subjectmaster'), 'a.course_id = subj.IdSubject', array('subj.SubjectName as course_name', 'SubCode'))
            ->join(array('sup' => 'thesis_article_supervisor'), 'sup.ps_pid=a.p_id', array())
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_code', 'st.status_description'))
            ->joinLeft(array('stp' => 'thesis_status'), 'stp.status_id=a.pd_status', array('stp.status_code as pd_status_name', 'stp.status_code as pd_status_code', 'stp.status_description as pd_status_description'))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition=a.activity_id', array('DefinitionDesc'))
            ->where('sup.ps_type=\'proposal\' AND sup.ps_supervisor_id = ?', $userid)
            //->where('sub.research_type  IN (?)',array(1066))
            ->where('stp.status_code=?', 'applied')
            ->where('st.status_code=?', 'registered')
            ->where('sup.ps_supervisor_role=?', 1)
            ->where('a.pid != ?', 0)
            ->group('a.p_id');

        /*$articleship = $db->select()
                    ->distinct()
                    ->from(array('a'=>$this->_article),array('a.student_id'))
                    ->join(array('sup'=>'thesis_article_supervisor'),'sup.ps_pid=a.a_id',array())
                    ->where('sup.ps_type=\'articleship\' AND sup.ps_supervisor_id = ?',$userid);

        $exemption = $db->select()
                    ->distinct()
                    ->from(array('a'=>$this->_exemption),array('a.student_id'))
                    ->join(array('sup'=>'thesis_article_supervisor'),'sup.ps_pid=a.e_id',array())
                    ->where('sup.ps_type=\'exemption\' AND sup.ps_supervisor_id = ?',$userid);*/

        //query
        /*$select = $db->select()
            ->from(array('a' => $this->_submit))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId as student_id'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
//            ->where('a.submit_type=?', $type)
            ->order('a.id DESC');

        $select->where(
//			$db->quoteInto('a.student_id IN ?', $proposal) . ' OR ' . $db->quoteInto('a.student_id IN ?', $articleship) . ' OR '. $db->quoteInto('a.student_id IN ?', $exemption)
            $db->quoteInto('a.student_id IN ?', $proposal)
        );*/


        if ($count == 1) {
//            $select->where('sub.approved = 0');
            $getTotal = $db->query($select)->rowCount();

            return $getTotal;
        } else {
            return $db->fetchAll($select);
        }
    }


    public function subjectRegistered($idstudreg)
    {
        $db = $this->db;

        $sql = $db->select()
            ->from(array("a" => 'tbl_studentregsubjects'), array('a.IdSubject'))
            ->join(array("s" => 'tbl_subjectmaster'), 's.IdSubject=a.IdSubject', array())
            ->where('s.CourseType IN (?)', array(2, 3, 19))
            ->where("a.IdStudentRegistration = ?", $idstudreg);


        return $sql;
    }

    public function getSubmissionByResearchId($id, $type)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_submit))
            ->where('a.research_id=?', $id)
            ->where('a.research_type=?', $type);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function addProposal($data)
    {
        $db = $this->db;

        $db->insert($this->_proposal, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function selectType($ex = null,$course = null,$student = null)
    {
        $db = $this->db;
        $select2 = "select activity_id from thesis_proposal where student_id = $student and course_id = $course";
        $select = $db->select()
            ->from(array('a' => 'thesis_research_activity_tagged'), array('a.*'))
            ->join(array('b' => 'tbl_definationms'), 'b.idDefinition=a.rat_type', array('idDefinition','DefinitionDesc'))
            ->where("a.rat_type  NOT in  ($select2)")
            ->where('a.rat_course = ?', $course)
            ->order('b.defOrder ASC')
        ;

        if(isset($ex)){
            $select->where('b.DefinitionCode != ?', 'registration');
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getPid($pid= null)
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal), array('a.*'))
            ->where('a.p_id = ?', $pid)
            ;

//        echo $select;exit;
            $result = $db->fetchRow($select);
            return $result;

    }

    public function getPidMain($stud, $course)
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal), array('p_id'))
            ->where('a.student_id = ?', $stud)
            ->where('a.course_id = ?', $course)
            ->where('a.pid = ?', 0)
        ;

        $result = $db->fetchRow($select);
        return $result;

    }

    public function getDef($def)
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'tbl_definationms'), array('a.*'))
            ->where('a.idDefinition = ?', $def)
        ;

        $result = $db->fetchRow($select);
        return $result;

    }

    public function getFiles($pid, $type)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => 'thesis_article_files'))
            ->where('a.af_pid = ?', $pid)
            ->where('a.af_type = ?', $type);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function addFile($data)
    {
        $db = $this->db;

        $db->insert('thesis_article_files', $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function researchIcemData($studid=null,$semcode=null)
    {
        $multidb = Zend_Registry::get("multidb");
        $this->_db = $multidb->getDb('oracle');

        $select = "select COURSEREGIS_GRADE from course_registration
                    where COURSEREGIS_STUD_ID = '$studid' 
                    and COURSEREGIS_SEMESTER_ID = '$semcode'";

        $result = $this->_db->fetchRow($select);

        return $result;

    }
}