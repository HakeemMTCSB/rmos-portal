<?php

class Thesis_Model_DbTable_Tracking extends Zend_Db_Table
{

    protected $_topic = 'thesis_researchtopics';
    protected $_categories = 'thesis_categories';
    protected $_status = 'thesis_status';
    protected $_supervisor = 'thesis_supervisor';
    protected $_examinersem = 'thesis_examinersem';
    protected $_interest = 'thesis_fieldofinterest';

    protected $_activity = 'thesis_activity';
    protected $_comments = 'thesis_activity_comments';
    protected $_files = 'thesis_activity_files';
    protected $_progreport = 'thesis_progressreport';

    protected $_activity_setup = 'thesis_activity_setup';

    protected $_superrole = 'thesis_article_supervisor';

    protected $subs;

    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->locale = Zend_Registry::get('Zend_Locale');
    }

    /* reports */
    public function getStudentLatestSemester($idStudReg)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'tbl_studentsemesterstatus'), array('value' => '*'))
            ->joinLeft(array('b' => 'tbl_definationms'), 'a.studentsemesterstatus=b.IdDefinition', array('status' => 'DefinitionDesc'))
            ->joinLeft(array('c' => 'tbl_semestermaster'), 'a.IdSemesterMain=c.IdSemesterMaster')
            ->where('a.IdStudentRegistration = ?', $idStudReg)
            ->order('c.SemesterMainStartDate DESC');

        return $db->fetchRow($select);
    }

    public function getReportSemesters($id, $type)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_progreport))
            ->joinLeft(array('sm' => 'tbl_semestermaster'), 'sm.IdSemesterMaster=a.semester_id', array('SemesterMainName', 'SemesterMainCode'))
            ->where('a.research_id = ?', $id)
            ->where('a.research_type = ?', $type)
            ->where('a.semester_id IS NOT NULL');

        return $db->fetchAll($select);
    }

    public function getReports($id, $type, $semester_id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_progreport))
            ->joinLeft(array('c' => 'tbl_semestermaster'), 'a.semester_id=c.IdSemesterMaster')
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('tp' => 'thesis_proposal'), 'tp.p_id = a.research_id', array('p_title', 'p_description'))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = tp.activity_id', array('DefinitionDesc'))
//            ->where('a.research_id = ?', $id)
            ->where('tp.p_id = ?', $id)
            ->orWhere('tp.pid = ?', $id)
            ->where('a.research_type = ?', $type);

        if (!empty($semester_id)) {
            $select->where('a.semester_id = ?', $semester_id);
        }

        return $db->fetchAll($select);
    }

    public function getReportsSupervisor($id, $type, $studid = null, $courseid = null, $semester_id = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_progreport))
            ->joinLeft(array('c' => 'tbl_semestermaster'), 'a.semester_id=c.IdSemesterMaster')
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('tp' => 'thesis_proposal'), 'tp.p_id = a.research_id', array('p_title', 'p_description'))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = tp.activity_id', array('DefinitionDesc'))
            ->where('a.student_id = ?', $studid)
            ->where('tp.course_id = ?', $courseid)
//            ->where('tp.p_id = ?', $id)
//            ->orWhere('tp.pid = ?', $id)
            ->where('a.research_type = ?', $type);

        if (!empty($semester_id)) {
            $select->where('a.semester_id = ?', $semester_id);
        }

        return $db->fetchAll($select);
    }

    public function getReport($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_progreport))
            ->join(array('tp' => 'thesis_proposal'), 'tp.p_id = a.research_id', array('p_title','p_id','student_id','course_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->joinLeft(array('sm' => 'tbl_semestermaster'), 'sm.IdSemesterMaster=a.semester_id', array('SemesterMainName', 'SemesterMainCode'))
            ->where('a.id = ?', $id);

        return $db->fetchRow($select);
    }

    public function addReport($data)
    {
        $db = $this->db;

        $db->insert($this->_progreport, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function updateReport($data, $where)
    {
        $db = $this->db;

        $db->update($this->_progreport, $data, $where);
    }

    /*
    *	Activities
    */
    public function getActivities()
    {

    }

    public function getActivitySetupData($type = '')
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_activity_setup))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name')
            ->order('a.id ASC');

        if ($type != '') {
            $select->where('a.research_type = ?', $type);
        }

        return $db->fetchAll($select);
    }

    public function getActivitiesByStudent($studentid)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_activity))
            ->joinLeft(array('deftn' => 'tbl_definationms'), 'deftn.idDefinition=a.Status', array('deftn.DefinitionDesc as status_name'))
            ->where('a.created_by = ?', $studentid);


        return $db->fetchAll($select);
    }

    public function getActivitiesByResearch($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_activity))
            ->joinLeft(array('deftn' => 'tbl_definationms'), 'deftn.idDefinition=a.Status', array('deftn.DefinitionDesc as status_name'))
            ->where('a.research_id = ?', $id);


        return $db->fetchAll($select);
    }

    public static function getActivity($id)
    {
        $db = getDb();

        $select = $db->select()
            ->from(array('a' => 'thesis_activity'))
            ->where('a.id = ?', $id);

        return $db->fetchRow($select);
    }

    public function updateActivity($data, $where)
    {
        $db = $this->db;

        $db->update($this->_activity, $data, $where);
    }

    public function addActivity($data)
    {
        $db = $this->db;

        $db->insert($this->_activity, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getComments($act_id, $type = '', $pid = null, $type2 = null )
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_comments))
            ->joinLeft(array("b" => $this->_superrole), 'b.ps_supervisor_id = a.posted_by', array('ps_id'))
            ->joinLeft(array("c" => $this->_supervisor), 'c.supervisor_id = b.ps_supervisor_role', array('c.code as role_name'))
            ->where('a.c_pid = ?', $act_id)
            ->group('a.c_id')
        ;

        if ($pid) {
            $select->where('b.ps_pid = ?', $pid);
        }

        if ($type2) {
            $select->where('b.ps_type = ?', $type2);
        }

        if ($type != '') {
            $select->where('a.c_type = ?', $type);
        }

//        echo $select;exit;

        return $db->fetchAll($select);
    }

    public function getCommentsStudent($act_id, $type = '', $type2 = '' )
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_comments))
            ->joinLeft(array("b" => $this->_superrole), 'b.ps_supervisor_id = a.posted_by', array('ps_id'))
            ->joinLeft(array("c" => $this->_supervisor), 'c.supervisor_id = b.ps_supervisor_role', array('c.code as role_name'))
            ->where('a.c_pid = ?', $act_id)
            ->group('a.c_id')
        ;

        if ($type != '') {
            $select->where('a.c_type = ?', $type);
        }

        if ($type2 != '') {
            $select->where('a.tag_student = ?', $type2);
        }

        return $db->fetchAll($select);
    }

    public function updateComments($data, $where)
    {
        $db = $this->db;

        $db->update($this->_comments, $data, $where);
    }

    public function addComments($data)
    {
        $db = $this->db;

        $db->insert($this->_comments, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function addFile($data)
    {
        $db = $this->db;

        $db->insert($this->_files, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }


    public static function getFiles($pid, $type)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a" => 'thesis_activity_files'))
            ->where('a.af_pid = ?', $pid)
            ->where('a.af_type = ?', $type);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getActivitiesForSelect($studentid)
    {
        if (!is_array($this->subs)) {
            $this->subs = $this->_getsublist(array('created_by' => $studentid));
        }

        $this->sublist = array();

        $this->getSublistChild(0);

        return $this->sublist;
    }

    public function getSublistChild($pid = 0, $depth = 1)
    {
        $sub = $this->subs;

        if (!isset($sub[$pid])) return;


        while (list($parent, $category) = each($sub[$pid])) {
            $name = str_repeat("-", $depth) . " " . $category['title'];

            $this->sublist[$category['id']] = $name;

            //maxchild syndrome
            //$this->getSublistChild($category['id'],$depth+1);

        }
    }

    protected function _getsublist($where = array())
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $db->select()
            ->from(array("a" => $this->_activity), array("a.*"));

        if (!empty($where)) {
            foreach ($where as $cond => $val) {
                $lstrSelect->where('a.' . $cond . ' = ?', $val);
            }
        }

        $results = $db->fetchAll($lstrSelect);

        $categories = array();
        foreach ($results as $row) {
            $categories[$row['pid']][] = $row;
        }

        return $categories;
    }
}