<?php
class Thesis_Model_DbTable_Documents extends Zend_Db_Table {
	
	protected $_topic = 'thesis_researchtopics';
	protected $_categories = 'thesis_categories';
	protected $_status = 'thesis_status';
	protected $_supervisor = 'thesis_supervisor';
	protected $_examinersem = 'thesis_examinersem';
	protected $_interest = 'thesis_fieldofinterest';
	protected $_examiner_setup = 'thesis_examiner_setup';

	protected $_activity = 'thesis_activity';
	protected $_comments = 'thesis_activity_comments';
	protected $_files = 'thesis_activity_files';
	protected $_progreport = 'thesis_progressreport';

	protected $_evalset = 'thesis_evaluation_set';
	protected $_evalitem = 'thesis_evaluation_item';
	protected $_evalgroups = 'thesis_evaluation_groups';
	protected $_evalsheet = 'thesis_evaluation_sheet';
	
	protected $_downloads = 'thesis_download';
	protected $_file = 'thesis_article_files';
	
	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
		$this->locale = Zend_Registry::get('Zend_Locale');
	}
	
	public function getFiles($type)
	{
		$db = $this->db;
		
		$select = $db->select()
	                ->from(array('a'=>$this->_downloads))
					->where('a.research_type=?',$type)
					->order('a.id DESC');
		
		$result = $db->fetchAll($select);	
		return $result;
	}

}