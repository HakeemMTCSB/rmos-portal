<?php

class Thesis_Model_DbTable_Evaluation extends Zend_Db_Table
{

    protected $_topic = 'thesis_researchtopics';
    protected $_categories = 'thesis_categories';
    protected $_status = 'thesis_status';
    protected $_supervisor = 'thesis_supervisor';
    protected $_examinersem = 'thesis_examinersem';
    protected $_interest = 'thesis_fieldofinterest';
    protected $_examiner_setup = 'thesis_examiner_setup';

    protected $_activity = 'thesis_activity';
    protected $_comments = 'thesis_activity_comments';
    protected $_files = 'thesis_activity_files';
    protected $_progreport = 'thesis_progressreport';

    protected $_evalset = 'thesis_evaluation_set';
    protected $_evalitem = 'thesis_evaluation_item';
    protected $_evalgroups = 'thesis_evaluation_groups';
    protected $_evalsheet = 'thesis_evaluation_sheet';

    protected $_proposal = 'thesis_proposal';


    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->locale = Zend_Registry::get('Zend_Locale');
    }

    /* reports */

    public function getEvaluation($examiner_id, $id, $type)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_evalsheet))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('set' => $this->_evalset), 'set.id=a.set_id', array('set.name as set_name'))
            ->where('a.research_id = ?', $id)
            ->where('a.research_type = ?', $type)
            ->where('a.created_by = ?', $examiner_id);


        return $db->fetchAll($select);
    }

    public function getEvaluationById($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_evalsheet))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('set' => $this->_evalset), 'set.id=a.set_id', array('set.name as set_name', 'set.*'))
            ->where('a.id = ?', $id);


        return $db->fetchRow($select);
    }

    public function getEvaluationItemsBySet($set_id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_evalitem))
            ->where('a.set_id=?', $set_id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getEvaluationGroupsBySet($set_id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_evalgroups))
            ->where('a.set_id=?', $set_id);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function addEvaluation($data)
    {
        $db = $this->db;

        $db->insert($this->_evalsheet, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function updateEvaluation($data, $where)
    {
        $db = $this->db;

        $db->update($this->_evalsheet, $data, $where);
    }

    public function getEvaluationSetData($subject_sql = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_evalset))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name');

        if (!empty($subject_sql)) {
            $select->where('a.subject_id IN (?)', $subject_sql);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getEvaluationSetById($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_evalset))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->where('a.id = ?', $id);


        $result = $db->fetchRow($select);

        return $result;
    }

    public function getEvaluationSheet($set_id = 0, $student_id = 0)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_evalsheet))
            ->join(array('s' => $this->_evalset), 's.id=a.set_id', array('s.name as set_name'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('user' => 'thesis_examiner_user'), 'user.id=a.examiner_id', array('user.fullname as supervisor_name', 'user.id as examiner_id'))
            ->joinLeft(array('u2' => 'tbl_user'), 'user.staff_id = u2.iduser and user.staff_id > 0', array())
            ->joinLeft(array('staff2' => 'tbl_staffmaster'), 'staff2.IdStaff  = u2.IdStaff', 'FullName as internal_fullname')
            ->order('a.id DESC');

        if ($set_id) {
            $select->where('a.set_id = ?', $set_id);
        }

        if ($student_id) {
            $select->where('a.student_id = ?', $student_id);
        }

        return $db->fetchAll($select);
    }

    public static function recommendationText($val)
    {
        $return = '';

        switch ($val) {
            case 1:
                $return = 'Pass';
                break;
            case 2:
                $return = 'Pass (with minor corrections)';
                break;
            case 3:
                $return = 'Pass (with major corrections)';
                break;
            case 4:
                $return = 'Re-examination';
                break;
            case 5:
                $return = 'Fail';
                break;
        }

        return $return;
    }

    /*
    *	GradinIp
    */
    public function getGradingIP($where = array())
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('sps' => 'thesis_article_supervisor'),array())
            ->join(array('a' => $this->_proposal), "sps.ps_pid = a.p_id",  array('a.*', 'a.p_id as research_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = a.course_id', array('SubCode', 'SubjectName'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=a.semester_start', array('SemesterMainName','SemesterMainCode'))
            //->joinLeft(array('sub' => 'tbl_studentregsubjects'), 'sub.IdStudentRegistration=a.student_id AND sub.IdSubject=\'' . $where['course_id'] . '\' AND sub.IdSemesterMain = \'' . $where['semester_id'] . '\'', array('sub.IdStudentRegSubjects', 'sub.grade_name'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
//            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_code', 'st.status_description'))
//            ->joinLeft(array('stp' => 'thesis_status'), 'stp.status_id=a.pd_status', array('stp.status_code as pd_status_name', 'stp.status_code as pd_status_code', 'stp.status_description as pd_status_description'))
//            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
//            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
//            ->joinLeft(array('u2' => 'tbl_user'), 'u2.iduser = a.approved_by', array())
//            ->joinLeft(array('staff2' => 'tbl_staffmaster'), 'staff2.IdStaff  = u2.IdStaff', 'FullName as approved_by_name')
//            ->joinLeft(array('u3' => 'tbl_user'), 'u3.iduser = a.rejected_by', array())
//            ->joinLeft(array('staff3' => 'tbl_staffmaster'), 'staff3.IdStaff  = u3.IdStaff', 'FullName as rejected_by_name')
            ->join(array('td' => 'tbl_definationms'), 'td.idDefinition = a.activity_id', array('DefinitionDesc'))
            ->where('td.DefinitionDesc != ?', 'registration')
            ->where('sps.ps_type = ?', 'proposal')
            //->group('sps.ps_id')
            ->order('a.updated_date DESC')
            ->order('a.created_date DESC');

        if (!empty($where)) {

            if (isset($where['supervisor_id']) && $where['supervisor_id'] != '') {
//                    ->joinLeft(array('usps' => 'tbl_user'), 'usps.iduser = sps.ps_supervisor_id', array())
//                    ->joinLeft(array('staffsps' => 'tbl_staffmaster'), 'staffsps.IdStaff  = usps.IdStaff', array());
                $select->where('sps.ps_supervisor_id = ?', $where['supervisor_id']);
            }

            if (isset($where['student_name']) && $where['student_name'] != '') {
                $select->where('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) LIKE ?', '%' . $where['student_name'] . '%');
            }

            if (isset($where['student_id']) && $where['student_id'] != '') {
                $select->where('registrationId LIKE ?', '%' . $where['student_id'] . '%');
            }

            if (isset($where['p_status']) && $where['p_status'] != '') {
                $select->where('a.pd_status = ?', $where['p_status']);
            }

            if (isset($where['semester_id']) && $where['semester_id'] != '') {
                $select->where('a.semester_start = ?', $where['semester_id']);
            }

            if (isset($where['course_id']) && $where['course_id'] != '') {
                $select->where('a.course_id = ?', $where['course_id']);
            }
        }

        return $select;
    }
}