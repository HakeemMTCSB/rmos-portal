<?php

class Thesis_Model_DbTable_Registration extends Zend_Db_Table
{

    protected $_proposal = 'thesis_proposal';
    protected $_file = 'thesis_article_files';
    protected $_supervisor = 'thesis_article_supervisor';
    protected $_article = 'thesis_articleship';
    protected $_exemption = 'thesis_exemption';
    protected $_employment = 'thesis_article_employment';
    protected $_submission = 'thesis_submission';
    protected $_submission_history = 'thesis_submission_history';

    protected $_topic = 'thesis_researchtopics';
    protected $_categories = 'thesis_categories';
    protected $_status = 'thesis_status';
    protected $_superrole = 'thesis_supervisor';
    protected $_examinersem = 'thesis_examinersem';

    protected $_evaluation_file = 'thesis_evaluation_files';

    protected $_supervisor_history = 'thesis_supervisor_history';

    protected $_notification = 'thesis_student_notification';

    public function init()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->locale = Zend_Registry::get('Zend_Locale');
        $this->auth = Zend_Auth::getInstance();
    }

    /*
    *	getProposal
    */
    public function getProposalByStudent($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email'))
            ->joinLeft(array('subj' => 'tbl_subjectmaster'), 'a.course_id = subj.IdSubject', array('subj.SubjectName as course_name', 'SubCode'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name');

        $select->where('a.student_id = ?', $id);

        $select->order('a.updated_date DESC');

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getProposalListByStudent($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=a.semester_start', array('SemesterMainName','SemesterMainDefaultLanguage','SemesterMainCode'))
            ->joinLeft(array('subj' => 'tbl_subjectmaster'), 'a.course_id = subj.IdSubject', array('subj.SubjectName as course_name', 'SubCode'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_code', 'st.status_description'))
            ->joinLeft(array('stp' => 'thesis_status'), 'stp.status_id=a.pd_status', array('stp.status_code as pd_status_name', 'stp.status_code as pd_status_code', 'stp.status_description as pd_status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('td' => 'tbl_definationms'), 'a.activity_id  = td.idDefinition', 'DefinitionDesc')
            ->joinLeft(array('tes' => 'thesis_event_student'), 'tes.research_id  = a.p_id', array())
            ->joinLeft(array('te' => 'thesis_event_setup'), 'te.event_id  = tes.event_id AND te.event_type = a.activity_id', array('te.*'))
            ->joinLeft(array('dir' => 'thesis_status'), 'dir.status_id=a.pd_approval', array('dir.status_code as dir_status_name', 'dir.status_code as dir_status_code', 'dir.status_description as dir_status_description'))
            ;

        $select->where('a.student_id = ?', $id);

        $select->order('a.p_id DESC');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getEvent($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'thesis_event_setup'),array('a.*'))
            ->joinLeft(array('te' => 'thesis_event_student'), 'te.event_id  = a.event_id', array('te.*'))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition  = a.event_type', 'DefinitionDesc')
            ->where('a.event_id = ?', $id)
        ;

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getProposalSupervisee($id, $formdata = null,  $where = null, $pid = 0)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal), array('a.*', 'a.p_id as research_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = a.course_id', array('SubCode', 'SubjectName'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=a.semester_start', array('SemesterMainName','SemesterMainCode'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_code', 'st.status_description'))
            ->joinLeft(array('stp' => 'thesis_status'), 'stp.status_id=a.pd_status', array('stp.status_code as pd_status_name', 'stp.status_code as pd_status_code', 'stp.status_description as pd_status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->join(array('sup' => 'thesis_article_supervisor'), 'sup.ps_pid=a.p_id', array())
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = a.activity_id', array('DefinitionDesc','DefinitionDesc as StatusName','DefinitionCode'))
//            ->group(array('a.student_id','a.course_id','a.p_title'))
            ->order('a.created_date desc')
        ;

        if($pid){
            $select->where('a.pid = ?', 0);
        }

        if (isset($formdata['SubCode']) && $formdata['SubCode'] != '') {
            $select->where('s.SubCode  like "%" ? "%"', $formdata['SubCode']);
        }

        if (isset($formdata['student-search']) && $formdata['student-search'] != '') {
            if ($formdata['search-type'] == 'id') {
                $select->where('sr.registrationId = ?', $formdata['student-search']);
            }
            elseif($formdata['search-type'] == 'name'){
                $select->where('p.appl_fname like "%" ? "%"', $formdata['student-search']);
            }
        }

        if (isset($where['p_status']) && $where['p_status'] != '') {
            $select->where('a.p_status = ?', $where['p_status']);
        }

        $select->where('sup.ps_type=\'proposal\' AND sup.ps_supervisor_id = ?', $id);

//        echo $select;exit;

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getProposalSuperviseeList($id, $formdata = null,  $where = null, $pid = 0)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal), array('a.*', 'a.p_id as research_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = a.course_id', array('SubCode', 'SubjectName'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=a.semester_start', array('SemesterMainName','SemesterMainCode'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_code', 'st.status_description'))
            ->joinLeft(array('stp' => 'thesis_status'), 'stp.status_id=a.pd_status', array('stp.status_code as pd_status_name', 'stp.status_code as pd_status_code', 'stp.status_description as pd_status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->join(array('sup' => 'thesis_article_supervisor'), 'sup.ps_pid=a.p_id', array())
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = a.activity_id', array('DefinitionDesc','DefinitionDesc as StatusName','DefinitionCode'))
            ->group(array('a.student_id','a.course_id'))
            ->order('a.created_date desc')
        ;

        if($pid){
            $select->where('a.pid = ?', 0);
        }

        if (isset($formdata['SubCode']) && $formdata['SubCode'] != '') {
            $select->where('s.SubCode  like "%" ? "%"', $formdata['SubCode']);
        }

        if (isset($formdata['student-search']) && $formdata['student-search'] != '') {
            if ($formdata['search-type'] == 'id') {
                $select->where('sr.registrationId = ?', $formdata['student-search']);
            }
            elseif($formdata['search-type'] == 'name'){
                $select->where('p.appl_fname like "%" ? "%"', $formdata['student-search']);
            }
        }

        if (isset($where['p_status']) && $where['p_status'] != '') {
            $select->where('a.p_status = ?', $where['p_status']);
        }

        $select->where('sup.ps_type=\'proposal\' AND sup.ps_supervisor_id = ?', $id);

//        echo $select;exit;

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getProposalSuperviseeHome($id, $formdata = null,  $where = null, $pid = 1)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal), array('a.*', 'a.p_id as research_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = a.course_id', array('SubCode', 'SubjectName'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=a.semester_start', array('SemesterMainName','SemesterMainCode'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_code', 'st.status_description'))
            ->joinLeft(array('stp' => 'thesis_status'), 'stp.status_id=a.pd_status', array('stp.status_code as pd_status_name', 'stp.status_code as pd_status_code', 'stp.status_description as pd_status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->join(array('sup' => 'thesis_article_supervisor'), 'sup.ps_pid=a.p_id', array())
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = a.activity_id', array('DefinitionDesc','DefinitionDesc as StatusName','DefinitionCode'))
//            ->group(array('a.student_id','a.course_id','a.p_title'))
            ->order('a.created_date desc')
        ;

        if($pid){
            $select->where('a.pid != ?', 0);
        }

        if (isset($formdata['SubCode']) && $formdata['SubCode'] != '') {
            $select->where('s.SubCode  like "%" ? "%"', $formdata['SubCode']);
        }

        if (isset($formdata['student-search']) && $formdata['student-search'] != '') {
            if ($formdata['search-type'] == 'id') {
                $select->where('sr.registrationId = ?', $formdata['student-search']);
            }
            elseif($formdata['search-type'] == 'name'){
                $select->where('p.appl_fname like "%" ? "%"', $formdata['student-search']);
            }
        }

        if (isset($where['p_status']) && $where['p_status'] != '') {
            $select->where('a.p_status = ?', $where['p_status']);
        }

        $select->where('sup.ps_type=\'proposal\' AND sup.ps_supervisor_id = ?', $id);

//        echo $select;exit;

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getProposalSuperviseeRep($id, $formdata = null,  $where = null, $pid = 0)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal), array('a.*', 'a.p_id as research_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = a.course_id', array('SubCode', 'SubjectName'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=a.semester_start', array('SemesterMainName','SemesterMainCode'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_code', 'st.status_description'))
            ->joinLeft(array('stp' => 'thesis_status'), 'stp.status_id=a.pd_status', array('stp.status_code as pd_status_name', 'stp.status_code as pd_status_code', 'stp.status_description as pd_status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->join(array('sup' => 'thesis_article_supervisor'), 'sup.ps_pid=a.p_id', array())
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = a.activity_id', array('DefinitionDesc','DefinitionDesc as StatusName'))
            ->group(array('a.student_id','a.course_id'))
            ->order('a.created_date desc')
        ;

        if(!$pid){
            $select->where('a.pid = ?', 0);
        }

        if (isset($formdata['SubCode']) && $formdata['SubCode'] != '') {
            $select->where('s.SubCode  like "%" ? "%"', $formdata['SubCode']);
        }

        if (isset($formdata['student-search']) && $formdata['student-search'] != '') {
            if ($formdata['search-type'] == 'id') {
                $select->where('sr.registrationId = ?', $formdata['student-search']);
            }
            elseif($formdata['search-type'] == 'name'){
                $select->where('p.appl_fname like "%" ? "%"', $formdata['student-search']);
            }
        }

        if (isset($where['p_status']) && $where['p_status'] != '') {
            $select->where('a.p_status = ?', $where['p_status']);
        }

        $select->where('sup.ps_type=\'proposal\' AND sup.ps_supervisor_id = ?', $id);

//        echo $select;exit;

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getProposalExaminee($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_code'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->join(array('ex' => 'thesis_article_examiner'), 'ex.ex_pid=a.p_id', array());

        $select->where('ex.ex_type=\'proposal\' AND ex.ex_examiner_id = ?', $id);


        $result = $db->fetchAll($select);

        return $result;
    }

    public function getProposalData($where = array())
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name');

        if (!empty($where)) {
            if (isset($where['p_status']) && $where['p_status'] != '') {
                $select->where('a.p_status = ?', $where['p_status']);
            }
        }

        return $select;
    }

    public function getProposal($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_proposal))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->joinLeft(array('td' => 'tbl_definationms'), 'a.activity_id  = td.idDefinition',array('DefinitionDesc as ActivityName','DefinitionDesc','DefinitionCode'))
            ->join(array('s' => 'tbl_subjectmaster'), 's.IdSubject = a.course_id', array('SubCode', 'SubjectName'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name','appl_email as student_email','appl_phone_mobile as mobile_no','appl_phone_home as home_no'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=a.semester_start', array('SemesterMainName','SemesterMainCode'))
            ->joinLeft(array('prg' => 'tbl_program'), 'prg.IdProgram=sr.IdProgram', array('ProgramName', 'ProgramCode','IdScheme'))
            ->joinLeft(array('intk' => 'tbl_intake'), 'intk.IdIntake=sr.IdIntake', array('intk.IntakeDesc as intake_name'))
            ->joinLeft(array('c' => 'thesis_categories'), 'c.rc_id=a.p_category', array('c.rc_name as category_name'))
            ->joinLeft(array('r' => 'thesis_researchtopics'), 'r.r_id=a.p_topic', array('r.r_topicname as topic_name'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.p_status', array('st.status_code as status_name', 'st.status_code', 'st.status_description'))
            ->joinLeft(array('stp' => 'thesis_status'), 'stp.status_id=a.pd_status', array('stp.status_code as pd_status_name', 'stp.status_code as pd_status_code', 'stp.status_description as pd_status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as supervisor_name')
            ->joinLeft(array('teu' => 'thesis_examiner_user'), 'teu.id=a.examiner_viva', array('fullname','institution'))
            ->joinLeft(array('rv' => 'registry_values'), 'rv.id=a.correction_viva', array('correction' => 'name'))
            ->joinLeft(array('rv2' => 'registry_values'), 'rv2.id=a.grade_viva', array('viva_grade' => 'name'))
            ->joinLeft(array('es' => 'thesis_event_student'), 'es.research_id = a.p_id', array('start_time','end_time'))
            ->joinLeft(array('e' => 'thesis_event_setup'), 'e.event_id = es.event_id and e.event_display = 1', array('event_id','event_name','event_date','event_venue','event_remark','event_display'))
            ->where('a.p_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function updateProposal($data, $where)
    {
        $db = $this->db;

        $db->update($this->_proposal, $data, $where);
    }

    public function addProposal($data)
    {
        $db = $this->db;

        $db->insert($this->_proposal, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    /*
    *	Files
    */
    public function addFile($data)
    {
        $db = $this->db;

        $db->insert($this->_file, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getFile($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_file))
            ->where('a.af_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getFiles($pid, $type)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_file))
            ->where('a.af_pid = ?', $pid)
            ->where('a.af_type = ?', $type);

        $result = $db->fetchAll($select);
        return $result;
    }

//    public function getEvaluationFiles($pid,$vid)
//    {
//        $db = $this->db;
//        $select = $db->select()
//            ->from(array("a" => $this->_evaluation_file))
//            ->joinLeft(array('v' => 'thesis_evaluation_visibility'), 'v.v_ef_id = a.ef_id', array())
//            ->where('a.ef_pid = ?', $pid)
//            ->where('v.v_visible_id = ?', $vid)
//        ;
////        echo $select;exit;
//
//        $result = $db->fetchAll($select);
//        return $result;
//    }

    public function getEvaluationFiles($pid,$type)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_evaluation_file))
            ->where('a.ef_pid = ?', $pid)
            ->where('a.type = ?', $type)
        ;

        $result = $db->fetchAll($select);
        return $result;
    }

    /* Supervisor */
    public function addSupervisor($data)
    {
        $db = $this->db;

        $db->insert($this->_supervisor, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    /* Supervisor History */
    public function addSupervisorHistory($data)
    {
        $db = $this->db;

        $db->insert($this->_supervisor_history, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getSupervisors($pid, $type)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_supervisor))
            ->join(array("b" => $this->_superrole), 'a.ps_supervisor_role=b.supervisor_id', array('b.code as role_name'))
            ->join(array('user' => 'tbl_user'), 'user.iduser=a.ps_supervisor_id', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff', array('staff.Email as supervisor_email','staff.Email2 as supervisor_email2'))
            ->joinLeft(array('d' => 'tbl_definationms'), 'd.idDefinition  = staff.FrontSalutation', array('DefinitionDesc as Salutation'))
            ->joinLeft(array('salutation' => 'tbl_definationms'), 'salutation.idDefinition  = staff.FrontSalutation',array("CONCAT(salutation.DefinitionDesc,' ',staff.FullName) as supervisor_name"))
            ->joinLeft(array('history' => 'thesis_supervisor_history'), 'history.ps_id=a.ps_id', array('history.reason'))
            ->where('a.ps_pid = ?', $pid)
            ->where('a.ps_type = ?', $type);

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getDirector($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array('a' => 'tbl_studentregistration'), array('registrationId'))
            ->join(array('b' => 'tbl_chiefofprogramlist'), 'b.IdProgram = a.IdProgram', array('IdProgram'))
            ->join(array('c' => 'tbl_staffmaster'), 'c.IdStaff  = b.IdStaff', array('FullName','Email','Email2'))
            ->joinLeft(array('d' => 'tbl_definationms'), 'd.idDefinition  = c.FrontSalutation', array('DefinitionDesc as Salutation'))
            ->where('a.registrationId = ?', $id)
            ->order('b.createddt desc')
        ;

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getSupervisorsStudent($id, $type)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("c" => 'thesis_proposal'))
            ->join(array("a" => $this->_supervisor), 'a.ps_pid=c.p_id', array())
            ->join(array("b" => $this->_superrole), 'a.ps_supervisor_role=b.supervisor_id', array('b.code as role_name'))
            ->join(array('user' => 'tbl_user'), 'user.iduser=a.ps_supervisor_id', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff', array('staff.FullName as supervisor_name', 'staff.Email as supervisor_email','Phone'))
            ->where('c.student_id = ?', $id)
            ->where('c.pid = ?', 0)
        ;

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSupervisorsStudentDistinct($id, $type)
    {
        $db = $this->db;
        $select = $db->select()
//            ->distinct('a.ps_supervisor_id')
            ->from(array("c" => 'thesis_proposal'))
            ->join(array("a" => $this->_supervisor), 'a.ps_pid=c.p_id', array())
            ->join(array("b" => $this->_superrole), 'a.ps_supervisor_role=b.supervisor_id', array('b.code as role_name'))
            ->join(array('user' => 'tbl_user'), 'user.iduser=a.ps_supervisor_id', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff', array('staff.FullName as supervisor_name', 'staff.Email as supervisor_email','Phone'))
            ->where('c.student_id = ?', $id)
            ->where('c.pid = ?', 0)
            ->group('a.ps_supervisor_id')
        ;

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getSupervisor($pid, $type, $user)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_supervisor))
            ->join(array("b" => $this->_superrole), 'a.ps_supervisor_role=b.supervisor_id', array('b.code as role_name'))
            ->join(array('user' => 'tbl_user'), 'user.iduser=a.ps_supervisor_id', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = user.IdStaff', array('staff.FullName as supervisor_name', 'staff.Email as supervisor_email'))
            ->joinLeft(array('d' => 'tbl_definationms'), 'd.idDefinition  = staff.FrontSalutation', array('DefinitionDesc as Salutation'))
            ->joinLeft(array('salutation' => 'tbl_definationms'), 'salutation.idDefinition  = staff.FrontSalutation',array("CONCAT(salutation.DefinitionDesc,' ',staff.FullName) as supervisor_name"))
            ->where('a.ps_pid = ?', $pid)
            ->where('a.ps_type = ?', $type);

        if($user){
            $select->where('a.ps_supervisor_id = ?', $user);
        }

        $result = $db->fetchRow($select);
        return $result;
    }

    public static function getExaminers($pid, $type)
    {
        $db = getDb();
        $select = $db->select()
            ->from(array("a" => 'thesis_article_examiner'))
            ->join(array('user' => 'thesis_examiner_user'), 'user.id=a.ex_examiner_id', array('user.fullname as supervisor_name'))
            ->join(array("b" => 'thesis_examiner_role'), 'a.ex_examiner_role=b.exr_id', array('b.code as role_name'))
            ->joinLeft(array('u' => 'tbl_user'), 'user.staff_id = u.iduser and user.staff_id > 0', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as internal_fullname')
            ->where('a.ex_pid = ?', $pid)
            ->where('a.ex_type = ?', $type);

        $result = $db->fetchAll($select);
        return $result;
    }

    /* Employment */
    public function addEmployment($data)
    {
        $db = $this->db;

        $db->insert($this->_employment, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getEmployment($pid)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_employment))
            ->where('a.ae_pid = ?', $pid);

        $result = $db->fetchAll($select);
        return $result;
    }

    /*
        Articleship
    */
    public function getArticleshipData()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_article))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('st' => $this->_status), 'a.status=st.status_id', array('st.status_code'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=a.semester_id', array('sem.SemesterMainName as semester_name'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name');


        return $select;
    }


    public function getArticleshipSupervisee($id, $where = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_article), array('a.*', 'a.a_id as research_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email'))
            ->join(array('st' => $this->_status), 'a.status=st.status_id', array('st.status_code'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=a.semester_id', array('sem.SemesterMainName as semester_name'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->join(array('sup' => 'thesis_article_supervisor'), 'sup.ps_pid=a.a_id', array());

        if (isset($where['status']) && $where['status'] != '') {
            $select->where('a.status = ?', $where['status']);
        }

        $select->where('sup.ps_type=\'articleship\' AND sup.ps_supervisor_id = ?', $id);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getArticleshipExaminee($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_article))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('st' => $this->_status), 'a.status=st.status_id', array('st.status_code'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=a.semester_id', array('sem.SemesterMainName as semester_name'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->join(array('ex' => 'thesis_article_examiner'), 'ex.ex_pid=a.a_id', array());

        $select->where('ex.ex_type=\'articleship\' AND ex.ex_examiner_id = ?', $id);

        $result = $db->fetchAll($select);

        return $result;
    }


    public function getArticleshipByStudent($studentid)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_article))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('st' => $this->_status), 'a.status=st.status_id', array('st.status_code'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=a.semester_id', array('sem.SemesterMainName as semester_name'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->where('a.student_id = ?', $studentid);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getArticleship($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_article))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('st' => $this->_status), 'a.status=st.status_id', array('st.status_code'))
            ->joinLeft(array('sem' => 'tbl_semestermaster'), 'sem.IdSemesterMaster=a.semester_id', array('sem.SemesterMainName as semester_name'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->where('a.a_id = ?', $id);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function updateArticleship($data, $where)
    {
        $db = $this->db;

        $db->update($this->_article, $data, $where);
    }

    public function addArticleship($data)
    {
        $db = $this->db;

        $db->insert($this->_article, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    /*
        Exemption
    */
    public function getExemptionData()
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_exemption))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('st' => $this->_status), 'a.status=st.status_id', array('st.status_code'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name');


        return $select;
    }


    public function getExemptionSupervisee($id, $where = null)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_exemption), array('a.*', 'a.e_id as research_id'))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name', 'appl_email as student_email'))
            ->join(array('st' => $this->_status), 'a.status=st.status_id', array('st.status_code'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->join(array('sup' => 'thesis_article_supervisor'), 'sup.ps_pid=a.e_id', array());

        if (isset($where['status']) && $where['status'] != '') {
            $select->where('a.status = ?', $where['status']);
        }

        $select->where('sup.ps_type=\'exemption\' AND sup.ps_supervisor_id = ?', $id);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getExemptionExaminee($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_exemption))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('st' => $this->_status), 'a.status=st.status_id', array('st.status_code'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->join(array('ex' => 'thesis_article_examiner'), 'ex.ex_pid=a.e_id', array());

        $select->where('ex.ex_type=\'exemption\' AND ex.ex_examiner_id = ?', $id);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getExemptionByStudent($studentid)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_exemption))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('st' => $this->_status), 'a.status=st.status_id', array('st.status_code'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->where('a.student_id = ?', $studentid);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getExemption($id)
    {
        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_exemption))
            ->join(array('sr' => 'tbl_studentregistration'), 'a.student_id=sr.IdStudentRegistration', array('sr.registrationId'))
            ->join(array('p' => 'student_profile'), 'p.id=sr.sp_id', array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
            ->join(array('st' => $this->_status), 'a.status=st.status_id', array('st.status_code'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as staff_name')
            ->where('a.e_id = ?', $id);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function updateExemption($data, $where)
    {
        $db = $this->db;

        $db->update($this->_exemption, $data, $where);
    }

    public function addExemption($data)
    {
        $db = $this->db;

        $db->insert($this->_exemption, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getEmploymentSingle($id)
    {
        $db = $this->db;
        $select = $db->select()
            ->from(array("a" => $this->_employment))
            ->where('a.ae_id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    /*
        Get User By Role
    */
    public function getUserByRole($id, $role)
    {
        $db = $this->db;

        switch ($role) {
            case 'admin':
            case 'supervisor':

                $select = $db->select()
                    ->from(array('u' => 'tbl_user'), array())
                    ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as user_name')
                    ->where('u.iduser = ?', $id);

                break;
        }

        $results = $db->fetchRow($select);
        return $results;
    }

    public function saveSubmission($id, $type, $status)
    {
        $db = $this->db;
        $defModel = new App_Model_General_DbTable_Definationms();
        $thesisModel = new Thesis_Model_DbTable_General();
        $regModel = new Thesis_Model_DbTable_Registration();

        $getResearchType = $defModel->getIdByDefType('Research Type', $type); // research type
        $getSubmission = $thesisModel->getSubmissionByResearchId($id, $getResearchType['key']);
        $getProposal = $regModel->getProposal($id);

        $typeId = ($type == 'proposal') ? 1 : 0;
        $agree = 1;
        $approved = 1;

        if ($getSubmission && $getProposal) {

            $submissionId = $getSubmission['id'];

            $dataUpd = array(
                'status'       => $thesisModel->getStatusByCode($status,$typeId),
                'agree'        => $agree,
                'approved'     => null,
                'created_role' => 'student',
                'created_by'   => $this->auth->getIdentity()->iduser ?? $getProposal['student_id'],
                'created_date' => new Zend_Db_Expr('NOW()')
            );

            $thesisModel->updateSubmission($dataUpd, 'id = ' . (int)$submissionId);

        } else {

            $data = array(
                'student_id'    => $getProposal['student_id'],
                'research_id'   => $id,
                'research_type' => $getResearchType['key'],
                'submit_type'   => 'pre',
                'submit_date'   => null,
                'agree'         => $agree,
                'approved'      => $approved,
                'status'        => $thesisModel->getStatusByCode($status,$typeId),
                'created_role'  => 'student',
                'created_by'    => $this->auth->getIdentity()->iduser ?? $getProposal['student_id'],
                'created_date'  => new Zend_Db_Expr('NOW()')
            );

            $submissionId = $thesisModel->addSubmission($data);
        }

        $dataHistory = array(
            'submission_id' => $submissionId,
            'student_id'    => $getProposal['student_id'],
            'research_id'   => $id,
            'research_type' => $getResearchType['key'],
            'submit_type'   => 'pre',
            'submit_date'   => null,
            'agree'         => $agree,
            'approved'      => $approved,
            'status'        => $thesisModel->getStatusByCode($status,$typeId),
            'created_role'  => 'student',
            'created_by'    => $this->auth->getIdentity()->iduser ?? $getProposal['student_id'],
            'created_date'  => new Zend_Db_Expr('NOW()')
        );

        $db->insert($this->_submission_history, $dataHistory);

    }

    public function getSubmissionHistory($studentId)
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => $this->_submission_history), array('a.*'))
//            ->join(array('b' => $this->_submission_history), 'a.id = b.submission_id', array(''))
            ->join(array('b' => 'tbl_definationms'), " a.research_type = b.idDefinition ", array('type' => 'b.DefinitionDesc'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=a.status', array('st.status_code as status_name', 'st.status_description'))
            ->joinLeft(array('u' => 'tbl_user'), 'u.iduser = a.created_by', array())
            ->joinLeft(array('staff' => 'tbl_staffmaster'), 'staff.IdStaff  = u.IdStaff', 'FullName as created_user')
            ->where('a.student_id = ?', $studentId)
            ->order('a.created_date asc');

        $result = $db->fetchAll($select);
        return $result;

    }

    /*
        Parse Content
    */

    public static function parseContent($research, $template)
    {
        $commDb = new App_Model_Template();

        $tags = $commDb->getTemplateTags('thesis');
        
        foreach ($tags as $tag) {
            if (preg_match('/\[' . $tag['tag_name'] . '\]/', $template)) {
                switch ($tag['tag_name']) {
                    case "Date":
                        $template = str_replace('[' . $tag['tag_name'] . ']', format_date(date('d-m-Y'), 'd F Y'), $template);
                        break;

                    case "Supervisor":
//                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['supervisor_info']['supervisor_name'], $template);
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['supervisor_info'], $template);
                        break;

                    case "Data":

                        if ($research['research_type'] == 'articleship') {
                            $html = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;"><tr><th style="padding:5px; border:1px solid #ccc">STUDENT NAME</th><th style="padding:5px; border:1px solid #ccc">COMPANY</th><th style="padding:5px; border:1px solid #ccc">DATE OF ARTICLESHIP</th></tr>';
                            $html .= '<tr><td style="padding:5px; border:1px solid #ccc">' . $research['student_name'] . '</td><td style="padding:5px; border:1px solid #ccc">' . $research['company'] . '</td><td style="padding:5px; border:1px solid #ccc">' . format_date($research['start_date']) . ' - ' . format_date($research['end_date']) . '</td></tr></table>';

                            $template = str_replace('[' . $tag['tag_name'] . ']', $html, $template);
                        } else if ($research['research_type'] == 'exemption') {
                            $html = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;"><tr><th style="padding:5px; border:1px solid #ccc">STUDENT NAME</th><th style="padding:5px; border:1px solid #ccc">PROPOSED TOPIC</th><th style="padding:5px; border:1px solid #ccc">DATE OF ARTICLESHIP</th></tr>';
                            $html .= '<tr><td style="padding:5px; border:1px solid #ccc">' . $research['student_name'] . '</td><td style="padding:5px; border:1px solid #ccc">' . $research['proposedarea'] . '</td><td style="padding:5px; border:1px solid #ccc">' . format_date($research['start_date']) . ' - ' . format_date($research['end_date']) . '</td></tr></table>';

                            $template = str_replace('[' . $tag['tag_name'] . ']', $html, $template);
                        }
                        break;

                    case "Title":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['p_title'], $template);
                        break;

                    /* approval only */

                    case "Name - Student":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['student_name'], $template);
                        break;

                    case "Address - Student":
                        //student info
                        $address = trim($research["appl_address1"]);

                        if ($research["appl_address2"]) {
                            $address .= '<br/>' . $research["appl_address2"] . ' ';
                        }

                        if ($research["appl_postcode"]) {
                            $address .= '<br/>' . $research["appl_postcode"] . ' ';
                        }

                        if ($research["appl_city"] != 99) {
                            $address .= '<br />' . $research["CityName"] . ' ';
                        } else {
                            $address .= '<br/ >' . $research["appl_city_others"] . ' ';
                        }

                        if ($research["appl_state"] != 99) {
                            $address .= $research["StateName"] . ' ';
                        } else {
                            $address .= $research["appl_state_others"];
                        }

                        $address .= '<br/>' . $research["CountryName"];

                        $address = trim(str_replace('_x000D_', '', $address));

                        $template = str_replace("[" . $tag['tag_name'] . "]", strtoupper($address), $template);
                        break;

                    case "ID - Student":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['registrationId'], $template);
                        break;

                    case "Semester Start":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['SemesterMainName'], $template);
                        break;

                    case "Course":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['course'], $template);
                        break;

                    case "Semester End Date":
                        $template = str_replace('[' . $tag['tag_name'] . ']', format_date_to_view($research['semester_end_date']), $template);
                        break;

                    case "Director":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['director_info'], $template);
                        break;

                    case "Activity":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['ActivityName'], $template);
                        break;

                    case "Program":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['ProgramCode'], $template);
                        break;

                    case "User":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['fName'], $template);
                        break;

                    case "Keylink":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['keylink'], $template);
                        break;

                    case "Message":
                        $template = str_replace('[' . $tag['tag_name'] . ']', $research['message'], $template);
                        break;

                }
            }
        }


        return $template;
    }

    public function addEvaluationFile($data)
    {
        $db = $this->db;

        $db->insert($this->_evaluation_file, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getActivities($id,$course)
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'thesis_research_activity_tagged'), array('a.*'))
            ->joinLeft(array('tp' => 'thesis_proposal'), "tp.course_id = a.rat_course AND tp.activity_id = a.rat_type AND (tp.pid = $id OR tp.p_id = $id)", array('tp.pid','tp.p_id','created_date','grade_status'))
            ->joinLeft(array('gd' => 'tbl_definationms'), 'gd.idDefinition = tp.grade_status', array('DefinitionDesc as Grade'))
            ->joinLeft(array('td' => 'tbl_definationms'), 'td.idDefinition = a.rat_type', array('DefinitionDesc','DefinitionCode'))
            ->joinLeft(array('st' => 'thesis_status'), 'st.status_id=tp.p_status', array('st.status_code as status_name', 'st.status_description'))
            ->joinLeft(array('pst' => 'thesis_status'), 'pst.status_id=tp.pd_status', array('pst.status_code as pd_status_name', 'pst.status_description as pd_status_description'))
            ->joinLeft(array('es' => 'thesis_event_student'), 'es.research_id = tp.p_id', array('start_time','end_time'))
            ->joinLeft(array('e' => 'thesis_event_setup'), 'e.event_id = es.event_id and e.event_display = 1', array('event_id','event_name','event_date','event_venue','event_remark','event_display'))
//            ->where('td.DefinitionCode != ?', 'registration')
            ->where('a.rat_course = ?', $course)
            ->order('td.idDefinition')
        ;
//echo $select;exit;
        $result = $db->fetchAll($select);
        return $result;

    }

    public function getActivity($id)
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'tbl_definationms'), array('DefinitionDesc as ActivityName'))
            ->where('a.idDefinition = ?', $id)
        ;

        $result = $db->fetchRow($select);
        return $result;

    }

    public function getSemesterBasedOnDate($date = null, $IdScheme)
    {

        $date = $date ?? date('Y-m-d');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from(array("a" => "tbl_semestermaster"))
            ->joinLeft(array('s' => 'tbl_scheme'), 's.IdScheme=a.IdScheme', array('EnglishDescription as SchemeName'))
            ->where("'$date'	BETWEEN SemesterMainStartDate AND SemesterMainEndDate")
            ->where('SemesterMainStartDate != ?','0000-00-00')
            ->where('a.IdScheme = ?',$IdScheme)
            ->order("SemesterMainStartDate DESC");

        $result = $db->fetchAll($select);

        return $result;
    }

    public function updateEmail2($data, $where)
    {
        $db = $this->db;

        $db->update('tbl_staffmaster', $data, $where);
    }

    public function getStaff($id)
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'tbl_staffmaster'))
            ->where('a.IdStaff = ?', $id)
        ;

        $result = $db->fetchRow($select);
        return $result;

    }

    public function addNotification($data)
    {
        $db = $this->db;

        $db->insert($this->_notification, $data);

        $_id = $db->lastInsertId();

        return $_id;
    }

    public function getMessage($id,$id2,$id3)
    {

        $db = $this->db;

        $select = $db->select()
            ->from(array('a' => 'thesis_student_notification'))
            ->where('a.research_id = ?', $id)
            ->where('a.student_id = ?', $id2)
            ->where('a.supervisor_id = ?', $id3)
            ->order('a.id desc')
        ;

        $result = $db->fetchAll($select);
        return $result;

    }
}