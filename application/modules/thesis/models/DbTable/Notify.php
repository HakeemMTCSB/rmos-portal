<?php
class Thesis_Model_DbTable_Notify extends Zend_Db_Table {
	
	protected $_topic = 'thesis_researchtopics';
	protected $_categories = 'thesis_categories';
	protected $_status = 'thesis_status';
	protected $_supervisor = 'thesis_supervisor';
	protected $_examinersem = 'thesis_examinersem';
	protected $_interest = 'thesis_fieldofinterest';

	protected $_activity = 'thesis_activity';
	protected $_comments = 'thesis_activity_comments';
	protected $_files = 'thesis_activity_files';
	protected $_progreport = 'thesis_progressreport';

	protected $_activity_setup = 'thesis_activity_setup';
	protected $_notify = 'thesis_notify';

	protected $subs;

	public function init()
	{
		$this->db = Zend_Db_Table::getDefaultAdapter();
		$this->locale = Zend_Registry::get('Zend_Locale');
	}

	public function getNotifiesForSupervisor( $supervisor_id , $limit = 10)
	{
		$db = $this->db;
		$select = $db->select()
	                ->from(array('a'=>$this->_notify))
					->where('a.recipient_id = ?', $supervisor_id)
					->order('a.id DESC')
					->limit($limit);

		return $db->fetchAll( $select );
	}

	public static function parseIcon($icon)
	{
		switch($icon)
		{
			case 'newcomment': return 'icon-chat'; break;
		}
	}

	public static function parseNotify($param='')
	{
		$replace = array();
		$find = array();
		
		$data = json_decode($param['target_data']);

		if ( $param['student_id'] != '' )
		{
			$student = self::getStudentById($param['student_id']);
			$find[] = '{student}';
			$replace[] = '<label>'.$student['student_name'].'</label>';
		}

		switch ( $param['notify'] ) 
		{
			case 'newcomment': 
				$activity = Thesis_Model_DbTable_Tracking::getActivity($data->activity_id);
				$find[] = '{activity}';
				$replace[] = $activity['title'];

				$text = '<a href="'.$param['target_url'].'">New feedback posted by {student} for {activity}</a>'; 
			break;
		}

		return str_replace($find, $replace, $text);
	}

	public static function getStudentById($IdStudentRegistration='')
	{
		$db = getDb();
		
		$select = $db->select()
	                ->from(array('sr' => 'tbl_studentregistration'),array())
					->join(array('p'=>'student_profile'),'p.id=sr.sp_id',array('CONCAT_WS(" ",appl_fname,appl_mname,appl_lname) as student_name'))
					->where('sr.IdStudentRegistration = ?', $IdStudentRegistration);
		
		return $db->fetchRow( $select );
	}
	
	public function notifySupervisors($param)
	{
		$db = $this->db;

		//check total supervisors
		$supervisors = $param['supervisors'];

		if ( count($supervisors) > 0 )
		{
			foreach ( $supervisors as $sup )
			{
				$data = array(
								'recipient_id'	=>	$sup['ps_supervisor_id'],
								'recipient_type' => 'supervisor',
								'notify' => 'newcomment',
								'notify_parsed' => '',
								'student_id' => $param['student_id'],
								'created_date' => $param['date'],
								'target_url' => $param['url'],
								'target_data' => json_encode(array('activity_id' => $param['activity_id'],'research_id' => $param['research_id'],'research_type' => $param['research_type']))
							);

				$db->insert($this->_notify, $data);
			}
		}
	}
}