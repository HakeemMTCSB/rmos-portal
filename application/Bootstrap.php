<?php
require_once('common_functions.php');

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initAutoload() {
		
		$moduleLoader = new Zend_Application_Module_Autoloader(array (
				'namespace'	=> 'App_'	,
				'basePath'	=> APPLICATION_PATH));
		
		return $moduleLoader;
	}
	
	
	protected function _initViewHelpers() {
		
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();
		
		$view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
	    $view->addHelperPath ( 'Zend/Dojo/View/Helper/', 'Zend_Dojo_View_Helper' );
		 		
		$view->doctype ('HTML5');
		$view->headMeta()->appendHttpEquiv ('Content-Type','text/html;charset=utf-8');
		$view->headMeta()->appendHttpEquiv ('X-UA-Compatible','IE=edge');
		$view->headMeta()->appendHttpEquiv ('viewport','width=device-width, initial-scale=1');
		$view->headMeta()->appendHttpEquiv ('Cache-control','no-cache');
		$view->headMeta()->appendHttpEquiv ('Pragma','no-cache');
		$view->headTitle()->setSeparator (' - ');
		$view->headTitle(APPLICATION_ENTERPRISE_SHORT ." - ". APPLICATION_TITLE_SHORT);

		$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
        $viewRenderer->setView($view);
        
        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);	
	}
	
	protected function setconstants($constants){
        foreach ($constants as $key=>$value){
            if(!defined($key)){
                define($key, $value);
            }
        }
	}
	
	protected function _initTranslate(){
		$registry = Zend_Registry::getInstance();	
		
		 // Create Session block and save the locale
        $session = new Zend_Session_Namespace('session');  
       
		    	
		$locale = new Zend_Locale('en_US');		
		$file = APPLICATION_PATH . DIRECTORY_SEPARATOR .'languages'. DIRECTORY_SEPARATOR . "en_US.php";
			
						
		$translate = new Zend_Translate('array',
            $file, $locale,
            array(
            'disableNotices' => true,    // This is a very good idea!
            'logUntranslated' => false,  // Change this if you debug
            )
        );
        
		        
        $registry->set('Zend_Locale', $locale);
        $registry->set('Zend_Translate', $translate);
              
        
        return $registry;
	}
	
	protected function _initPlugin(){
		$fc = Zend_Controller_Front::getInstance();
        $fc->registerPlugin(new icampus_Plugin_LangSelector());        
	}
	
	
	protected function _initConfig()
    {
        $config = new Zend_Config($this->getOptions());
        Zend_Registry::set('config', $config);
    }
    
	protected function _initLoadAclIni ()
	{
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/acl.ini');
		Zend_Registry::set('acl', $config);
	}
	
	protected function _initAuth() {
		/*
         * ACL
         */
		
		//All Module & Controller
		$acl = array();
    	$front = Zend_Controller_Front::getInstance();
		foreach ($front->getControllerDirectory() as $module => $path) {

                foreach (scandir($path) as $file) {

                        if (strstr($file, "Controller.php") !== false) {

                                include_once $path . DIRECTORY_SEPARATOR . $file;

                                foreach (get_declared_classes() as $class) {

                                        if (is_subclass_of($class, 'Zend_Controller_Action')) {

                                                $controller = strtolower(substr($class, 0, strpos($class, "Controller")));
                                                $actions = array();

                                                foreach (get_class_methods($class) as $action) {

                                                        if (strstr($action, "Action") !== false) {
                                                        		$action = str_replace("Action", "", $action);
                                                                $actions[] = $action;
                                                        }
                                                }
                                        }
                                }

                                $acl[$module][$controller] = $actions;
                        }
                }
   		}
   		
   		
   		
        $acl = new icampus_Plugin_Acl($acl);
		
		/*
		 * AUTH
		 */
		
		$auth = Zend_Auth::getInstance();
        $fc = Zend_Controller_Front::getInstance();

        //never expire
        $authns = new Zend_Session_Namespace('Zend_Auth');
        $authns->setExpirationSeconds(9999);
        
        //$fc->registerPlugin(new icampus_Plugin_Auth($auth,null));
        $fc->registerPlugin(new icampus_Plugin_Auth( $auth, $acl ));

	}
	
	protected function _initACLLayout(){
		$auth = Zend_Auth::getInstance();
		
		if ($auth->hasIdentity()) {
			$front = Zend_Controller_Front::getInstance(); 
        	$front->registerPlugin(new icampus_Plugin_Layout()); 
		}
	}
	

	protected function _initDomPdf(){
		//set_include_path(APPLICATION_PATH . "/../../library/dompdf/" . PATH_SEPARATOR . get_include_path());
		set_include_path("/var/www/html/triapp/library/dompdf/" . PATH_SEPARATOR . get_include_path());
    }
    
    /*protected function _initStudentSelector(){
    	$front = Zend_Controller_Front::getInstance();
    	$front->registerPlugin(new icampus_Plugin_StudentSelector());
    	
    }*/

	protected function _initDatabase(){
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');
		$parameters = array('host'=>$config->resources->db->params->host,
			'username' => $config->resources->db->params->username,
			'password'=>$config->resources->db->params->password,
			'dbname'=>$config->resources->db->params->dbname,
			'driver_options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8')
		);
		if(isset($config->resources->db->params->unix_socket)) {
			$params['unix_socket'] = $config->resources->db->params->unix_socket;
		}

		try {
			$db = Zend_Db::factory('Pdo_Mysql', $parameters);

			$db->getConnection();
			//Zend_Registry::set('sisdb', $db);
			Zend_Db_Table_Abstract::setDefaultAdapter($db);
		} catch (Zend_Db_Adapter_Exception $e) {
			echo $e->getMessage();
			die('Could not connect to database.');
		} catch (Zend_Exception $e) {
			echo $e->getMessage();
			die('Could not connect to database.');
		}

		Zend_Registry::set('dbapp', $db);

		if ( $_SERVER['REMOTE_ADDR'] == '127.0.0.1' )
		{
			//$profiler = new Zend_Db_Profiler_Firebug('All DB Queries');
			//$profiler->setEnabled(true);
			//$db->setProfiler($profiler);
		}

		$resource = $this->getPluginResource('multidb');
		Zend_Registry::set("multidb", $resource);
	}
}

