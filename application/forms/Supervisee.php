<?php
class App_Form_Supervisee extends Zend_Form
{
    public function init()
    {
        $this->setMethod('post');
//        $this->setAttrib('id','search_registry');

//        //SubCode
//        $this->addElement('text','SubCode', array(
//            'label'=>'Course',
//            'class'=>'input-txt'
//        ));

//        //Student
//        $this->addElement('text','student-search', array(
//            'label'=>'Student',
//            'class'=>'input-txt'
//        ));

//        //race desc english
//        $this->addElement('text','race_desc', array(
//            'label'=>'Name(Malay)',
//            'class'=>'input-txt'
//        ));

//        //race type
//        $registryDb = new GeneralSetup_Model_DbTable_Registry_RegistryType();
//        $this->addElement('select','race_type', array(
//            'label'=>'Race Type',
//            'required'=>false
//        ));
//
//        $this->race_type->addMultiOption(null,"-- Any --");
//        foreach($registryDb->getDataByCodeType('race-type')  as $type){
//            $this->race_type->addMultiOption($type["key"],$type["value"]);
//        }

//        $this->addElement('checkbox', 'active', array(
//            'label'      => 'Active',
//            'required'   => false,
//            'checked' => true,
//        ));

        $this->addElement('submit', 'save', array(
            'label'=>'Search',
            'decorators'=>array('ViewHelper')
        ));

        $this->addElement('reset', 'clear', array(
            'label'=>'Clear',
            'onclick'=>"window.location=window.location;",
            'decorators'=>array('ViewHelper'),
        ));

        $this->addDisplayGroup(array('save','clear'),'buttons', array(
            'decorators'=>array(
                'FormElements',
                array('HtmlTag', array('tag'=>'div', 'class'=>'buttons')),
                'DtDdWrapper'
            )
        ));
    }
}