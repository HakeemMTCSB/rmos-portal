<?php
/*
* thumbnail stuff
* munzir 4/2011
*/
error_reporting(0);
set_time_limit(3600);


$f			= str_replace(' ','%20',stripslashes($_GET['f']));
$new_width	= $_GET['w'] != '' ? $_GET['w'] : 150;
$new_height = (int) isset($_GET['h']) ? $_GET['h'] : 0;
$new_height = $new_height > 0 ? $new_height : '';

$s			= isset($_GET['s']) && $_GET['s'] == '1' ? 1:0;

//cache
$usecache		= 0;
$cache_folder	= 'thumbcache/';
$cache_expire	= isset($_GET['p']) && $_GET['p'] != '' ? $_GET['p'] : (86400 * 30); // 30 days
$fileext		= get_ext(basename(strtolower($f)));

if ( $usecache && !is_dir($cache_folder) )
{
	@mkdir($cache_folder,0777);
}

if ( $fileext == '' )
{
	switch(exif_imagetype($f))
	{
		case IMAGETYPE_GIF: 
			header("Content-type: image/gif");
			$type = "gif";
		break;

		case IMAGETYPE_JPEG:
			header("Content-type: image/jpeg");
			$type = "jpeg";
		break;

		case IMAGETYPE_PNG:
			header("Content-type: image/png");
			$type = "png";
		break;
	}
}
else
{
	//DETERMINE IMAGE TYPE
	if ( $fileext == "png" )
	{
		header("Content-type: image/png");
		$type = "png";
	}
	else if ( $fileext == "gif" )
	{
		header("Content-type: image/gif");
		$type = "gif";
	}
	else
	{
		header("Content-type: image/jpeg");
		$type = "jpeg";
	}
}

if ( $type == '' )
{
	exit;
}


if ( $usecache )
{
	$filename = substr(sha1($_GET['f']),0,8).".".$fileext;
	
	$cache_file = $cache_folder.$new_width.$new_height.'_'.$filename;
	$cache_file = $s == 1 ? $cache_folder.$new_width.'_square_'.$filename:$cache_file; //square
	
	if($_GET['k'] == 1)
	{
		@unlink($cache_file);
	}
	
	$exists = 0;

	if ( file_exists($cache_file) )
	{
		$exists = 1;
		/*$unique = substr(md5($_GET['f']),0,4);
		$uniquefile = $s==1 ? $cache_file.$new_width.'_square_'.$unique.'-'.$filename : $cache_file.$new_width.'_'.$unique.'-'.$filename;
		
		$cache_file = file_exists($uniquefile) ? $uniquefile : $cache_file;*/
	}
	

	if ( $exists == 1 )
	{
		if ( time() > ( filemtime($cache_file) + $cache_expire ) )
		{
			@unlink($cache_file);
			make_thumbnail($f);
			//echo readfile($cache_file);
			header('location: '.$cache_file);
		}
		else
		{
			$fileModTime = filemtime($cache_file);
			$expires = $fileModTime+$cache_expire;

			/*$headers = getRequestHeaders();			
			if (isset($headers['If-Modified-Since']) && (strtotime($headers['If-Modified-Since']) == $fileModTime)) 
			{
				// Client's cache IS current, so we just respond '304 Not Modified'.
			    header('Last-Modified: '.gmdate('D, d M Y H:i:s', $fileModTime).' GMT', true, 304);
				header('Cache-Control: max-age=3600, must-revalidate');
			}
			else
			{*/
				header('Last-Modified: '.gmdate('D, d M Y H:i:s', $fileModTime).' GMT', true, 200);
				header('Expires: '.gmdate('D, d M Y H:i:s', $expires ).' GMT');
				header("Cache-Control: maxage=".$cache_expire);

				//echo readfile($cache_file);
				header('location: '.$cache_file);
			//}
			//exit;
		}
	}
	else
	{
		make_thumbnail($f);
		
		header('location: '.$cache_file);
		//echo readfile($cache_file);
		//header('location: '.$cache_file);
		exit;
	}
}
else
{
	make_thumbnail($f);
}

function getRequestHeaders() {
  if (function_exists("apache_request_headers")) {
    if($headers = apache_request_headers()) {
      return $headers;

    }
  }
  $headers = array();
  // Grab the IF_MODIFIED_SINCE header
  if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
    $headers['If-Modified-Since'] = $_SERVER['HTTP_IF_MODIFIED_SINCE'];
  }
  return $headers;
}

function get_ext($file="")
{
	return substr($file,(strrpos($file,".")?strrpos($file,".")+1:strlen($file)),strlen($file)); 
}


function make_thumbnail($f)
{
	global $s;
	
	if ($f != '')
	{	
		global $cache_file,$new_width, $new_height,$type;

		$file = $f;
		$split = explode(".", $file);
		$name = $split[0];


		switch ($type)
		{
			case "png" : $image = imagecreatefrompng($file); break;
			case "gif" : $image = imagecreatefromgif($file); break;
			case "jpg": case "jpeg": $image = imagecreatefromjpeg($file); break;
		}
		
		
		//check image size first
		$size = getimagesize($f);
	
		//kalau width yang dia nak beso dari yang image punya
		if ($new_width > $size[0])
		{
			echo file_get_contents($f);
			//readfile($f);
			exit;
		}


		$width = imagesx($image) ;
		$height = imagesy($image) ;
		
		//square
		if ( $s == 1 )
		{
			if($width > $height)
			{
				$x = ceil(($width - $height) / 2 );
				$width = $height;
			}
			elseif($height> $width)
			{
				$y = ceil(($height - $width) / 2);
				$height = $width;
			}
			
			if ( function_exists(imagecreatetruecolor) && $type != 'gif' )
			{
				$thumb = imagecreatetruecolor( $new_width, $new_width );
			}
			else
			{
				$thumb = imagecreate($new_width, $new_width );
			}

			if ( $type == 'png' )
			{
				imagealphablending( $thumb, false );
				imagesavealpha( $thumb, true );
			}


			imagecopyresampled($thumb,$image,0,0,$x,$y,$new_width,$new_width,$width,$height);
		}
		else
		{
			$x = $y = 0;

			if ( $new_height > 0 )
			{
				if ( $new_height == 0 )
				{
					$new_height = ($new_width * $height) / $width ;
				}
				else
				{

					$ratio = array(0 => $width / $height, 1 => $new_width / $new_height);

					if ($ratio[0] > $ratio[1]) {
						$width = $height * $ratio[1];
						$x = (imagesx($image) - $width) / 2;
					}

					else if ($ratio[0] < $ratio[1]) {
						$height = $width / $ratio[1];
						$y = (imagesy($image) - $height) / 2;
					}
				}
			}
			else
			{
				$new_height = ($new_width * $height) / $width ;
			}

			if ( function_exists(imagecreatetruecolor) && $type != 'gif' )
			{
				$thumb = imagecreatetruecolor($new_width,$new_height);
			}
			else
			{
				$thumb = imagecreate($new_width,$new_height);
			}
			
			if ( $type == 'png' )
			{
				imagealphablending( $thumb, false );
				imagesavealpha( $thumb, true );
			}

			//imagecopyresized($thumb,$image,0,0,0,0,$new_width,$new_height,$width,$height);
			imagecopyresampled($thumb, $image, 0, 0, $x, $y, $new_width, $new_height, $width, $height);

			if ( $new_width > 800 )
			{
				$thumb = UnsharpMask($thumb, 50, 0.5, 0);
			}
			else if ( $new_width > 400 )
			{
				$thumb = UnsharpMask($thumb, 50, 0.3, 0);
			}
			else
			{
				$thumb = UnsharpMask($thumb, 30, 0.2, 0);
			}
		}

		switch ($type)
		{
			case "png":				
				$type = imagepng( $thumb ,$cache_file );			
			break;
			/* ----------------------------------------------------- */
			case "gif":				
				$type = imagegif( $thumb, $cache_file );			
			break;
			/* ----------------------------------------------------- */
			case "jpeg":				
				$type = imagejpeg( $thumb,$cache_file ,100 );			
			break;
			/* ----------------------------------------------------- */
		}

		imagedestroy($image);
	}
}

function UnsharpMask($img, $amount, $radius, $threshold)    { 

////////////////////////////////////////////////////////////////////////////////////////////////  
////  
////                  Unsharp Mask for PHP - version 2.1.1  
////  
////    Unsharp mask algorithm by Torstein H�nsi 2003-07.  
////             thoensi_at_netcom_dot_no.  
////               Please leave this notice.  
////  
///////////////////////////////////////////////////////////////////////////////////////////////  



    // $img is an image that is already created within php using 
    // imgcreatetruecolor. No url! $img must be a truecolor image. 

    // Attempt to calibrate the parameters to Photoshop: 
    if ($amount > 500)    $amount = 500; 
    $amount = $amount * 0.016; 
    if ($radius > 50)    $radius = 50; 
    $radius = $radius * 2; 
    if ($threshold > 255)    $threshold = 255; 
     
    $radius = abs(round($radius));     // Only integers make sense. 
    if ($radius == 0) { 
        return $img; imagedestroy($img); break;        } 
    $w = imagesx($img); $h = imagesy($img); 
    $imgCanvas = imagecreatetruecolor($w, $h); 
    $imgBlur = imagecreatetruecolor($w, $h); 
     

    // Gaussian blur matrix: 
    //                         
    //    1    2    1         
    //    2    4    2         
    //    1    2    1         
    //                         
    ////////////////////////////////////////////////// 
         

    if (function_exists('imageconvolution')) { // PHP >= 5.1  
            $matrix = array(  
            array( 1, 2, 1 ),  
            array( 2, 4, 2 ),  
            array( 1, 2, 1 )  
        );  
        imagecopy ($imgBlur, $img, 0, 0, 0, 0, $w, $h); 
        imageconvolution($imgBlur, $matrix, 16, 0);  
    }  
    else {  

    // Move copies of the image around one pixel at the time and merge them with weight 
    // according to the matrix. The same matrix is simply repeated for higher radii. 
        for ($i = 0; $i < $radius; $i++)    { 
            imagecopy ($imgBlur, $img, 0, 0, 1, 0, $w - 1, $h); // left 
            imagecopymerge ($imgBlur, $img, 1, 0, 0, 0, $w, $h, 50); // right 
            imagecopymerge ($imgBlur, $img, 0, 0, 0, 0, $w, $h, 50); // center 
            imagecopy ($imgCanvas, $imgBlur, 0, 0, 0, 0, $w, $h); 

            imagecopymerge ($imgBlur, $imgCanvas, 0, 0, 0, 1, $w, $h - 1, 33.33333 ); // up 
            imagecopymerge ($imgBlur, $imgCanvas, 0, 1, 0, 0, $w, $h, 25); // down 
        } 
    } 

    if($threshold>0){ 
        // Calculate the difference between the blurred pixels and the original 
        // and set the pixels 
        for ($x = 0; $x < $w-1; $x++)    { // each row
            for ($y = 0; $y < $h; $y++)    { // each pixel 
                     
                $rgbOrig = ImageColorAt($img, $x, $y); 
                $rOrig = (($rgbOrig >> 16) & 0xFF); 
                $gOrig = (($rgbOrig >> 8) & 0xFF); 
                $bOrig = ($rgbOrig & 0xFF); 
                 
                $rgbBlur = ImageColorAt($imgBlur, $x, $y); 
                 
                $rBlur = (($rgbBlur >> 16) & 0xFF); 
                $gBlur = (($rgbBlur >> 8) & 0xFF); 
                $bBlur = ($rgbBlur & 0xFF); 
                 
                // When the masked pixels differ less from the original 
                // than the threshold specifies, they are set to their original value. 
                $rNew = (abs($rOrig - $rBlur) >= $threshold)  
                    ? max(0, min(255, ($amount * ($rOrig - $rBlur)) + $rOrig))  
                    : $rOrig; 
                $gNew = (abs($gOrig - $gBlur) >= $threshold)  
                    ? max(0, min(255, ($amount * ($gOrig - $gBlur)) + $gOrig))  
                    : $gOrig; 
                $bNew = (abs($bOrig - $bBlur) >= $threshold)  
                    ? max(0, min(255, ($amount * ($bOrig - $bBlur)) + $bOrig))  
                    : $bOrig; 
                 
                 
                             
                if (($rOrig != $rNew) || ($gOrig != $gNew) || ($bOrig != $bNew)) { 
                        $pixCol = ImageColorAllocate($img, $rNew, $gNew, $bNew); 
                        ImageSetPixel($img, $x, $y, $pixCol); 
                    } 
            } 
        } 
    } 
    else{ 
        for ($x = 0; $x < $w; $x++)    { // each row 
            for ($y = 0; $y < $h; $y++)    { // each pixel 
                $rgbOrig = ImageColorAt($img, $x, $y); 
                $rOrig = (($rgbOrig >> 16) & 0xFF); 
                $gOrig = (($rgbOrig >> 8) & 0xFF); 
                $bOrig = ($rgbOrig & 0xFF); 
                 
                $rgbBlur = ImageColorAt($imgBlur, $x, $y); 
                 
                $rBlur = (($rgbBlur >> 16) & 0xFF); 
                $gBlur = (($rgbBlur >> 8) & 0xFF); 
                $bBlur = ($rgbBlur & 0xFF); 
                 
                $rNew = ($amount * ($rOrig - $rBlur)) + $rOrig; 
                    if($rNew>255){$rNew=255;} 
                    elseif($rNew<0){$rNew=0;} 
                $gNew = ($amount * ($gOrig - $gBlur)) + $gOrig; 
                    if($gNew>255){$gNew=255;} 
                    elseif($gNew<0){$gNew=0;} 
                $bNew = ($amount * ($bOrig - $bBlur)) + $bOrig; 
                    if($bNew>255){$bNew=255;} 
                    elseif($bNew<0){$bNew=0;} 
                $rgbNew = ($rNew << 16) + ($gNew <<8) + $bNew; 
                    ImageSetPixel($img, $x, $y, $rgbNew); 
            } 
        } 
    } 
    imagedestroy($imgCanvas); 
    imagedestroy($imgBlur); 
     
    return $img; 

} 

?>