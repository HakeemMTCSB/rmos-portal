/* common js functions */
function sure(msg)
{
	if (msg == undefined)
	{
		var msg = 'Are you sure?';
	}

	var ask = confirm(msg);
	if (ask == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function escapeHtml(string) 
{
	var entityMap = 
	{
		"&": "&amp;",
		"<": "&lt;",
		">": "&gt;",
		'"': '&quot;',
		"'": '&#39;',
		"/": '&#x2F;'
	};
	return String(string).replace(/[&<>"'\/]/g, function (s) {
	  return entityMap[s];
	});
}

/* simple menu */
$(function() 
{
	var menu_ul = $('#sidebar .menu ul > li > ul'), menu_a  = $('#sidebar .menu ul > li > a');
	menu_ul.hide();

	menu_a.click(function(e) 
	{
		if (  $(this).attr("href")== '#' ) 
		{
			e.preventDefault();
		}

		if(!$(this).hasClass('active')) {
			menu_a.removeClass('active');
			menu_ul.filter(':visible').slideUp('normal');
			$(this).addClass('active').next().stop(true,true).slideDown('normal');
		} else {
			$(this).removeClass('active');
			$(this).next().stop(true,true).slideUp('normal');
		}
	});

	if ($.fn.superfish)
	{
		$('.sfmenu').superfish();
	}
	
	var header = $("#sidebar");
	var headerY = header.offset().top;
	$(document).scroll(function () {
		var y = $(document).scrollTop()+50;

		if (y >= headerY) {
			header.addClass('fixed');
		} else {
			header.removeClass('fixed');
		}
	});

	//flashmessenger
	setTimeout(function(){
		$(".success").slideUp(function(){
			$(this).parent().remove();	
		});
	},2000);
	
	$(".fancy").fancybox();

	//tooltips
	loadTooltip();
	
	//scroll to top if hash exists
	setTimeout(function()
    {
        if (location.hash) {
        window.scrollTo(0, 0);
        }
    }, 1);

	//show active child
	if ( $('.menu ul li a.active').next('ul').length > 0 ) {
		$('.menu ul li a.active').next('ul').show();
	}

});

function loadTooltip()
{
	$('.tooltip').each(function() {
         $(this).qtip({
             content: {
                 text: $(this).next('.tooltiptext')
             },style: {
				classes: 'qtip-light'
			},
			hide: {
                fixed: true,
                delay: 300
            },
			position: {
				my: 'bottom center',  // Position my top left...
				at: 'top center', // at the bottom right of...
			}
         });
     });

	 $('.tooltip2').each(function() {
         $(this).qtip({
             content: {
                 text: $(this).next('.tooltiptext')
             },style: {
				classes: 'qtip-dark'
			},
			hide: {
                fixed: true,
                delay: 300
            },
			position: {
				my: 'bottom center',  // Position my top left...
				at: 'top center', // at the bottom right of...
			}
         });
     });
}

/* calendar */
function initCalendar(CalEvents)
{
	
	var transEndEventNames = {
			'WebkitTransition' : 'webkitTransitionEnd',
			'MozTransition' : 'transitionend',
			'OTransition' : 'oTransitionEnd',
			'msTransition' : 'MSTransitionEnd',
			'transition' : 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		$wrapper = $( '#custom-inner' ),
		$calendar = $( '#calendar' ),
		cal = $calendar.calendario({
			onDayClick : function( $el, data, dateProperties ) {

				if(data.content.length > 0 ) {
					showEvents(data.content, dateProperties );
				}

			},
			caldata : CalEvents,
			displayWeekAbbr : true,
			events: 'click'
		} ),
		$month = $( '#custom-month' ).html( cal.getMonthName() ),
		$year = $( '#custom-year' ).html( cal.getYear() );

	$( '#custom-next' ).on( 'click', function() {
		cal.gotoNextMonth( updateMonthYear );
	} );
	$( '#custom-prev' ).on( 'click', function() {
		cal.gotoPreviousMonth( updateMonthYear );
	} );

	function updateMonthYear() {                
		$month.html( cal.getMonthName() );
		$year.html( cal.getYear() );
	}

	// just an example..
	function showEvents( contentEl, dateProperties ) {

		hideEvents();
		
		var $events = $( '<div id="custom-content-reveal" class="custom-content-reveal"><h4>Events for ' + dateProperties.monthname + ' ' + dateProperties.day + ', ' + dateProperties.year + '</h4></div>' ),
			$close = $( '<span class="custom-content-close"></span>' ).on( 'click', hideEvents );

		$events.append( contentEl.join('') , $close ).insertAfter( $wrapper );
		
		setTimeout( function() {
			$events.css( 'top', '0%' );
		}, 25 );

	}
	function hideEvents() {

		var $events = $( '#custom-content-reveal' );
		if( $events.length > 0 ) {
			
			$events.css( 'top', '100%' );
			Modernizr.csstransitions ? $events.on( transEndEventName, function() { $( this ).remove(); } ) : $events.remove();

		}

	}
}