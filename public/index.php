<?php

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';  

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV, 
    APPLICATION_PATH . '/configs/application.ini'
);

if(!defined('CRONJOB') || CRONJOB == false)
{
	try
	{
		$application->bootstrap()->run();
	} 
	catch(Zend_Db_Statement_Exception $e)
	{
		error_handler($e,0,'#pdo');
	}
	catch(Exception $e)
	{
		error_handler($e);
	}
}

function error_handler($e='',$showmsg=1,$additional='')
{
	?>
	<!DOCTYPE html>
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="shortcut icon" href="http://sms.inceif.org/images/favicon.ico" />
		<link href="/fonts/entypo/entypo.css" media="screen" rel="stylesheet" type="text/css" >
		<head>
			<title>Opps</title>
			<style type="text/css">
				body { color:#333; font:normal 12px Verdana; background: url('/images/bg-loop.png')  }
				#wrapper { margin-top:100px }
				#message { height:  250px; width: 600px; margin:0 auto;  }
				#message h1 { font: bold 24px Arial; letter-spacing:-1px; text-align:center; color:#29b3e1; margin:0 4px; }
				#message #logo { text-align:center; padding-bottom:15px; border-bottom:1px solid #efefef; margin-bottom:15px; }
				#error { }
				#error textarea { color:#888; font-family:Courier; font-size:11px; padding:3px; width:98%; color:#ccc ; border:1px solid #ccc;}
				.message { line-height:22px; margin:15px 10px 20px 10px; background:#fff; font-size:16px; border:1px solid #eee; padding:10px; -webkit-box-shadow: 1px 1px 0px rgba(50, 50, 50, 0.25); -moz-box-shadow:    1px 1px 0px rgba(50, 50, 50, 0.25); box-shadow: 1px 1px 0px rgba(50, 50, 50, 0.25);  }
				#wrapper .icon { color:#fbc91e; font-size:30px; float:left; margin-right:15px; }
				.message-wrap { margin-left:50px }
				#content-wrap { background:#fefefe; padding:10px; border:1px solid #eee }
			</style>
		</head>
		<body>
			<div id="wrapper">
				<div id="message">
					<div id="logo"><a href="/"><img src="/images/logo.png" alt="logo" width="300"/></a></div>
					<div id="error">
						
						<!-- <h1>An error has occured</h1> -->
						
						<div id="content-wrap">
							<?php if ( $showmsg == 1 ) {?>
							<div class="message">
								<span class="icon icon-attention"></span>
								<div class="message-wrap">
								<?php echo $e->getMessage() == '' ? 'An Error Has Occured' : $e->getMessage() ?>
								</div>
							 </div>
							<?php } else { echo '<!-- '.$e->getMessage().'-->'; } ?>
							<br /><br />
							<textarea>http://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].$additional ?></textarea>
							</div>
						</div>
				</div>
			</div>
		</body>
	</html>
	
	<?php
	exit;
}